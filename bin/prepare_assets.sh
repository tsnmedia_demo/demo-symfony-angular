#!/bin/bash

set -xe

LC_CTYPE="en_US.UTF-8"

SOURCE="${BASH_SOURCE[0]}"

echo "Preparing the npm modules and assets for dev environment"

type npm >/dev/null 2>&1
if [[ $? != 0 ]]; then
    echo "Node Package Manager (npm) not found. Please install node.js and run this script again."
    exit 1
fi

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )/../" && pwd )" # one folder out
cd $DIR

rm -rf web/assets
echo "Removed web/assets/"

mkdir web/assets
echo "Created web/assets/"

echo "Change to the directory Frontend folder"
cd src/Frontend
npm install --production

# Copy images
bash bin/deploy/copy-images.sh

# Run npm deploy
if [ "$1" = "live" ]; then
    ENV_DEPLOY="deploy:live"
else
    ENV_DEPLOY="deploy:production"
fi

npm run $ENV_DEPLOY

# Add async attribute
bash bin/deploy/add-async-attribute.sh

# Add critical CSS to login.html
bash bin/deploy/login-critical-css.sh

# Add style.*.css login.js
bash bin/deploy/login-add-style-css.sh

# Add main.*.js login.js
bash bin/deploy/login-add-main-js.sh

# Add remove unused CSS
bash bin/deploy/remove-unused-css.sh

exit 0
