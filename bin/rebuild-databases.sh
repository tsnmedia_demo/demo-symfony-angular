#!/usr/bin/env bash
BIN_FOLDER="$(dirname "${BASH_SOURCE[0]}")"

function rebuildDatabases()
{
    echo "Removing old $1 databases"

    php app/console doctrine:database:drop --env=$1 --force --no-interaction
    php app/console doctrine:database:drop --env=$1 --force --no-interaction --connection=app_global_write

    echo "Creating $1 databases"

    php app/console doctrine:database:create --env=$1 --no-interaction
    php app/console doctrine:database:create --env=$1 --no-interaction --connection=app_global_write

    echo "Migrating"

    php app/console doctrine:migrations:migrate --env=$1 --no-interaction
    php app/console doctrine:migrations:migrate --env=$1 --no-interaction --db=app_global_write

    echo "Inserting $1 queue"

    if [ $1 = "dev" ]; then
         APP_DB="app_db_global"

         mysql -uroot -psecret -h crowd-mysql5.6 -e '
         USE app_db_global;
         INSERT INTO app_db_global.user (username, first_name, last_name) VALUES ("username", "Firstname", "Lastname");
         INSERT INTO app_db_global.user_role (user_id, role_id) VALUES (1, 1);'

    fi

    echo "All done"
}


if [ "$1" != "test" ] && [ "$1" != "dev" ]; then
    echo "You need to specify in which environment you want to refresh databases (test or dev)"
else
    rm -rf ./$BIN_FOLDER/../app/cache/;
    mkdir ./$BIN_FOLDER/../app/cache/;
    touch ./$BIN_FOLDER/../app/cache/.gitkeep;
    echo "Cache has been deleted";


    rebuildDatabases $1
fi
