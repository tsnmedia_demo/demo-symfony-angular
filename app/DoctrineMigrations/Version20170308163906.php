<?php

namespace Backend\Migrations;

use Backend\Component\Migration\AppGlobalMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170308163906 extends AppGlobalMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
            CREATE TABLE `user` (
              `id` int unsigned NOT NULL AUTO_INCREMENT,
              `username` varchar(40) NOT NULL,
              `first_name` varchar(40) DEFAULT NULL,
              `last_name` varchar(40) DEFAULT NULL,
              `email` varchar(80) DEFAULT NULL,
              `city` varchar(100) DEFAULT NULL,
              `street` varchar(100) DEFAULT NULL,
              `postcode` varchar(10) DEFAULT NULL,
              PRIMARY KEY (`id`),
              INDEX IDX_FIRST_NAME_LAST_NAME (first_name, last_name),
              INDEX IDX_EMAIL (email),
              UNIQUE KEY `IDX_USERNAME` (`username`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');

        $this->addSql('
            CREATE TABLE `user_role` (
              `user_id` INT(10) UNSIGNED NOT NULL,
              `role_id` SMALLINT(5) UNSIGNED NOT NULL,
              PRIMARY KEY (`user_id`,`role_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('DROP TABLE `user`;');
        $this->addSql('DROP TABLE `user_role`;');
    }
}
