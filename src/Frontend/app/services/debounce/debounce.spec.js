import debounceModule from './debounce';

describe('debounceService', () => {
    beforeEach(window.module(debounceModule.name));

    describe('Module', () => {
        it('has as a name app.service.debounce', () => {
            expect(debounceModule.name).toBe('app.service.debounce');
        });
    });
});
