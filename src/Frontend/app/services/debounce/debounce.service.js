import debounce from 'lodash.debounce';

const debounceService = () => {
    return debounce;
};

export default debounceService;
