import angular from 'angular';
import debounceService from './debounce.service';

const debounceModule = angular.module('app.service.debounce', [])
    .service('debounceService', debounceService);

export default debounceModule;
