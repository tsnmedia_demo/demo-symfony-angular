import isPlainObject from 'lodash.isplainobject';
import linksListToObjectModule from './links-list-to-object';
import servicesModule from '../services';

describe('linksListToObjectService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.linksListToObject', () => {
            expect(linksListToObjectModule.name).toBe('app.service.linksListToObject');
        });
    });

    describe('Service', () => {
        let arr, obj, linksListToObjectService;

        beforeEach(window.inject((_linksListToObjectService_) => {
            linksListToObjectService = _linksListToObjectService_;

            arr = [
                {
                    'name': 'main',
                    'href': '/main',
                    'rel': 'main',
                    'method': 'GET'
                },
                {
                    'name': 'logout',
                    'href': '/logout',
                    'rel': 'logout',
                    'method': 'GET'
                },
                {
                    'name': 'request-rights',
                    'href': '/request-rights',
                    'rel': 'request-rights',
                    'method': 'GET'
                }
            ];

            obj = linksListToObjectService.get(arr);
        }));

        ['get'].forEach(property => {
            it(`has a ${property} property`, () => {
                expect(linksListToObjectService[property]).toBeDefined();
            });
        });

        it('should return an object', () => {
            expect(isPlainObject(linksListToObjectService.get(arr))).toBeTruthy();
        });

        it('should return an object with a main key links', () => {
            expect(obj).toBeDefined();
        });

        it('should return an empty object if the argument is not an array', () => {
            let str = linksListToObjectService.get('test');

            expect(str).toEqual({});
        });

        ['main', 'logout'].forEach(item => {
            it(`should a key build with name + Href (${item}Href) return the href value`, () => {
                expect(obj[`${item}Href`]).toBe(`/${item}`);
            });
        });

        it(`should have a key build with name, request-rights, with dash as a camelCase + Href (requestRightsHref) return the href value`, () => {
            expect(obj['requestRightsHref']).toBe('/request-rights');
        });
    });
});
