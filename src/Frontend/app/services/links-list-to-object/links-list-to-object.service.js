const linksListToObjectService = () => {
    const generateAKeyWithTheNamePlusHrefAndTitle = (arr) => {
        const linksObj = {};
        const newArray = Array.isArray(arr) ? arr : [];

        newArray.forEach((item) => {
            const name = item.name.replace(/[-_](\w|\d)/g, (text) => text[1].toUpperCase());

            linksObj[`${name}Href`] = item.href;
        });

        return linksObj;
    };

    return {
        get: generateAKeyWithTheNamePlusHrefAndTitle
    };
};

export default linksListToObjectService;
