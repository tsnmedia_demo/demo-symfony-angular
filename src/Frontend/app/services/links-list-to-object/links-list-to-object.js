import angular from 'angular';
import linksListToObjectService from './links-list-to-object.service';

const linksListToObjectModule = angular
    .module('app.service.linksListToObject', [])
    .service('linksListToObjectService', linksListToObjectService);

export default linksListToObjectModule;
