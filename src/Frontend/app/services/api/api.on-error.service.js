const apiOnErrorService = (
    apiErrorsService,
    whenService
) => {
    const onError = (response, status) => {
        apiErrorsService.errorMessage(response, status);
        apiErrorsService.alreadySubmitted(response, status);
        apiErrorsService.unauthorized(status);
        apiErrorsService.internalError(status);
        apiErrorsService.internetConnectionProblem(status);
    };

    const requestError = (obj, response) => {
        const {defer, isNotLog} = obj;
        const {data, status} = response;

        whenService(isNotLog, onError.bind(null, data, status))();
        defer.reject({response: data, status});
    };

    return {
        init: requestError
    };
};

export default apiOnErrorService;
