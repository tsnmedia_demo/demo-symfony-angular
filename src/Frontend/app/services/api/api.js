import angular from 'angular';
import apiService from './api.service';
import apiCancelPromiseService from './api.cancel-promise.service';
import apiErrorsService from './api.errors.service';
import apiOnErrorService from './api.on-error.service';
import apiOnSuccessService from './api.on-success.service';

const apiModule = angular.module('app.service.api', [])
    .service('apiService', apiService)
    .service('apiCancelPromiseService', apiCancelPromiseService)
    .service('apiErrorsService', apiErrorsService)
    .service('apiOnErrorService', apiOnErrorService)
    .service('apiOnSuccessService', apiOnSuccessService);

export default apiModule;
