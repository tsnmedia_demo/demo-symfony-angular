/**
 * @see https://gist.github.com/atuttle/8804601
 * @see http://odetocode.com/blogs/scott/archive/2014/04/24/canceling-http-requests-in-angularjs.aspx
 */

const apiService = (
    $rootScope,
    $http,
    $q,
    apiCancelPromiseService,
    apiOnErrorService,
    apiOnSuccessService,
    buttonsStatesService,
    constantsService,
    whenCurriedService
) => {
    const C_SPINNER_WATCH = constantsService.get('C_SPINNER_WATCH');

    const disabledButtons = () => {
        buttonsStatesService.disabled({clear: false});
    };

    const addDataAsArgument = (httpArgs, data) => {
        httpArgs.push(data || {});
    };

    const makeRequest = (verb, uri, data, disableNotified) => {
        let defer = $q.defer();

        const httpArgs = [uri];
        const isNotLog = !RegExp(`^${constantsService.get('API_LOG_URI')}`).test(uri);
        const isNotGet = !verb.match(/get/);
        const isNotAutosave = !RegExp(`[\&|\?]${constantsService.get('AUTOSAVE_GET_PARAMETER')}`).test(uri);

        whenCurriedService(isNotGet && isNotAutosave)(disabledButtons);
        whenCurriedService(isNotGet)(addDataAsArgument.bind(null, httpArgs, data));

        const promise = $http[verb].apply(null, httpArgs);

        promise.then(apiOnSuccessService.init.bind(null, {
            defer: defer,
            promise: promise,
            uri: uri,
            hasToBeNotified: isNotGet && isNotLog && isNotAutosave && !disableNotified
        }), apiOnErrorService.init.bind(null, {
            defer: defer,
            isNotLog: isNotLog,
            promise: promise
        }));

        apiCancelPromiseService.init({
            defer: defer,
            promise: promise,
            uri: uri
        });

        $rootScope.$broadcast(C_SPINNER_WATCH, defer.promise);

        return defer.promise;
    };

    return {
        get: makeRequest.bind(null, 'get'),
        delete: makeRequest.bind(null, 'delete'),
        post: makeRequest.bind(null, 'post'),
        put: makeRequest.bind(null, 'put')
    };
};

export default apiService;
