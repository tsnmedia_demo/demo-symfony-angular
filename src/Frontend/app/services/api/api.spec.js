import angular from 'angular';
import apiModule from './api';
import buttonsStates from 'services/buttons-states/buttons-states';
import servicesModule from 'services/services';

describe('apiService', () => {

    const windowObj = {};
    const dataStore = {
        $state: {
            current: ''
        }
    };

    beforeEach(window.module(
        // buttonsStates is a dependency of api and should be before servicesModule
        buttonsStates.name,
        servicesModule.name,
        ($provide, $qProvider) => {
            $qProvider.errorOnUnhandledRejections(false);

            windowObj.globalConfig = {};
            windowObj.globalConfig.i18n = {
                '_message.successful_': 'message successful',
                '_message.internal.error_': 'message internal error',
                '_message.notification.error.0_': 'Must contain only letters and spaces',
                '_message.notification.error.8_': 'test error 8',
                '_message.notification.unauthorized_': '',
                '_message.notification.internet.connection.problem_': ''
            };

            $provide.value('$window', windowObj);
            $provide.value('$document', angular.element(document));
            $provide.service('dataStoreService', () => {
                return {
                    setData: (key, data) => {
                        dataStore[key] = data;
                    },
                    getData: (key) => {
                        return dataStore[key];
                    }
                };
            });

            $provide.service('$state', () => {
                return {
                    transitionTo: () => {}
                };
            });
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.api', () => {
            expect(apiModule.name).toBe('app.service.api');
        });
    });

    describe('Service', () => {
        let $window,
            $document,
            $state,
            apiMainUri,
            apiLogUri,
            apiSettingsUri,
            apiService,
            buttonsStatesService,
            constantsService,
            httpBackend,
            notificationsService,
            rootScope,
            timeout;

        beforeEach(window.inject((
            _$document_,
            _$httpBackend_,
            _$rootScope_,
            _$timeout_,
            _$window_,
            _$state_,
            _apiService_,
            _buttonsStatesService_,
            _constantsService_,
            _notificationsService_
        ) => {
            $document = _$document_;
            $window = _$window_;
            $state = _$state_;
            apiService = _apiService_;
            buttonsStatesService = _buttonsStatesService_;
            constantsService = _constantsService_;
            httpBackend = _$httpBackend_;
            notificationsService = _notificationsService_;
            rootScope = _$rootScope_;
            timeout = _$timeout_;

            apiMainUri = constantsService.get('API_MAIN_URI');
            apiLogUri = constantsService.get('API_LOG_URI');
            apiSettingsUri = constantsService.get('API_SETTINGS_URI');

            spyOn(notificationsService, 'set').and.callThrough();
            spyOn(buttonsStatesService, 'disabled').and.callThrough();
            spyOn(buttonsStatesService, 'reset').and.callThrough();
            spyOn(rootScope, '$broadcast').and.callThrough();
        }));

        afterEach(() => {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        function errors(context) {
            const whenMethod = 'when' + context;
            const method = context.toLowerCase();

            describe('Status 400', () => {
                it('should reject promise returning an error message with the status', () => {
                    let error;

                    const errorResponse = {error: 0, value: 'test'};

                    httpBackend[whenMethod](apiMainUri).respond(() => {
                        return [400, errorResponse, {}];
                    });
                    apiService[method](apiMainUri);
                    httpBackend.flush();

                    expect(notificationsService.set).toHaveBeenCalledWith('error', { message: '_message.notification.error.0_', value: 'test' });
                });

                it('should not notify on api/log', () => {
                    httpBackend[whenMethod](apiLogUri).respond(() => {
                        return [400, '', {}];
                    });
                    apiService[method](apiLogUri);
                    httpBackend.flush();

                    expect(notificationsService.set).not.toHaveBeenCalled();
                });
            });

            describe('Status 401', () => {
                it('should call notificationsService.set', () => {
                    httpBackend[whenMethod](apiMainUri).respond(401);
                    apiService[method](apiMainUri);
                    httpBackend.flush();

                    expect(notificationsService.set).toHaveBeenCalledWith('warning', {message: '_message.notification.unauthorized_'});
                });

                it('should not notify on api/log', () => {
                    httpBackend[whenMethod](apiLogUri).respond(401);
                    apiService[method](apiLogUri);
                    httpBackend.flush();

                    expect(notificationsService.set).not.toHaveBeenCalled();
                });
            });

            describe('Status 500', () => {
                it('should call notificationsService.set', () => {
                    httpBackend[whenMethod](apiMainUri).respond(500);
                    apiService[method](apiMainUri);
                    httpBackend.flush();

                    expect(notificationsService.set).toHaveBeenCalledWith('error', '_message.internal.error_');
                });

                it('should not notify on api/log', () => {
                    httpBackend[whenMethod](apiLogUri).respond(500);
                    apiService[method](apiLogUri);
                    httpBackend.flush();

                    expect(notificationsService.set).not.toHaveBeenCalled();
                });
            });
        }

        describe('get', () => {
            const response = 'test';

            describe('Status 200', () => {
                beforeEach(() => {
                    httpBackend.whenGET(apiMainUri).respond(200, 'test');
                });

                it('should resolve a promise returning the response', () => {
                    let success;

                    apiService.get(apiMainUri).then((res) => {
                        success = res;
                    });
                    httpBackend.flush();
                    expect(success).toBe(response);
                });

                it('should not returning a success message', () => {
                    let success;

                    apiService.get(apiMainUri).then((res) => {
                        success = res;
                    });
                    httpBackend.flush();

                    expect(notificationsService.set).not.toHaveBeenCalled();
                });
            });

            errors('GET');
        });

        describe('delete', () => {
            const response = 'test';

            describe('Status 200', () => {
                it('should resolve a promise returning the response', () => {
                    let success;

                    httpBackend.whenDELETE(apiMainUri).respond(200, 'test');
                    apiService.delete(apiMainUri).then((res) => {
                        success = res;
                    });
                    httpBackend.flush();
                    expect(success).toBe(response);
                });
            });

            errors('DELETE');
        });

        describe('post', () => {
            const dataSent = {test: 'test'};

            describe('Status 200', () => {
                let responseHeader;

                beforeEach(() => {
                    httpBackend.whenPOST(apiMainUri).respond(
                        (method, url, data, header) => {
                            responseHeader = header;

                            return [200, dataSent, {}];
                        }
                    );
                });

                it('should returning a success message', () => {
                    apiService.post(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(notificationsService.set).toHaveBeenCalledWith('success', constantsService.get('I18N_MESSAGES_SUCCESSFUL'));
                });

                it('should call buttonsStatesService.disabled', () => {
                    apiService.post(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(buttonsStatesService.disabled).toHaveBeenCalledWith({clear: false});
                });

                it('should not call buttonsStatesService.reset', () => {
                    apiService.post(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(buttonsStatesService.reset).not.toHaveBeenCalled();
                });

                it('should call $rootScope.$broadcast', () => {
                    apiService.post(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(rootScope.$broadcast).toHaveBeenCalledWith('EVENT_API_SUCCESS', apiMainUri);
                });

                it('should resolve a promise returning the response', () => {
                    let success;

                    apiService.post(apiMainUri, dataSent).then((res) => {
                        success = res;
                    });
                    httpBackend.flush();
                    expect(JSON.stringify(success)).toBe(JSON.stringify(dataSent));
                });

                it('should has header Content-Type: application/json;charset=utf-8', () => {
                    apiService.post(apiMainUri, dataSent);
                    httpBackend.flush();
                    expect('application/json;charset=utf-8')
                        .toBe(responseHeader['Content-Type']);
                });
            });

            errors('POST');
        });

        describe('put', () => {
            const dataSent = {test: 'test'};

            describe('Status 200', () => {
                let responseHeader;

                beforeEach(() => {
                    httpBackend.whenPUT(apiMainUri).respond(
                        (method, url, data, header) => {
                            responseHeader = header;

                            return [200, dataSent, {}];
                        }
                    );
                });

                it('should returning a success message', () => {
                    apiService.put(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(notificationsService.set).toHaveBeenCalledWith('success', constantsService.get('I18N_MESSAGES_SUCCESSFUL'));
                });

                it('should call buttonsStatesService.disabled', () => {
                    apiService.put(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(buttonsStatesService.disabled).toHaveBeenCalledWith({clear: false});
                });

                it('should not call buttonsStatesService.reset', () => {
                    apiService.put(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(buttonsStatesService.reset).not.toHaveBeenCalled();
                });

                it('should call $rootScope.$broadcast', () => {
                    apiService.put(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect(rootScope.$broadcast).toHaveBeenCalledWith('EVENT_API_SUCCESS', apiMainUri);
                });

                it('should resolve a promise returning the response', () => {
                    let success;

                    apiService.put(apiMainUri, dataSent).then((res) => {
                        success = res;
                    });
                    httpBackend.flush();

                    expect(JSON.stringify(success)).toBe(JSON.stringify(dataSent));
                });

                it('should has header Content-Type: application/json;charset=utf-8', () => {
                    apiService.put(apiMainUri, dataSent);
                    httpBackend.flush();

                    expect('application/json;charset=utf-8').toBe(responseHeader['Content-Type']);
                });
            });

            errors('PUT');
        });

        describe('apiCancelPromiseService', () => {
            let apiCancelPromiseService, cancelData, dataStoreService, defer, promise;

            beforeEach(window.inject((
                _$q_,
                _apiCancelPromiseService_,
                _dataStoreService_
            ) => {
                apiCancelPromiseService = _apiCancelPromiseService_;
                dataStoreService = _dataStoreService_;
                defer = _$q_.defer();

                httpBackend.whenGET(apiSettingsUri).respond(200, 'test');
                promise = apiService.get(apiSettingsUri);
                httpBackend.flush();

                cancelData = {
                    defer: defer,
                    promise: promise,
                    uri: apiSettingsUri
                };

                spyOn(dataStoreService, 'setData').and.callThrough();
                spyOn(dataStoreService, 'getData').and.callThrough();
                spyOn(defer, 'reject').and.callThrough();
                spyOn(rootScope, '$on').and.callThrough();
            }));

            it('should called dataStoreService.getData with cancel promises constant key', () => {
                apiCancelPromiseService.init(cancelData);

                expect(dataStoreService.getData).toHaveBeenCalledWith(constantsService.get('DATA_STORE_CANCELABLE_PROMISES'));
            });

            it('should resolve the defer after promise.cancel', () => {
                cancelData = {
                    defer: defer,
                    promise: apiService.get(apiSettingsUri),
                    uri: apiSettingsUri
                };

                apiCancelPromiseService.init(cancelData);
                cancelData.promise.cancel();

                httpBackend.flush();

                expect(defer.reject).toHaveBeenCalledWith(constantsService.get('DATA_STORE_CANCELABLE_PROMISES'));
            });

            it('should not resolve the defer after promise.cancel if finally has been invoked', () => {
                cancelData = {
                    defer: defer,
                    promise: apiService.get(apiSettingsUri),
                    uri: apiSettingsUri
                };

                apiCancelPromiseService.init(cancelData);

                httpBackend.flush();

                cancelData.promise.cancel();

                expect(defer.reject).not.toHaveBeenCalled();
            });

            it('should call defer reject after call cancelAllPromises', () => {
                cancelData = {
                    defer: defer,
                    promise: apiService.get(apiSettingsUri),
                    uri: apiSettingsUri
                };

                apiCancelPromiseService.init(cancelData);
                apiCancelPromiseService.cancelAllPromises();

                httpBackend.flush();

                expect(defer.reject).toHaveBeenCalled();
            });

            it('should clean DATA_STORE_CANCELABLE_PROMISES after call cancelAllPromises and the last promise is cancelled', () => {
                cancelData = {
                    defer: defer,
                    promise: apiService.get(apiSettingsUri),
                    uri: apiSettingsUri
                };

                apiCancelPromiseService.init(cancelData);
                apiCancelPromiseService.cancelAllPromises();

                const promises = dataStoreService.getData('DATA_STORE_CANCELABLE_PROMISES');

                httpBackend.flush();

                expect(promises).toEqual([]);
            });

            it('should clean DATA_STORE_CANCELABLE_PROMISES after broadcast $stateChangeStart and the last promise is cancelled', () => {
                cancelData = {
                    defer: defer,
                    promise: apiService.get(apiSettingsUri),
                    uri: apiSettingsUri
                };

                apiCancelPromiseService.init(cancelData);
                rootScope.$broadcast('$stateChangeStart');

                const promises = dataStoreService.getData('DATA_STORE_CANCELABLE_PROMISES');

                httpBackend.flush();

                expect(promises).toEqual([]);
            });

            it('should listen $stateChangeStart', () => {
                cancelData = {
                    defer: defer,
                    promise: apiService.get(apiSettingsUri),
                    uri: apiSettingsUri
                };

                apiCancelPromiseService.init(cancelData);
                rootScope.$broadcast('$stateChangeStart');

                httpBackend.flush();

                expect(rootScope.$on).toHaveBeenCalled();
            });

            describe('apiMainUri', () => {
                it('should not be called dataStoreService.getData', () => {
                    httpBackend.whenGET(apiMainUri).respond(200, 'test');
                    promise = apiService.get(apiMainUri);

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiMainUri),
                        uri: apiMainUri
                    };

                    apiCancelPromiseService.init(cancelData);
                    httpBackend.flush();

                    expect(dataStoreService.getData).not.toHaveBeenCalled();
                });

                it('should not throw an error when try to call promise.cancel', () => {
                    httpBackend.whenGET(apiMainUri).respond(200, 'test');

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiMainUri),
                        uri: apiMainUri
                    };

                    apiCancelPromiseService.init(cancelData);

                    expect(() => {
                        cancelData.promise.cancel();
                    }).toThrowError(/undefined/);

                    httpBackend.flush();
                });

                it('should not call defer reject after call cancelAllPromises', () => {
                    httpBackend.whenGET(apiMainUri).respond(200, 'test');

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiMainUri),
                        uri: apiMainUri
                    };

                    apiCancelPromiseService.init(cancelData);
                    apiCancelPromiseService.cancelAllPromises();

                    httpBackend.flush();

                    expect(defer.reject).not.toHaveBeenCalled();
                });

                it('should not listen $stateChangeStart', () => {
                    httpBackend.whenGET(apiMainUri).respond(200, 'test');

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiMainUri),
                        uri: apiMainUri
                    };

                    apiCancelPromiseService.init(cancelData);
                    rootScope.$broadcast('$stateChangeStart');

                    httpBackend.flush();

                    expect(rootScope.$on).not.toHaveBeenCalled();
                });
            });

            describe('apiLogUri', () => {
                it('should not be called dataStoreService.getData', () => {
                    httpBackend.whenGET(apiLogUri).respond(200, 'test');
                    promise = apiService.get(apiLogUri);

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiLogUri),
                        uri: apiLogUri
                    };

                    apiCancelPromiseService.init(cancelData);
                    httpBackend.flush();

                    expect(dataStoreService.getData).not.toHaveBeenCalled();
                });

                it('should not throw an error when try to call promise.cancel', () => {
                    httpBackend.whenGET(apiLogUri).respond(200, 'test');

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiLogUri),
                        uri: apiLogUri
                    };

                    apiCancelPromiseService.init(cancelData);

                    expect(() => {
                        cancelData.promise.cancel();
                    }).toThrowError(/undefined/);

                    httpBackend.flush();
                });

                it('should not call defer reject after call cancelAllPromises', () => {
                    httpBackend.whenGET(apiLogUri).respond(200, 'test');

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiLogUri),
                        uri: apiLogUri
                    };

                    apiCancelPromiseService.init(cancelData);
                    apiCancelPromiseService.cancelAllPromises();

                    httpBackend.flush();

                    expect(defer.reject).not.toHaveBeenCalled();
                });

                it('should not listen $stateChangeStart', () => {
                    httpBackend.whenGET(apiLogUri).respond(200, 'test');

                    cancelData = {
                        defer: defer,
                        promise: apiService.get(apiLogUri),
                        uri: apiLogUri
                    };

                    apiCancelPromiseService.init(cancelData);
                    rootScope.$broadcast('$stateChangeStart');

                    httpBackend.flush();

                    expect(rootScope.$on).not.toHaveBeenCalled();
                });
            });
        });

        describe('apiErrorsService', () => {
            let state,
                apiErrorsService,
                originalTimeout,
                timeoutReloadService;

            beforeEach(window.inject((
                $state,
                _apiErrorsService_,
                _timeoutReloadService_
            ) => {
                state = $state;
                apiErrorsService = _apiErrorsService_;
                timeoutReloadService = _timeoutReloadService_;

                originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
                jasmine.DEFAULT_TIMEOUT_INTERVAL = 0;

            }));

            beforeEach(() => {
                spyOn(timeoutReloadService, 'reload').and.callThrough();
                spyOn(state, 'transitionTo');
            });

            it('should call notificationsService.set when errorMessage is called with the proper params', () => {
                apiErrorsService.alreadySubmitted({error: 8}, 400);

                expect(timeoutReloadService.reload).toHaveBeenCalled();
            });

            it('should call notificationsService.set when errorMessage is called with status an error different than 7', () => {
                apiErrorsService.alreadySubmitted({error: 0}, 400);

                expect(timeoutReloadService.reload).not.toHaveBeenCalled();
            });

            it('should call notificationsService.set when errorMessage is called with the proper params', () => {
                apiErrorsService.errorMessage({error: 8}, 400);

                expect(notificationsService.set).toHaveBeenCalledWith('error', { message: '_message.notification.error.8_', value: undefined });
            });

            it('should call buttonsStatesService.reset when errorMessage is called with status an error different than 7', () => {
                apiErrorsService.errorMessage({error: 0}, 400);

                expect(buttonsStatesService.reset).toHaveBeenCalledWith({clear: false});
            });

            it('should not call notificationsService.set when errorMessage is called with status different to 400', () => {
                apiErrorsService.errorMessage({error: 8}, 0);

                expect(notificationsService.set).not.toHaveBeenCalled();
            });

            it('should call notificationsService.set when user has problems with internet connection', () => {
                apiErrorsService.internetConnectionProblem(-1);

                expect(notificationsService.set).toHaveBeenCalledWith('error', { message: '_message.notification.internet.connection.problem_' });
            });

            it('should call notificationsService.set when unauthorized is called with the proper params', () => {
                apiErrorsService.unauthorized(401);

                expect(notificationsService.set).toHaveBeenCalledWith('warning', { message: '_message.notification.unauthorized_' });
            });

            it('should goes to the logout', () => {
                apiErrorsService.unauthorized(401);

                $document[0].body.click();

                expect($window.location)
                    .toBe(`${window.location.protocol}//${window.location.host}${constantsService.get('URI_LOGOUT')}`);
            });

            it('should call notificationsService.set when unauthorized is called with status different to 401', () => {
                apiErrorsService.unauthorized(0);

                expect(notificationsService.set).not.toHaveBeenCalled();
            });

            it('should call buttonsStatesService.disabled', () => {
                apiErrorsService.internalError(500);

                expect(buttonsStatesService.disabled).toHaveBeenCalledWith({clear: true});
            });

            it('should call notificationsService.set when internalError is called with the proper params', () => {
                apiErrorsService.internalError(500);

                expect(notificationsService.set).toHaveBeenCalledWith('error', '_message.internal.error_');
            });

            it('should call $state.transitionTo when internalError is called with the proper params', () => {
                apiErrorsService.internalError(503);

                expect($state.transitionTo).toHaveBeenCalled();
            });

            it('should call notificationsService.set when internalError is called with the proper params', () => {
                apiErrorsService.internalError(500);

                expect(rootScope.$broadcast).toHaveBeenCalledWith(constantsService.get('EVENT_API_ERROR_500'));
            });
        });

        describe('apiOnErrorService', () => {
            let apiErrorsService, apiOnErrorService, defer, errorData, state;

            beforeEach(window.inject((
                _$state_,
                _$q_,
                _apiErrorsService_,
                _apiOnErrorService_
            ) => {
                apiErrorsService = _apiErrorsService_;
                apiOnErrorService = _apiOnErrorService_;
                defer = _$q_.defer();
                state = _$state_;

                errorData = {
                    defer: defer,
                    isNotLog: false
                };

                spyOn(defer, 'reject').and.callThrough();
                spyOn(apiErrorsService, 'errorMessage').and.callThrough();
                spyOn(apiErrorsService, 'alreadySubmitted').and.callThrough();
                spyOn(apiErrorsService, 'unauthorized').and.callThrough();
                spyOn(apiErrorsService, 'internalError').and.callThrough();
            }));

            ['errorMessage', 'alreadySubmitted'].forEach((method) => {
                it(`should call apiErrorsService.${method} if isNotLog is true`, () => {
                    errorData.isNotLog = true;

                    apiOnErrorService.init(errorData, {data: 'test', status: 500});

                    expect(apiErrorsService[method]).toHaveBeenCalledWith('test', 500);
                });

                it(`should call apiErrorsService.${method} if isNotLog is true`, () => {
                    apiOnErrorService.init(errorData, {data: 'test', status: 500});

                    expect(apiErrorsService[method]).not.toHaveBeenCalledWith('test', 500);
                });
            });

            ['unauthorized', 'internalError'].forEach((method) => {
                it(`should call apiErrorsService.${method} if isNotLog is true`, () => {
                    errorData.isNotLog = true;

                    apiOnErrorService.init(errorData, {data: 'test', status: 500});

                    expect(apiErrorsService[method]).toHaveBeenCalledWith(500);
                });

                it(`should call apiErrorsService.${method} if isNotLog is true`, () => {
                    apiOnErrorService.init(errorData, {data: 'test', status: 500});

                    expect(apiErrorsService[method]).not.toHaveBeenCalledWith(500);
                });
            });

            it('should call defer.reject', () => {
                apiOnErrorService.init(errorData, {data: 'test', status: 500});

                expect(defer.reject).toHaveBeenCalledWith({ response: 'test', status: 500 });
            });
        });

        describe('apiOnSuccessService', () => {
            let apiOnSuccessService, defer, successData;

            beforeEach(window.inject((
                _$q_,
                _apiOnSuccessService_
            ) => {
                apiOnSuccessService = _apiOnSuccessService_;
                defer = _$q_.defer();

                successData = {
                    defer: defer,
                    uri: apiMainUri,
                    hasToBeNotified: false
                };

                spyOn(defer, 'resolve').and.callThrough();
            }));

            it('should call a success message if hasToBeNotified is true', () => {
                successData.hasToBeNotified = true;

                apiOnSuccessService.init(successData, 'test');

                expect(notificationsService.set).toHaveBeenCalledWith('success', constantsService.get('I18N_MESSAGES_SUCCESSFUL'));
            });

            it('should not call a success message if hasToBeNotified is false', () => {
                apiOnSuccessService.init(successData, 'test');

                expect(notificationsService.set).not.toHaveBeenCalled();
            });

            it('should call $rootScope.$broadcast', () => {
                apiOnSuccessService.init(successData, 'test');

                expect(rootScope.$broadcast).toHaveBeenCalledWith('EVENT_API_SUCCESS', apiMainUri);
            });

            it('should call defer.resolve', () => {
                apiOnSuccessService.init(successData, {data: 'test'});

                expect(defer.resolve).toHaveBeenCalledWith('test');
            });
        });
    });
});
