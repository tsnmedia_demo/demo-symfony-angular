const apiCancelPromiseService = (
    $rootScope,
    constantsService,
    dataStoreService,
    whenService,
    whenCurriedService
) => {
    let offStateChangeStart;

    const DATA_STORE_CANCELABLE_PROMISES = constantsService.get('DATA_STORE_CANCELABLE_PROMISES');
    const apiFreeToBeCancelled = [
        constantsService.get('API_LOG_URI'),
        constantsService.get('API_MAIN_URI')
    ];

    const addRequests = (promise) => {
        const requestedPromises = dataStoreService.getData(DATA_STORE_CANCELABLE_PROMISES) || [];

        requestedPromises.push(promise);

        dataStoreService.setData(DATA_STORE_CANCELABLE_PROMISES, requestedPromises);
    };

    const finalizePromise = (defer, promise) => {
        promise.finally(() => {
            promise.cancel = () => {};
            defer = promise = null;
        });
    };

    const cancelPromise = (defer, promise) => {
        promise.cancel = () => {
            defer.reject(DATA_STORE_CANCELABLE_PROMISES);
        };
    };

    const cleanDataStoreCancelablePromises = () => {
        dataStoreService.setData(DATA_STORE_CANCELABLE_PROMISES, []);
    };

    const whenLastPromise = (length, index) => {
        const last = length - 1 === index;

        whenService(last, cleanDataStoreCancelablePromises)();
        whenService(last && typeof offStateChangeStart === 'function', offStateChangeStart)();
    };

    const cancelEachPromise = (length, promise, index) => {
        promise.cancel();
        whenLastPromise(length, index);
    };

    const cancelAllPromises = () => {
        const promises = dataStoreService.getData(DATA_STORE_CANCELABLE_PROMISES);

        promises.forEach(cancelEachPromise.bind(null, promises.length));
    };

    const stateChangeStart = () => {
        whenService(typeof offStateChangeStart === 'function', offStateChangeStart)();
        offStateChangeStart = $rootScope.$on('$stateChangeStart', cancelAllPromises);
    };

    const initialize = (obj) => {
        let {defer, promise, uri} = obj;

        const whenUriIsCancelableCall = whenCurriedService(apiFreeToBeCancelled.indexOf(uri) === -1);

        whenUriIsCancelableCall(stateChangeStart);
        whenUriIsCancelableCall(cancelPromise.bind(null, defer, promise));
        whenUriIsCancelableCall(finalizePromise.bind(null, defer, promise));
        whenUriIsCancelableCall(addRequests.bind(null, promise));
    };

    return {
        init: initialize,
        cancelAllPromises
    };
};

export default apiCancelPromiseService;
