const apiErrorService = (
    $document,
    $rootScope,
    $window,
    $state,
    buttonsStatesService,
    constantsService,
    notificationsService,
    timeoutReloadService
) => {
    const disabledButtons = () => {
        buttonsStatesService.disabled({clear: true});
    };

    const resetButtons = () => {
        buttonsStatesService.reset({clear: false});
    };

    const messageNotificationUnauthorized = () => {
        notificationsService.set('warning', {
            message: '_message.notification.unauthorized_'
        });
    };

    const alreadySubmitted = (response, status) => {
        if (status === 400 && parseInt(response.error, 10) === constantsService.get('ERROR_CODE_ALREADY_SUBMITTED')) {
            timeoutReloadService.reload();
        }
    };

    const messageNotificationError = (response) => {
        notificationsService.set('error', {
            message: `_message.notification.error.${response.error}_`,
            value: response.value
        });
    };

    const errorMessage = (response, status) => {
        if (status === 400) {
            messageNotificationError(response);
            resetButtons();
        } else if (status === 403){
            $state.transitionTo(constantsService.get('STATE_SETTINGS'), {}, {location: false});
            notificationsService.set('warning', 'notification.access.denied');
        }
    };

    const internetConnectionProblem = (status) => {
        if (status === -1) {
            notificationsService.set('error', {
                message: '_message.notification.internet.connection.problem_'
            });
            resetButtons();
        }
    };

    const logout = (ev) => {
        $window.location = [
            window.location.protocol,
            '//',
            window.location.host,
            constantsService.get('URI_LOGOUT')
        ].join('');
        ev.preventDefault();
    };

    const unauthorized = (status) => {
        if (status === 401) {
            messageNotificationUnauthorized();
            $document[0].addEventListener('click', logout, false);
        }
    };

    const internalError = (status) => {
        if (status === 503) {
            $rootScope.$broadcast(constantsService.get('EVENT_AUTOSAVE_DISABLE_ACTIVE_TIMEOUT'));
            $state.transitionTo(constantsService.get('STATE_503'), {}, {location: false});
        } else if (/^5/.test(status)) {
            disabledButtons();
            notificationsService.set('error', constantsService.get('I18N_MESSAGES_INTERNAL_ERROR'));
            $rootScope.$broadcast(constantsService.get('EVENT_API_ERROR_500'));
        }
    };

    return {
        internetConnectionProblem,
        alreadySubmitted,
        errorMessage,
        unauthorized,
        internalError
    };
};

export default apiErrorService;
