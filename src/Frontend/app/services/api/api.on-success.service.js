const apiOnSuccessService = (
    $rootScope,
    constantsService,
    notificationsService,
    whenService
) => {
    const successNotification = () => {
        notificationsService.set('success', constantsService.get('I18N_MESSAGES_SUCCESSFUL'));
    };

    const requestSuccess = (obj, response) => {
        const {defer, uri, hasToBeNotified} = obj;

        $rootScope.$broadcast(constantsService.get('EVENT_API_SUCCESS'), uri);
        whenService(hasToBeNotified, successNotification)();
        defer.resolve(response.data);
    };

    return {
        init: requestSuccess
    };
};

export default apiOnSuccessService;
