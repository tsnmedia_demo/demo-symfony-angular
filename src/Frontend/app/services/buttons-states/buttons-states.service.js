const buttonsStatesService = ($document, dataStoreService, whenService) => {
    const buttonsData = {
        buttonList: {},
        buttonStateList: [],
        stateName: ''
    };

    const initButtonsData = (stateName) => {
        buttonsData.buttonList = {};
        buttonsData.buttonStateList = [];
        buttonsData.stateName = stateName || '';

        return buttonsData;
    };

    const buttonsDataState = (item) => {
        return buttonsData[item];
    };

    const isAllowedButton = (button) => {
        const dataEnabled = button.getAttribute('data-enabled');

        return dataEnabled === null || dataEnabled === 'false';
    };

    const changeButtonState = (obj) => {
        const button = obj.item;

        button.disabled = isAllowedButton(button) ? obj.disabled : button.disabled;
    };

    const buildButtonStateList = (obj) => {
        buttonsDataState('buttonStateList').push(obj);

        return obj;
    };

    const buttonState = (button) => {
        return {
            item: button,
            disabled: button.disabled
        };
    };

    const buttonsList = () => {
        const buttonList = buttonsDataState('buttonList');
        const stateName = buttonsDataState('stateName');

        buttonList[stateName] = buttonList[stateName] || Array.from($document[0].querySelectorAll('button'))
            .filter(isAllowedButton);

        return buttonList[stateName];
    };

    const initializeButtonsData = () => {
        const stateName = dataStoreService.getData('$state').current.name;
        const stateHasChanged = !buttonsDataState('buttonStateList').hasOwnProperty(stateName);
        const callback = whenService(stateHasChanged, initButtonsData.bind(null, stateName));

        return callback();
    };

    const initializeButtonsState = () => {
        return buttonsList()
            .map(buttonState)
            .map(buildButtonStateList);
    };

    const cleanButtonsData = (isClean) => {
        const callback = whenService(isClean, initButtonsData);

        return callback();
    };

    const initializeButtonsChangeState = (disabled, config) => {
        initializeButtonsData();
        initializeButtonsState().map(obj => changeButtonState({disabled: disabled, item: obj.item}));
        cleanButtonsData(config ? config.clean : false);
    };

    const resetButtonListState = (config) => {
        buttonsDataState('buttonStateList').map(obj => changeButtonState(obj));
        cleanButtonsData(config.clean);
    };

    return {
        buttonsDataState,
        enabled: initializeButtonsChangeState.bind(null, false),
        disabled: initializeButtonsChangeState.bind(null, true),
        reset: resetButtonListState,
        clean: cleanButtonsData.bind(null, true),
        setStates: initializeButtonsState
    };
};

export default buttonsStatesService;
