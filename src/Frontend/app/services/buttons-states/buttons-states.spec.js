import buttonsStatesModule from './buttons-states';
import servicesModule from 'services/services';

describe('buttonsStatesService', () => {
    beforeEach(window.module(
        servicesModule.name,
        ($provide) => {
            $provide.service('dataStoreService', () => {
                return {
                    setData: () => '',
                    getData: () => {
                        return {
                            current: {
                                name: 'test'
                            }
                        }
                    }
                };
            });
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.buttons-states', () => {
            expect(buttonsStatesModule.name).toBe('app.service.buttonsStates');
        });
    });

    describe('Service', () => {
        let buttonsStatesService, button;

        beforeEach(window.inject((_$document_, _buttonsStatesService_) => {
            buttonsStatesService = _buttonsStatesService_;
            button = document.createElement('button');

            document.body.appendChild(button);
        }));

        it('should be defined enabled', () => {
            expect(buttonsStatesService.enabled).toBeDefined();
        });

        it('should be defined disabled', () => {
            expect(buttonsStatesService.disabled).toBeDefined();
        });

        it('should disable the button', () => {
            buttonsStatesService.disabled();

            expect(button.outerHTML).toBe('<button disabled=""></button>');
        });

        it('should enable the button', () => {
            button.disabled = true;

            buttonsStatesService.enabled();

            expect(button.outerHTML).toBe('<button></button>');
        });

        it('should reset the button', () => {
            buttonsStatesService.disabled({clean: false});
            buttonsStatesService.reset({clean: true});

            expect(button.outerHTML).toBe('<button></button>');
        });

        it('should clean the buttons data object when clean is defined as true', () => {
            buttonsStatesService.disabled({clean: true});

            expect({
                buttonList: buttonsStatesService.buttonsDataState('buttonList'),
                buttonStateList: buttonsStatesService.buttonsDataState('buttonStateList'),
                stateName: buttonsStatesService.buttonsDataState('stateName')
            }).toEqual({ buttonList: {}, buttonStateList: [], stateName: '' });
        });

        it('should not clean the buttons data object when clean is defined as false', () => {
            buttonsStatesService.disabled({clean: false});

            expect({
                buttonList: buttonsStatesService.buttonsDataState('buttonList'),
                buttonStateList: buttonsStatesService.buttonsDataState('buttonStateList'),
                stateName: buttonsStatesService.buttonsDataState('stateName')
            }).toEqual({
                buttonList: {'test': [button]},
                buttonStateList: [{item: button, disabled: false}],
                stateName: 'test'
            });
        });

        it('should throw and exception on reset without define the param clean as a false', () => {
            buttonsStatesService.enabled();

            expect(() => buttonsStatesService.reset()).toThrowError(/undefined is not an object/);
        });

        it('should not disable the button because has the attribute data-enabled', () => {
            button.setAttribute('data-enabled', true);

            buttonsStatesService.disabled();

            expect(button.outerHTML).toBe('<button data-enabled="true"></button>');
        });

        it('should clean the buttons data object', () => {
            buttonsStatesService.disabled();

            expect(buttonsStatesService.clean()).toEqual({ buttonList: {}, buttonStateList: [], stateName: '' });
        });

        it('should set the buttons states', () => {
            const arr = [];

            buttonsStatesService.clean();
            buttonsStatesService.setStates().map(state => arr.push(state));

            expect(arr).toEqual([{ item: button, disabled: false }]);
        });

        afterEach(() => {
            buttonsStatesService.clean();
            button.removeAttribute('data-enabled');
            Array.from(document.querySelectorAll('button')).map(item => document.body.removeChild(item));
        });
    });
});
