import angular from 'angular';
import buttonsStatesService from './buttons-states.service';

const buttonsStatesModule = angular.module('app.service.buttonsStates', [])
    .service('buttonsStatesService', buttonsStatesService);

export default buttonsStatesModule;
