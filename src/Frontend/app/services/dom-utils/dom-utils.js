import angular from 'angular';
import domUtilsService from './dom-utils.service';

const domUtilsModule = angular.module('app.service.domUtils', [])
    .service('domUtilsService', domUtilsService);

export default domUtilsModule;
