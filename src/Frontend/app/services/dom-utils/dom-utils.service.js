const domUtilsService = () => {
    const closest = (el, selector) => {
        let matchedElement = null;

        const matchesSelector = el.matches || el.webkitMatchesSelector ||
            el.mozMatchesSelector || el.msMatchesSelector;

        while (el) {
            if (matchesSelector.call(el, selector)) {
                matchedElement = el;
                break;
            } else {
                el = el.parentElement;
            }
        }

        return matchedElement;
    };

    return {
        closest
    };
};

export default domUtilsService;
