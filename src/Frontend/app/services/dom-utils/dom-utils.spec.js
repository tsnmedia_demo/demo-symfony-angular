import domUtilsModule from './dom-utils';
import servicesModule from 'services/services';

describe('domUtilsService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.dom-utils', () => {
            expect(domUtilsModule.name).toBe('app.service.domUtils');
        });
    });

    describe('Service', () => {
        let domUtilsService, button, mozMockedElement, msMatchesSelector;

        beforeEach(window.inject((_domUtilsService_) => {
            domUtilsService = _domUtilsService_;

            const form = document.createElement('form');

            button = document.createElement('button');

            mozMockedElement = {
                mozMatchesSelector: {
                    call: () => {
                        return false;
                    }
                }
            };

            msMatchesSelector = {
                msMatchesSelector: {
                    call: () => {
                        return false;
                    }
                }
            };

            form.appendChild(button);
            document.body.appendChild(form);
        }));

        it('should find the closest form tag', () => {
            expect(domUtilsService.closest(button, 'form').outerHTML)
                .toBe('<form><button></button></form>');
        });

        it('should not find the closest div tag because it do not exist', () => {
            const button = document.querySelector('button');

            expect(domUtilsService.closest(button, 'div')).toBe(null);
        });

        it('should accept mozMatchesSelector', () => {
            expect(domUtilsService.closest(mozMockedElement)).toBe(null);
        });

        it('should accept msMatchesSelector', () => {
            expect(domUtilsService.closest(msMatchesSelector)).toBe(null);
        });

        afterEach(() => {
            const form = document.body.querySelector('form');

            form.outerHTML = '';
        });
    });
});
