import angular from 'angular';

import api from './api/api';
import assign from './assign/assign';
import assignPathname from './assign-pathname/assign-pathname';
import base from './base/base';
import buttonsStates from './buttons-states/buttons-states';
import constants from './constants/constants';
import countries from './countries/countries';
import currencyFormat from './currency-format/currency-format';
import currentDate from './current-date/current-date';
import dataStore from './data-store/data-store';
import dateFormat from './date-format/date-format';
import deepFreeze from './deep-freeze/deep-freeze';
import debounce from './debounce/debounce';
import domUtils from './dom-utils/dom-utils';
import errorLog from './error-log/error-log';
import formatString from './format-string/format-string';
import getMain from './get-main/get-main';
import getService from './get/get';
import hrefToState from './href-to-state/href-to-state';
import i18n from './i18n/i18n';
import isEqual from './is-equal/is-equal';
import isPlainObject from './is-plain-object/is-plain-object';
import languages from './languages/languages';
import linksListToObject from './links-list-to-object/links-list-to-object';
import loadingList from './loading-list/loading-list';
import mappingSubmitData from './mapping-submit-data/mapping-submit-data';
import mappingObject from './mapping-object/mapping-object';
import matchingData from './matching-data/matching-data';
import merge from './merge/merge';
import monthsByYear from './months-by-year/months-by-year';
import notifications from './notifications/notifications';
import objectReference from './object-reference/object-reference';
import onInit from './on-init/on-init';
import rangeService from './range/range';
import seoContentTypesService from './seo-content-types/seo-content-types';
import stacktrace from './stacktrace/stacktrace';
import templateCache from './template-cache/template-cache';
import timeoutReload from './timeout-reload/timeout-reload';
import validation from './validation/validation';
import when from './when/when';
import whenCurried from './when-curried/when-curried';
import yearsAndMonthsGenerator from './years-and-months-generator/years-and-months-generator';

export default angular.module('app.services', [
    api.name,
    assign.name,
    assignPathname.name,
    base.name,
    buttonsStates.name,
    constants.name,
    countries.name,
    currencyFormat.name,
    currentDate.name,
    dataStore.name,
    dateFormat.name,
    debounce.name,
    deepFreeze.name,
    domUtils.name,
    errorLog.name,
    formatString.name,
    getMain.name,
    getService.name,
    hrefToState.name,
    i18n.name,
    isEqual.name,
    isPlainObject.name,
    languages.name,
    linksListToObject.name,
    languages.name,
    loadingList.name,
    mappingSubmitData.name,
    mappingObject.name,
    matchingData.name,
    merge.name,
    monthsByYear.name,
    notifications.name,
    objectReference.name,
    onInit.name,
    rangeService.name,
    seoContentTypesService.name,
    stacktrace.name,
    templateCache.name,
    timeoutReload.name,
    validation.name,
    when.name,
    whenCurried.name,
    yearsAndMonthsGenerator.name
]);
