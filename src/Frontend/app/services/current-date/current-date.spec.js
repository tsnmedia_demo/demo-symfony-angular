import currentDateModule from './current-date';
import servicesModule from 'services/services';

describe('currentDateService', () => {
    beforeEach(window.module(
        servicesModule.name,($provide) => {
            $provide.value('Date', new Date(2015, 1, 1));
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.currentDate', () => {
            expect(currentDateModule.name).toBe('app.service.currentDate');
        });
    });

    describe('Service', () => {
        let currentDateService, currentDate, day, month, year;

        beforeEach(window.inject((_currentDateService_) => {
            currentDateService = _currentDateService_;

            currentDate = new Date(2015, 1, 1);
            day = currentDate.getDate();
            month = currentDate.getMonth() + 1;
            year = currentDate.getFullYear();

            jasmine.clock().mockDate(currentDate);
        }));

        it('should return and object with the current day, month and year', () => {
            expect(currentDateService.dateObject()).toEqual({
                day: day,
                month: month,
                year: year
            });
        });

        it('should return the current date', () => {
            expect(currentDateService.date()).toEqual(currentDate);
        });

        it('should return the current year', () => {
            expect(currentDateService.year()).toBe(year);
        });

        it('should return the current month', () => {
            expect(currentDateService.month()).toBe(month);
        });

        it('should return the current day', () => {
            expect(currentDateService.day()).toBe(day);
        });
    });
});
