const currentDateService = () => {
    const date = () => {
        return new Date();
    };

    const dateObject = () => {
        const currentDate = date();
        const currentDay = currentDate.getDate();
        const currentMonth = currentDate.getMonth() + 1;
        const currentYear = currentDate.getFullYear();

        return {
            day: currentDay,
            month: currentMonth,
            year: currentYear
        };
    };

    const day = () => {
        const currentDate = dateObject();

        return currentDate.day;
    };

    const month = () => {
        const currentDate = dateObject();

        return currentDate.month;
    };

    const year = () => {
        const currentDate = dateObject();

        return currentDate.year;
    };

    return {
        date,
        day,
        dateObject,
        month,
        year
    };
};

export default currentDateService;
