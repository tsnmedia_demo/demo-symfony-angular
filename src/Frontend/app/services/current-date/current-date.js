import angular from 'angular';
import currentDateService from './current-date.service';

const currentDateModule = angular.module('app.service.currentDate', [])
    .service('currentDateService', currentDateService);

export default currentDateModule;
