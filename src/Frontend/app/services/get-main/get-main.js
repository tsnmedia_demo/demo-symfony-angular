import angular from 'angular';

import getMainService from './get-main.service';

const getMainModule = angular.module(
    'app.service.getMain', []).service('getMainService', getMainService);

export default getMainModule;
