import getMainServiceModule from './get-main';

describe('getMainService', () => {
    beforeEach(window.module(
        getMainServiceModule.name,
        $provide => {
            $provide.service('constantsService', () => {
                return {
                    get: value => value
                };
            });
            $provide.service('apiService', () => {
                return {
                    get: () => {}
                };
            });
            $provide.service('dataStoreService', () => {
                return {
                    setData: () => {},
                };
            });
            $provide.service('hrefToStateService', () => {
                return {
                    convert: value => {
                        return {
                            state: value
                        };
                    }
                };
            });
        }
    ));

    describe('Service', () => {
        let getMainService,
            dataStoreService,
            response;

        beforeEach(window.inject((
            _getMainService_,
            _dataStoreService_,
            apiService
        ) => {
            getMainService = _getMainService_;
            dataStoreService = _dataStoreService_;

            response = {
                "lastToolIndex": 1,
                "menu": {
                    "anotherTool": {
                        "label": "another.tool",
                        "url": "/another/tool",
                    },
                    "user": {
                        "label": "Hawkee Crowd",
                        "url": "",
                        "items": [
                            {
                                "label": "payments",
                                "url": "/payments"
                            },
                            {
                                "label": "settings",
                                "url": "/settings"
                            }
                        ]
                    },
                    "tools": {
                        "label": "tools",
                        "url": "",
                        "items": [
                            {
                                "label": "advertiser.matching",
                                "url": "/tools/advertiser-matching"
                            },
                            {
                                "label": "descriptive.content",
                                "url": "/tools/seo"
                            }
                        ]
                    }
                }
            };

            spyOn(apiService, 'get').and.callFake(() => {
                return {
                    then: fn => fn(response)
                };
            });
        }));

        describe('get', () => {
            it('tracks that stored formatted menu data', () => {
                spyOn(dataStoreService, 'setData');

                getMainService.get();

                expect(dataStoreService.setData).toHaveBeenCalledWith('DATA_STORE_MAIN_MENU', {
                    "lastToolIndex": 1,
                    "menu": {
                        "anotherTool": {
                            "label": "another.tool",
                            "url": "/another/tool",
                            "state": "/another/tool"
                        },
                        "user": {
                            "label": "Hawkee Crowd",
                            "url": "",
                            "items": [
                                {
                                    "label": "payments",
                                    "url": "/payments",
                                    "state": "/payments"
                                },
                                {
                                    "label": "settings",
                                    "url": "/settings",
                                    "state": "/settings"
                                }
                            ]
                        },
                        "tools": {
                            "label": "tools",
                            "url": "",
                            "items": [
                                {
                                    "label": "advertiser.matching",
                                    "url": "/tools/advertiser-matching",
                                    "state": "/tools/advertiser-matching"
                                },
                                {
                                    "label": "descriptive.content",
                                    "url": "/tools/seo",
                                    "state": "/tools/seo"
                                }
                            ]
                        }
                    }
                });
            });
        });
    });
});
