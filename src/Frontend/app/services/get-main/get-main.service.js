const getMainService = (
    apiService,
    constantsService,
    dataStoreService,
    hrefToStateService
) => {
    const DATA_STORE_MAIN_MENU = constantsService.get('DATA_STORE_MAIN_MENU');

    function addStateByLink(link) {
        link.state = hrefToStateService.convert(link.url).state;
    }

    function addStateToEachLink(menu) {
        Object.keys(menu).forEach(key => {
            if (menu[key].url) {
                addStateByLink(menu[key]);
            }
            if (menu[key].items) {
                menu[key].items.forEach(addStateByLink);
            }
        });

        return menu;
    }

    function storeMenu(menuData) {
        dataStoreService.setData(DATA_STORE_MAIN_MENU, menuData);
    }

    function defineMenu(response) {
        const formattedResponse = Object.assign(
            {},
            response,
            {menu: addStateToEachLink(JSON.parse(JSON.stringify(response.menu)))}
        );

        storeMenu(formattedResponse);

        return formattedResponse;
    }

    const getMainApiData = () => {
        return apiService.get(constantsService.get('API_MAIN_URI')).then(defineMenu);
    };

    return {
        get: getMainApiData
    };
};

export default getMainService;
