import constantsModule from './constants';
import servicesModule from 'services/services';

describe('constantsService', () => {
    beforeEach(window.module(
        servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.constants', () => {
            expect(constantsModule.name).toBe('app.service.constants');
        });
    });

    describe('Service', () => {
        let constantsService;

        const invalid = 'is-invalid';

        beforeEach(window.inject((_constantsService_) => {
            constantsService = _constantsService_;
        }));

        it('should throw an error if the param do not exist as a key', () => {
            expect(() => constantsService.get('MOCK_KEY_DO_NOT_EXIST'))
                .toThrowError(/do not exist as a constant/);
        });

        it('should constantsService.all() be a frozen object', () => {
            expect(Object.isFrozen(constantsService.all())).toBeTruthy();
        });
    });
});