import apiUrls from './constants.api-uris';
import keyCodes from './constants.key-codes';
import other from './constants.other';
import statesUrls from './constants.states-urls';

const constantsService = (deepFreezeService) => {
    const constants = deepFreezeService.set(Object.assign({}, apiUrls, keyCodes, other, statesUrls));

    const keyNotExist = (key) => {
        if (!constants.hasOwnProperty(key)) {
            throw new Error(`The key "${key}" do not exist as a constant.`);
        }
    };

    const getConstantByKey = (key) => {
        keyNotExist(key);

        return constants[key];
    };

    return {
        all: () => constants,
        get: getConstantByKey
    };
};

export default constantsService;
