import angular from 'angular';
import constantsService from './constants.service';

const constantsModule = angular.module('app.service.constants', [])
    .service('constantsService', constantsService);

export default constantsModule;
