import range from 'lodash.range';

const rangeService = () => {
    return range;
};

export default rangeService;
