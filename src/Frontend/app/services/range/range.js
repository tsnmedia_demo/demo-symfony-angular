import angular from 'angular';
import rangeService from './range.service';

const rangeModule = angular.module('app.service.range', [])
    .service('rangeService', rangeService);

export default rangeModule;
