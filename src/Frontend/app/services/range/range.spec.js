import rangeModule from './range';

describe('rangeService', () => {
    beforeEach(window.module(rangeModule.name));

    describe('Module', () => {
        it('has as a name app.service.range', () => {
            expect(rangeModule.name).toBe('app.service.range');
        });
    });
});
