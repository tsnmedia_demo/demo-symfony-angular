import monthsByYearModule from './months-by-year';
import servicesModule from 'services/services';

describe('monthsByYearService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.months-by-year', () => {
            expect(monthsByYearModule.name).toBe('app.service.monthsByYear');
        });
    });

    describe('Service', () => {
        let monthsByYearService;
        let arrayOfDates;

        beforeEach(window.inject((_monthsByYearService_) => {
            monthsByYearService = _monthsByYearService_;

            arrayOfDates = [
                "2017-12-02T00:00:00",
                "2017-12-02T00:00:00",
                "2016-12-02T00:00:00",
                "2017-11-02T00:00:00",
                "2017-09-02T00:00:00",
                "2015-12-02T00:00:00",
                "2017-11-02T00:00:00",
                "2016-11-02T00:00:00",
                "2015-10-02T00:00:00"
            ];
        }));

        it('should return an Array of unique numbers using unique and reduce', () => {
            const arr = monthsByYearService.set(arrayOfDates);

            expect(arr).toEqual(
                [
                    {
                        year: 2015,
                        months: [10, 12]
                    },
                    {
                        year: 2016,
                        months: [11, 12]
                    },
                    {
                        year: 2017,
                        months: [9, 11, 12]
                    }
                ]
            );
        });

        it('should throw an Error if the previous param is not an Array', () => {
            expect(() => monthsByYearService.set(''))
                .toThrowError(/is not an Array/);
        });
    });
});
