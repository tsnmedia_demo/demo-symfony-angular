const monthsByYearService = () => {
    const createAListMonthsByYear = (arr, current, index) =>{
        const obj = {};
        const previous = arr[arr.length - 1];
        const year = new Date(current).getFullYear();
        const month = new Date(current).getMonth() + 1;

        if (index === 0 || previous.year !== year) {
            obj.year = year;
            obj.months = [month];

            arr.push(obj);
        } else if (previous.months.indexOf(month) === -1) {
            previous.months.push(month);
        }

        return arr;
    };

    const setMonthsByYear = (dates) => {
        if (!Array.isArray(dates)) {
            throw new Error(`The param previous is not an Array.`);
        }

        return dates.sort().reduce(createAListMonthsByYear, []);
    };

    return {
        set: setMonthsByYear
    };
};

export default monthsByYearService;
