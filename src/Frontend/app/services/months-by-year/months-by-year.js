import angular from 'angular';
import monthsByYearService from './months-by-year.service';

const monthsByYearModule = angular.module('app.service.monthsByYear', [])
    .service('monthsByYearService', monthsByYearService);

export default monthsByYearModule;
