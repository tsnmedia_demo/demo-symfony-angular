import seoContentTypesModule from './seo-content-types';

describe('seoContentTypesService', () => {
    beforeEach(window.module(
        seoContentTypesModule.name,
        $provide => {
            $provide.service('constantsService', () => {
                return {
                    get: value => value
                }
            });

            $provide.service('apiService', () => {
                return {
                    get: () => {}
                }
            });

            $provide.service('dataStoreService', () => {
                return {
                    getData: () => {}
                };
            });

            $provide.service('formatStringService', () => {
                return {
                    add: value => value
                };
            });
        }
    ));

    describe('Service', () => {
        let seoContentTypesService,
            dataStoreService,
            apiService;

        const projectData = {
            project: {
                contentTypes: [
                    'SEO_CONTENT_TYPE_DESCRIPTION',
                    'SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT',
                    'SEO_CONTENT_TYPE_REVIEW'
                ]
            }
        }

        const dataTypesWithStatus = {
            contentTypes: {
                SEO_CONTENT_TYPE_DESCRIPTION: {
                    isCompleted: true
                },
                SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT: {
                    isCompleted: false
                },
                SEO_CONTENT_TYPE_REVIEW: {
                    isCompleted: false
                }
            }
        }

        beforeEach(window.inject((
            _seoContentTypesService_,
            _dataStoreService_,
            _apiService_
        ) => {
            seoContentTypesService = _seoContentTypesService_;
            dataStoreService = _dataStoreService_;
            apiService = _apiService_;
        }));

        describe('getTypes', () => {
            it('tracks that returned types in right order', () => {
                expect(seoContentTypesService.getTypes()).toEqual([
                    'SEO_CONTENT_TYPE_DESCRIPTION',
                    'SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT',
                    'SEO_CONTENT_TYPE_REVIEW',
                    'SEO_CONTENT_TYPE_CONFIRMATION'
                ]);
            });
        });

        describe('getTypeState', () => {
            it('tracks that returned correct description state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_DESCRIPTION'))
                    .toBe('STATE_TOOLS_SEO_PROJECT_DESCRIPTION');
            });

            it('tracks that returned correct description qa state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_DESCRIPTION', true))
                    .toBe('STATE_TOOLS_SEO_QA_PROJECT_DESCRIPTION');
            });

            it('tracks that returned correct description writer state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_DESCRIPTION', true, 2))
                    .toBe('STATE_TOOLS_SEO_QA_PROJECT_WRITER_DESCRIPTION');
            });

            it('tracks that returned correct review state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_REVIEW'))
                    .toBe('STATE_TOOLS_SEO_PROJECT_REVIEWS');
            });

            it('tracks that returned correct review qa state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_REVIEW', true))
                    .toBe('STATE_TOOLS_SEO_QA_PROJECT_REVIEWS');
            });

            it('tracks that returned correct review writer state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_REVIEW', true, 2))
                    .toBe('STATE_TOOLS_SEO_QA_PROJECT_WRITER_REVIEWS');
            });

            it('tracks that returned correct confirmation state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_CONFIRMATION'))
                    .toBe('STATE_TOOLS_SEO_PROJECT_CONFIRMATION');
            });

            it('tracks that returned correct confirmation qa state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_CONFIRMATION', true))
                    .toBe('STATE_TOOLS_SEO_QA_PROJECT_CONFIRMATION');
            });

            it('tracks that returned correct confirmation writer state', () => {
                expect(seoContentTypesService.getTypeState('SEO_CONTENT_TYPE_CONFIRMATION', true, 2))
                    .toBe('STATE_TOOLS_SEO_QA_PROJECT_WRITER_CONFIRMATION');
            });
        });

        describe('getNextTypeState', () => {
            it('tracks that returned correct next state', () => {
                expect(seoContentTypesService.getNextTypeState(
                    ['SEO_CONTENT_TYPE_DESCRIPTION'],
                    'SEO_CONTENT_TYPE_DESCRIPTION'
                )).toBe('STATE_TOOLS_SEO_PROJECT_CONFIRMATION');
            });

            it('tracks that returned correct next qa state', () => {
                expect(seoContentTypesService.getNextTypeState(
                    ['SEO_CONTENT_TYPE_DESCRIPTION'],
                    'SEO_CONTENT_TYPE_DESCRIPTION',
                    true
                )).toBe('STATE_TOOLS_SEO_QA_PROJECT_CONFIRMATION');
            });

            it('tracks that returned correct next writer state', () => {
                expect(seoContentTypesService.getNextTypeState(
                    ['SEO_CONTENT_TYPE_DESCRIPTION'],
                    'SEO_CONTENT_TYPE_DESCRIPTION',
                    true,
                    2
                )).toBe('STATE_TOOLS_SEO_QA_PROJECT_WRITER_CONFIRMATION');
            });
        });

        describe('getUncompletedState', () => {
            it('tracks that returned next state in right order', () => {
                expect(seoContentTypesService.getUncompletedState(
                    {
                        SEO_CONTENT_TYPE_REVIEW: {
                            isCompleted: false
                        },
                        SEO_CONTENT_TYPE_DESCRIPTION: {
                            isCompleted: false
                        }
                    }
                )).toBe('STATE_TOOLS_SEO_PROJECT_DESCRIPTION');
            });

            it('tracks that returned correct next state', () => {
                expect(seoContentTypesService.getUncompletedState(
                    {
                        SEO_CONTENT_TYPE_DESCRIPTION: {
                            isCompleted: true
                        },
                        SEO_CONTENT_TYPE_REVIEW: {
                            isCompleted: false
                        }
                    }
                )).toBe('STATE_TOOLS_SEO_PROJECT_REVIEWS');
            });

            it('tracks that returned correct next qa state', () => {
                expect(seoContentTypesService.getUncompletedState(
                    {
                        SEO_CONTENT_TYPE_DESCRIPTION: {
                            isCompleted: true
                        },
                        SEO_CONTENT_TYPE_REVIEW: {
                            isCompleted: false
                        }
                    },
                    true
                )).toBe('STATE_TOOLS_SEO_QA_PROJECT_REVIEWS');
            });

            it('tracks that returned correct next writer state', () => {
                expect(seoContentTypesService.getUncompletedState(
                    {
                        SEO_CONTENT_TYPE_DESCRIPTION: {
                            isCompleted: true
                        },
                        SEO_CONTENT_TYPE_REVIEW: {
                            isCompleted: false
                        }
                    },
                    true,
                    2
                )).toBe('STATE_TOOLS_SEO_QA_PROJECT_WRITER_REVIEWS');
            });

            it('tracks that is autosave submit', () => {
                const isAutosave = seoContentTypesService.isAutosaveSubmit(
                    {
                        contentTypes: {
                            SEO_CONTENT_TYPE_DESCRIPTION: {
                                isCompleted: true
                            },
                            SEO_CONTENT_TYPE_REVIEW: {
                                isCompleted: false
                            }
                        }
                    },
                    'SEO_CONTENT_TYPE_DESCRIPTION',
                    true,
                    2
                );

                expect(isAutosave).toBe(true);
            });
        });

        describe('getProjectContentsType', () => {
            it('tracks that service got stored data', () => {
                spyOn(dataStoreService, 'getData');
                seoContentTypesService.getProjectContentsType(null, false);

                expect(dataStoreService.getData).toHaveBeenCalledWith(
                    'GET_SEO_PROJECT'
                );
            });

            it('tracks that service got data from api', () => {
                spyOn(apiService, 'get');
                seoContentTypesService.getProjectContentsType(false, true);

                expect(apiService.get).toHaveBeenCalledWith(
                    'API_SEO_PROJECT_CONTENT_TYPES_URI'
                );
            });

            it('tracks that service got qa data from api', () => {
                spyOn(apiService, 'get');
                seoContentTypesService.getProjectContentsType(true, true);

                expect(apiService.get).toHaveBeenCalledWith(
                    'API_SEO_QA_PROJECT_CONTENT_TYPES_URI'
                );
            });

            it('tracks that service return first uncompleted type (if completed)', () => {
                const state = seoContentTypesService.getFirstUncompletedTypeState(
                    projectData,
                    dataTypesWithStatus,
                    'SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT',
                    true,
                    true,
                    false
                );

                expect(state).toEqual(
                    'STATE_TOOLS_SEO_QA_PROJECT_WRITER_REVIEWS'
                );
            });

            it('tracks that service return first uncompleted type (if not completed)', () => {
                const state = seoContentTypesService.getFirstUncompletedTypeState(
                    projectData,
                    dataTypesWithStatus,
                    'SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT',
                    true,
                    true,
                    true
                );

                expect(state).toEqual(
                    'STATE_TOOLS_SEO_QA_PROJECT_WRITER_HOTEL_HIGHLIGHT'
                );
            });

            it('tracks that service return first uncompleted type (if not completed)', () => {
                const state = seoContentTypesService.getFirstUncompletedTypeState(
                    projectData,
                    dataTypesWithStatus,
                    'SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT',
                    true,
                    true,
                    true
                );

                expect(state).toEqual(
                    'STATE_TOOLS_SEO_QA_PROJECT_WRITER_HOTEL_HIGHLIGHT'
                );
            });
        });
    });
});
