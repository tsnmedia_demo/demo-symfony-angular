import angular from 'angular';
import seoContentTypesService from './seo-content-types.service';

const seoContentTypesModule = angular.module('app.service.seoContentTypes', [])
    .service('seoContentTypesService', seoContentTypesService);

export default seoContentTypesModule;
