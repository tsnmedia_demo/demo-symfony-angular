const seoContentTypesService = (
    $q,
    apiService,
    constantsService,
    dataStoreService,
    formatStringService
) => {
    const SEO_CONTENT_TYPE_DESCRIPTION = constantsService.get('SEO_CONTENT_TYPE_DESCRIPTION');
    const SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT = constantsService.get('SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT');
    const SEO_CONTENT_TYPE_REVIEW = constantsService.get('SEO_CONTENT_TYPE_REVIEW');
    const SEO_CONTENT_TYPE_CONFIRMATION = constantsService.get('SEO_CONTENT_TYPE_CONFIRMATION');
    const API_SEO_PROJECT_CONTENT_TYPES_URI = constantsService.get('API_SEO_PROJECT_CONTENT_TYPES_URI');
    const API_SEO_QA_PROJECT_CONTENT_TYPES_URI = constantsService.get('API_SEO_QA_PROJECT_CONTENT_TYPES_URI');
    const GET_SEO_PROJECT = constantsService.get('GET_SEO_PROJECT');
    const GET_SEO_TASK_ID = constantsService.get('GET_SEO_TASK_ID');

    const contentTypesOrder = [
        SEO_CONTENT_TYPE_DESCRIPTION,
        SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT,
        SEO_CONTENT_TYPE_REVIEW,
        SEO_CONTENT_TYPE_CONFIRMATION
    ];

    const contentTypesStates = {
        [SEO_CONTENT_TYPE_DESCRIPTION]: [
            constantsService.get('STATE_TOOLS_SEO_PROJECT_DESCRIPTION'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_DESCRIPTION'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_WRITER_DESCRIPTION')
        ],
        [SEO_CONTENT_TYPE_HOTEL_HIGHLIGHT]: [
            constantsService.get('STATE_TOOLS_SEO_PROJECT_HOTEL_HIGHLIGHT'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_HOTEL_HIGHLIGHT'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_WRITER_HOTEL_HIGHLIGHT')
        ],
        [SEO_CONTENT_TYPE_REVIEW]: [
            constantsService.get('STATE_TOOLS_SEO_PROJECT_REVIEWS'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_REVIEWS'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_WRITER_REVIEWS')
        ],
        [SEO_CONTENT_TYPE_CONFIRMATION]: [
            constantsService.get('STATE_TOOLS_SEO_PROJECT_CONFIRMATION'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_CONFIRMATION'),
            constantsService.get('STATE_TOOLS_SEO_QA_PROJECT_WRITER_CONFIRMATION')
        ]
    };

    const getTypes = () => contentTypesOrder;

    const sortTypes = (a, b) => {
        return contentTypesOrder.indexOf(a) - contentTypesOrder.indexOf(b);
    };

    const filterTypes = (projectContentTypes, type) => {
        return projectContentTypes.indexOf(type) !== -1 || type === SEO_CONTENT_TYPE_CONFIRMATION;
    };

    const getTypeState = (type, isSeoQA, isWriter) => {
        const stateType = isWriter ? 2 : isSeoQA ? 1 : 0;

        return contentTypesStates[type][stateType];
    };

    const getNextTypeState = (projectContentTypes, currentType, isSeoQA, isWriter) => {
        const filteredTypes = contentTypesOrder.filter(filterTypes.bind(null, projectContentTypes));
        const nextTypeIndex = filteredTypes.indexOf(currentType) + 1;
        const nextType = filteredTypes[nextTypeIndex];

        return getTypeState(nextType, isSeoQA, isWriter);
    };

    const getUncompletedState = (projectContentTypes, isSeoQA, isWriter) => {
        const sortedUncompletedTypes = Object.keys(projectContentTypes)
            .filter(key => !projectContentTypes[key].isCompleted)
            .sort(sortTypes);

        return getTypeState(sortedUncompletedTypes[0], isSeoQA, isWriter);
    };

    /**
     * @param {boolean|null} isSeoQa
     * @param {boolean} isActionCompleted
     *
     * @return {Array}
     */
    const getProjectContentsType = (isSeoQa, isActionCompleted) => {
        const promises = [dataStoreService.getData(GET_SEO_PROJECT)];

        if (isActionCompleted) {
            const taskId = dataStoreService.getData(GET_SEO_TASK_ID);
            const apiUrlConstant = isSeoQa ? API_SEO_QA_PROJECT_CONTENT_TYPES_URI : API_SEO_PROJECT_CONTENT_TYPES_URI;
            const urlFormat = formatStringService.add(apiUrlConstant, {id: taskId});

            promises.push(apiService.get(urlFormat));
        }

        return $q.all(promises);
    };

    /**
     * @param {Object} data
     * @param {String} currentContentType
     * @param {boolean|null} isSeoQa
     * @param {boolean|null} isWriter
     *
     * @return {boolean}
     */
    const isAutosaveSubmit = (data, currentContentType, isSeoQa, isWriter) => {
        const uncompletedState = getUncompletedState(
            data.contentTypes,
            isSeoQa,
            isWriter
        );

        return uncompletedState !== currentContentType;
    };

    /**
     * @param {Object} dataProjectTypes
     * @param {Object} dataTypesWithStatus
     * @param {String} currentType
     * @param {boolean|null} isSeoQa
     * @param {boolean|null} isWriter
     * @param {boolean|null} isNotActionCompleted
     *
     * @return {String}
     */
    const getFirstUncompletedTypeState = (
        dataProjectTypes,
        dataTypesWithStatus,
        currentType,
        isSeoQa,
        isWriter,
        isNotActionCompleted
    ) => {
        let state = getNextTypeState(
            dataProjectTypes.project.contentTypes,
            currentType,
            isSeoQa,
            isWriter
        );

        if (isNotActionCompleted && dataTypesWithStatus) {
            state = getUncompletedState(
                dataTypesWithStatus.contentTypes,
                isSeoQa,
                isWriter
            );
        }

        return state;
    };

    return {
        getTypes,
        getTypeState,
        getNextTypeState,
        getUncompletedState,
        getProjectContentsType,
        getFirstUncompletedTypeState,
        isAutosaveSubmit
    };
};

export default seoContentTypesService;
