import angular from 'angular';
import errorLogService from './error-log.service';

const errorLogModule = angular.module('app.service.errorLog', [])
    .service('errorLogService', errorLogService);

export default errorLogModule;
