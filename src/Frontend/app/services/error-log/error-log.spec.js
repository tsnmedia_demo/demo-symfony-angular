import angular from 'angular';
import errorLogModule from './error-log';
import servicesModule from 'services/services';
import stacktraceService from 'services/stacktrace/stacktrace';

describe('errorLogService', () => {
    beforeEach(window.module(
        servicesModule.name,
        stacktraceService.name,
        ($provide) => {
            const windowObj = {};
            windowObj.location = {};
            windowObj.location.href = 'http://test.trv';
            windowObj.globalConfig = {};
            windowObj.globalConfig.env = 'production';
            windowObj.navigator = {
                userAgent: ''
            };

            $provide.value('$window', windowObj);
            $provide.value('$document', angular.element(document));
            $provide.service('debounceService', () => {
                return (fn) => {
                    return (obj, exception) => {
                        return fn.call(null, obj, exception);
                    };
                };
            });
            $provide.service('stacktraceService', ($q) => {
                return {
                    fromError: () => {
                        const deferred = $q.defer();

                        deferred.resolve(['@stackFrame', 'array']);

                        return deferred.promise;
                    }
                };
            });
            $provide.service('dataStoreService', () => {
                return {
                    setData: () => '',
                    getData: (key) => {
                        return key === 'DATA_STORE_CANCELABLE_PROMISES' ? [] : {current: ''}
                    }
                };
            });

            $provide.service('$state', () => {});
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.error-log', () => {
            expect(errorLogModule.name).toBe('app.service.errorLog');
        });
    });

    function injector() {
        let $document,
            $log,
            $window,
            apiService,
            calledWith,
            constantsService,
            errorLogService,
            httpBackend,
            rootScope;

        window.inject((
            _$document_,
            _$httpBackend_,
            _$log_,
            _$rootScope_,
            _$window_,
            _apiService_,
            _constantsService_,
            _errorLogService_
        ) => {
            $document = _$document_;
            $log = _$log_;
            $window = _$window_;
            apiService = _apiService_;
            calledWith = (message) => constantsService.get('API_LOG_URI') +
                '?message=' + JSON.stringify(message);
            constantsService = _constantsService_;
            errorLogService = _errorLogService_;
            httpBackend = _$httpBackend_;
            rootScope = _$rootScope_;

        });

        return [
            $document,
            $log,
            $window,
            apiService,
            calledWith,
            constantsService,
            errorLogService,
            httpBackend,
            rootScope
        ];
    }

    describe('Service', () => {
        let $document,
            $log,
            $window,
            apiService,
            calledWith,
            constantsService,
            errorLogService,
            httpBackend,
            message,
            rootScope;

        beforeEach(() => {
            message = {
                error: {
                    message: 'test',
                    stack: undefined,
                    trace: '@stackFrame | array'
                },
                url: 'http://test.trv'
            };
        });

        describe('Errors before do the post', () => {
            beforeEach(window.module(
                ($provide) => {
                    $provide.service('stacktraceService', ($q) => {
                        return {
                            fromError: () => {
                                const deferred = $q.defer();

                                deferred.reject();

                                return deferred.promise;
                            }
                        };
                    });
                }
            ));

            beforeEach(() => {
                [
                    $document,
                    $log,
                    $window,
                    apiService,
                    calledWith,
                    constantsService,
                    errorLogService,
                    httpBackend,
                    rootScope
                ] = injector();

                spyOn($log, 'error').and.callThrough();
            });

            it('should the exceptionHandler call $log.error when because an error is cached', () => {
                errorLogService.exceptionHandler(message);
                rootScope.$digest();

                expect($log.error).toHaveBeenCalledWith(message);
            });

            it('should the exceptionHandler call $log.error when globalConfig.env = dev', () => {
                $window.globalConfig.env = 'dev';

                errorLogService.exceptionHandler(message.error);
                rootScope.$digest();

                expect($log.error).toHaveBeenCalledWith(message.error);
            });
        });

        describe('errorHandler', () => {
            beforeEach(window.module(
                ($provide) => {
                    $provide.service('stacktraceService', ($q) => {
                        return {
                            fromError: () => {
                                const deferred = $q.defer();

                                deferred.resolve(['@stackFrame', 'array']);

                                return deferred.promise;
                            }
                        };
                    });
                }
            ));

            beforeEach(() => {
                [
                    $document,
                    $log,
                    $window,
                    apiService,
                    calledWith,
                    constantsService,
                    errorLogService,
                    httpBackend,
                    rootScope
                ] = injector();

                spyOn(apiService, 'post').and.callThrough();
                spyOn($log, 'error').and.callThrough();
            });

            it('should the exceptionHandler post the error data to the api log for IE11', () => {
                $document.documentMode = 'ie11';
                message.error.trace = null;

                errorLogService.exceptionHandler(message.error);
                httpBackend.whenPOST(constantsService.get('API_LOG_URI'), {message: message}).respond(200);
                httpBackend.flush();

                expect(apiService.post).toHaveBeenCalledWith(constantsService.get('API_LOG_URI'), {message: message});
            });

            it('should the exceptionHandler post the error data to the api log for none IE11', () => {
                errorLogService.exceptionHandler(message.error);
                httpBackend.whenPOST(constantsService.get('API_LOG_URI'), {message: message}).respond(200);
                httpBackend.flush();

                expect(apiService.post).toHaveBeenCalledWith(constantsService.get('API_LOG_URI'), {message: message});
            });

            it('should the exceptionHandler return a $log.error', () => {
                errorLogService.exceptionHandler(message.error);
                httpBackend.whenPOST(constantsService.get('API_LOG_URI'), {message: message}).respond(200);
                httpBackend.flush();

                expect($log.error).toHaveBeenCalledWith(message.error);
            });

            it('should the exceptionHandler return a $log.error for IE11', () => {
                $document.documentMode = 'ie11';
                message.error.trace = null;

                errorLogService.exceptionHandler(message.error);
                httpBackend.whenPOST(constantsService.get('API_LOG_URI'), {message: message}).respond(200);
                httpBackend.flush();

                expect($log.error).toHaveBeenCalledWith(message.error);
            });
        });
    });
});
