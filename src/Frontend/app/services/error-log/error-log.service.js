const errorLogService = (
    $document,
    $injector,
    $log,
    $window,
    constantsService,
    debounceService,
    stacktraceService
) => {
    const errorBack = (exception) => {
        $log.error(exception);
    };

    const submitLog = (obj, exception, stackFrames) => {
        const apiService = $injector.get('apiService');

        obj.url = $window.location.href;
        obj.error.trace = Array.isArray(stackFrames) ?
            stackFrames.map((sf) => sf.toString()).join(' | ') : null;

        apiService.post(constantsService.get('API_LOG_URI'), {message: obj})
            .then(errorBack.bind(null, exception));
    };

    const getStack = (obj, exception) => {
        stacktraceService.fromError(exception)
            .then(submitLog.bind(null, obj, exception))
            .catch(errorBack.bind(null, exception));
    };

    const errorHandler = (obj, exception) => {
        // @see: https://msdn.microsoft.com/en-us/library/cc196988(v=vs.85).aspx
        // @see: https://technet.microsoft.com/en-us/itpro/internet-explorer/ie11-deploy-guide/deprecated-document-modes
        const isIE = !!$document.documentMode;

        if (isIE) {
            // IE11 do not support promises and is a dependency of stacktraceService
            // @see http://caniuse.com/#feat=promises
            submitLog(obj, exception);
        } else {
            getStack(obj, exception);
        }
    };

    const exceptionHandler = (exception) => {
        const debounceLogs = debounceService(errorHandler, 2000);

        if ($window.globalConfig.env === 'production') {
            debounceLogs({
                error: {
                    message: exception.message,
                    stack: exception.stack
                }
            }, exception);
        } else {
            errorBack(exception);
        }
    };

    return {
        exceptionHandler
    };
};

export default errorLogService;
