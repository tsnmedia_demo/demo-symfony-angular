import dateFormatModule from './date-format';
import servicesModule from 'services/services';

describe('dateFormatService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.date-format', () => {
            expect(dateFormatModule.name).toBe('app.service.dateFormat');
        });
    });

    describe('Service', () => {
        let dateFormatService;

        beforeEach(window.inject((_dateFormatService_) => {
            dateFormatService = _dateFormatService_;
        }));

        it('should format a date to the default crowd format adding 0 before day and/or month if they has one digit', () => {
            expect(dateFormatService.set('2015-01-01T00:00:00')).toBe('01.01.2015');
        });

        it('should format a date to the default crowd format', () => {
            expect(dateFormatService.set('2015-10-10T00:00:00')).toBe('10.10.2015');
        });

        it('should throw an error if it do not match with the Date Time String Format pattern', () => {
            expect(() => dateFormatService.set('2015-10-01T00:00:'))
                .toThrowError(/not have the Date Time String Format/);
        });
    });
});
