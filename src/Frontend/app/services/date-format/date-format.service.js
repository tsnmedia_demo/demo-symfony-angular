const dateFormatService = ($filter) => {
    const set = (date) =>{
        if (isNaN(new Date(date).getTime()) === true) {
            throw new Error(`The date do not have the Date Time String Format.`);
        }

        return $filter('date')(date, 'dd.MM.yyyy');
    };

    return {
        set
    };
};

export default dateFormatService;
