import angular from 'angular';
import dateFormatService from './date-format.service';

const dateFormatModule = angular.module('app.service.dateFormat', [])
    .service('dateFormatService', dateFormatService);

export default dateFormatModule;
