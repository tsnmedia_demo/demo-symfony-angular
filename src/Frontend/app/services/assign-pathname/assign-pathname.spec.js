import assignPathnameModule from './assign-pathname';
import servicesModule from 'services/services';

describe('assignPathnameService', () => {
    beforeEach(window.module(
        servicesModule.name,
        ($provide) => {
            const windowObj = {};
            windowObj.location = {};
            windowObj.location.pathname = 'test';

            $provide.value('$window', windowObj);
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.assign-pathname', () => {
            expect(assignPathnameModule.name).toBe('app.service.assignPathname');
        });
    });

    describe('Service', () => {
        let $timeout, assignPathnameService;

        beforeEach(window.inject((_$timeout_, _assignPathnameService_) => {
            $timeout = _$timeout_;
            assignPathnameService = _assignPathnameService_;
        }));

        it('should assign a node pathname to an object', () => {
            const obj = {pathname: ''};

            assignPathnameService.assignTo(obj);
            $timeout.flush();

            expect(obj.pathname).toBe('test');
        });
    });
});
