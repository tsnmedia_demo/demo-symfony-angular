const assignPathnameService = ($timeout, $window) => {
    const assignPathname = (context) => {
        context.pathname = $window.location.pathname;
    };

    const assignPathnameTo = (context) => {
        $timeout(assignPathname.bind(null, context), 0);
    };

    return {
        assignTo: assignPathnameTo
    };
};

export default assignPathnameService;
