import angular from 'angular';
import assignPathnameService from './assign-pathname.service';

const assignPathnameModule = angular.module('app.service.assignPathname', [])
    .service('assignPathnameService', assignPathnameService);

export default assignPathnameModule;
