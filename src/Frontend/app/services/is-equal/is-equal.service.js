import isEqual from 'lodash.isequal';

const isEqualService = () => {
    return isEqual;
};

export default isEqualService;
