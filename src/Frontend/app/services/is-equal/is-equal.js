import angular from 'angular';
import isEqualService from './is-equal.service';

const isEqualModule = angular.module('app.service.isEqual', [])
    .service('isEqualService', isEqualService);

export default isEqualModule;
