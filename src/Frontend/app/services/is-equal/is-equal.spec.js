import isEqualModule from './is-equal';

describe('isEqualService', () => {
    beforeEach(window.module(isEqualModule.name));

    describe('Module', () => {
        it('has as a name app.service.isEqual', () => {
            expect(isEqualModule.name).toBe('app.service.isEqual');
        });
    });
});
