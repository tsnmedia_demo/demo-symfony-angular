import angular from 'angular';
import countriesService from './countries.service';

const countriesModule = angular.module('app.service.countries', [])
    .service('countriesService', countriesService);

export default countriesModule;
