const countriesService = (
    $window,
    constantsService,
    apiService
) => {

    const API_GET_COUNTRIES_LIST_URI = constantsService.get('API_GET_COUNTRIES_LIST_URI');
    const i18nCountries = $window.globalConfig.i18nCountries;
    let countriesPromise = {};

    const updateList = (response) => {
        const countries = [];

        response.forEach((countryObj) => {
            if(i18nCountries[countryObj.code]){
                countries.push({
                    code: countryObj.code,
                    name: i18nCountries[countryObj.code],
                    id: countryObj.countryId
                });
            } else {
                throw new Error(`i18nCountries: Country code "${countryObj.code}" not found.`);
            }
        });

        countries.sort((prevCountry, nextCountry) => {
            return prevCountry.name === nextCountry.name ? 0 :
                 prevCountry.name > nextCountry.name ? 1 : -1;
        });

        return countries;
    };

    const updateData = () => {
        return apiService.get(API_GET_COUNTRIES_LIST_URI).then(updateList);
    };

    const getPromise = () => {
        if(!Object.keys(countriesPromise).length){
            countriesPromise = updateData();
        }

        return countriesPromise;
    };

    return {
        getPromise
    };
};

export default countriesService;
