import servicesModule from 'services/services';

describe('countriesService', () => {
    beforeEach(window.module(
        servicesModule.name,
        $provide => {
            $provide.service('$window', () => {
                return {
                    globalConfig: {
                        i18nCountries: {
                            'FN': 'First Country',
                            'SN': 'Second Country',
                            'TN': 'Third Country'
                        }
                    }
                };
            });

            $provide.service('constantsService', () => {
                return {
                    get: value => value
                };
            });

            $provide.service('apiService', () => {
                return {
                    get: () => {}
                };
            });
        }
    ));

    describe('getPromise', () => {
        let countriesService,
            apiService,
            response;

        const expectedResult = [
            {code: 'FN', name: 'First Country', id: '1'},
            {code: 'SN', name: 'Second Country', id: '2'}
        ];

        const thenFunction = (response, callback) => {
            const result = callback(response);

            return {
                then: thenFunction.bind(null, result)
            };
        };

        beforeEach(window.inject((
            _countriesService_,
            _apiService_
        ) => {
            countriesService = _countriesService_;
            apiService = _apiService_;
            response = [
                { code: 'FN', countryId: '1' },
                { code: 'SN', countryId: '2' }
            ];

            spyOn(apiService, 'get').and.returnValue({
                then: thenFunction.bind(null, response)
            });
        }));

        it('track that apiService was called with arguments', () => {
            countriesService.getPromise();

            expect(apiService.get).toHaveBeenCalledWith('API_GET_COUNTRIES_LIST_URI');
        });

        it('track that the service generate allowed array of countries', () => {
            let countries;

            countriesService.getPromise().then((response) => {
                countries = response;
            });

            expect(countries).toEqual(expectedResult);
        });

        it('track that the countries array is sorted', () => {
            response[0] = { code: 'SN', countryId: '2' };
            response[1] = { code: 'FN', countryId: '1' };
            let countries;

            countriesService.getPromise().then((response) => {
                countries = response;
            });

            expect(countries).toEqual(expectedResult);
        });

        it('track that error has been thrown if language didn\'t find', () => {
            response[0] = { code: 'CODE_NOT_EXIST', countryId: '' };

            expect(() => { countriesService.getPromise() })
                .toThrowError('i18nCountries: Country code "CODE_NOT_EXIST" not found.');
        })
    });
});
