import angular from 'angular';
import assignService from './assign.service';

const assignModule = angular.module('app.service.assign', [])
    .service('assignService', assignService);

export default assignModule;
