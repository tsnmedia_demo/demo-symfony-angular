import assignModule from './assign';

describe('assignService', () => {
    beforeEach(window.module(assignModule.name));

    describe('Module', () => {
        it('has as a name app.service.assign', () => {
            expect(assignModule.name).toBe('app.service.assign');
        });
    });
});
