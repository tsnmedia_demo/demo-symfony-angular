import assign from 'lodash.assign';

const assignService = () => {
    return assign;
};

export default assignService;
