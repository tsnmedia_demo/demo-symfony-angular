import angular from 'angular';
import objectReferenceService from './object-reference.service';

const objectReferenceModule = angular.module('app.service.objectReference', [])
    .service('objectReferenceService', objectReferenceService);

export default objectReferenceModule;
