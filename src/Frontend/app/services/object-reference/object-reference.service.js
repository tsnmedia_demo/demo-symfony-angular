const objectReferenceService = () => {
    const getObjectItem = (dotNotation, initialObject) => {
        return dotNotation.split('.').reduce(
            (previousObject, key) => previousObject[key], initialObject
        );
    };

    const updateObjectItem = (dotNotation, initialObject, value) => {
        return dotNotation.split('.').reduce(
            (previousObject, key, index, arr) => {
                const isLast = index === arr.length - 1;

                if (isLast && previousObject[key] !== value) {
                    previousObject[key] = value;
                } else if (previousObject[key] === undefined) {
                    previousObject[key] = {};
                }

                return isLast ? initialObject : previousObject[key];
            }, initialObject
        );
    };

    const setObjectItem = (dotNotation, initialObject, value) => {
        let newInitialObj = JSON.parse(JSON.stringify(initialObject));

        return updateObjectItem(dotNotation, newInitialObj, value);
    };

    return {
        get: getObjectItem,
        set: setObjectItem,
        update: updateObjectItem
    };
};

export default objectReferenceService;
