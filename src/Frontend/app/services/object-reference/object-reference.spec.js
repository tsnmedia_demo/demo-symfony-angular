import objectReferenceModule from './object-reference';
import servicesModule from 'services/services';

describe('objectReferenceService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.object-reference', () => {
            expect(objectReferenceModule.name).toBe('app.service.objectReference');
        });
    });

    describe('Service', () => {
        let obj, objectReferenceService;

        beforeEach(window.inject((_objectReferenceService_) => {
            objectReferenceService = _objectReferenceService_;

            obj = {
                test: {
                    child: {
                        inner: 'test'
                    }
                }
            };
        }));

        it('should return the value as the object path is indicated on the first argument', () => {
            expect(objectReferenceService.get('test.child.inner', obj)).toBe('test');
        });

        it('should return an object with the value specified changed on the last item dotted', () => {
            const newObj = objectReferenceService.set('test.child.inner', obj, 'new');

            expect(newObj.test.child.inner).toBe('new');
        });

        it('should return an object with the value specified created on the last item dotted', () => {
            const newObj = objectReferenceService.set('test.child.newInner', obj, 'new');

            expect(newObj).toEqual({
                test: {
                    child: {
                        inner: 'test',
                        newInner: 'new'
                    }
                }
            });
        });

        it('should return the object updated with the value specified changed', () => {
            objectReferenceService.update('test.child.inner', obj, 'new');

            expect(obj.test.child.inner).toBe('new');
        });

        it('should not return the object updated with the value specified changed', () => {
            objectReferenceService.set('test.child.inner', obj, 'new');

            expect(obj.test.child.inner).not.toBe('new');
        });
    });
});
