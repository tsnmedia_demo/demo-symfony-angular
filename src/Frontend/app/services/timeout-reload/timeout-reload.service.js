const timeoutReloadService = (
    $timeout,
    dataStoreService
) => {
    const reload = (time) => {
        var timeout = $timeout(() => {
            // We are using storing the state to run the tests of this service
            const state = dataStoreService.getData('$state');

            $timeout.cancel(timeout);
            state.reload(state.current);
        }, time || 2000);
    };


    return {
        reload
    };
};

export default timeoutReloadService;
