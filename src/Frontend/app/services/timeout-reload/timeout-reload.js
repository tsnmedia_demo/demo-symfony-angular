import angular from 'angular';
import timeoutReloadService from './timeout-reload.service';

const timeoutReloadModule = angular.module('app.service.timeoutReload', [])
    .service('timeoutReloadService', timeoutReloadService);

export default timeoutReloadModule;
