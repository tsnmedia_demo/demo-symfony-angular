import timeoutReloadModule from './timeout-reload';
import servicesModule from 'services/services';

describe('timeoutReloadService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.timeout-reload', () => {
            expect(timeoutReloadModule.name).toBe('app.service.timeoutReload');
        });
    });

    describe('Service', () => {
        let timeoutReloadService,
            buttonsStatesService,
            dataStoreService,
            originalTimeout,
            stateObject = {
                current: 'test',
                reload: () => {}
            };

        beforeEach(window.inject((
            _timeoutReloadService_,
            _buttonsStatesService_,
            _dataStoreService_
        ) => {
            timeoutReloadService = _timeoutReloadService_;
            buttonsStatesService = _buttonsStatesService_;
            dataStoreService = _dataStoreService_;

            originalTimeout = jasmine.DEFAULT_TIMEOUT_INTERVAL;
            jasmine.DEFAULT_TIMEOUT_INTERVAL = 0;

            dataStoreService.setData('$state', stateObject);
        }));

        describe('reload method state.reload', () => {
            let timeout, state;

            beforeEach(window.inject((
                _$timeout_,
                _timeoutReloadService_
            ) => {
                timeoutReloadService = _timeoutReloadService_;
                state = dataStoreService.getData('$state');
                timeout = _$timeout_;

                spyOn(state, 'reload').and.callThrough();
            }));

            it('should call state.reload when reload is called with the proper params', () => {
                timeoutReloadService.reload();

                timeout.flush();

                expect(state.reload).toHaveBeenCalledWith(stateObject.current);
            });
        });

        describe('reload method timeout.cancel', () => {
            let timeout;

            beforeEach(window.inject((
                _$timeout_,
                _timeoutReloadService_
            ) => {
                timeoutReloadService = _timeoutReloadService_;
                timeout = _$timeout_;

                spyOn(timeout, 'cancel').and.callThrough();
            }));

            it('should call timeout.cancel when reload is called with the proper params', () => {
                timeoutReloadService.reload();

                timeout.flush();

                expect(timeout.cancel).toHaveBeenCalled();
            });
        });
    });
});
