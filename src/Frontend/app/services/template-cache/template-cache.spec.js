import templateCacheModule from './template-cache';
import servicesModule from 'services/services';

describe('templateCacheService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.template-cache', () => {
            expect(templateCacheModule.name).toBe('app.service.templateCache');
        });
    });

    describe('Service', () => {
        let templateCacheService, templateTest, templateOtherTest, $templateCache;

        beforeEach(window.inject((_$templateCache_, _templateCacheService_) => {
            $templateCache = _$templateCache_;
            templateCacheService = _templateCacheService_;

            templateTest = '<div>test</div>';
            templateOtherTest = '<div>other test</div>';

            templateCacheService.set([
                {'test.html': templateTest},
                {'other-test.html': templateOtherTest}
            ]);
        }));

        it('should return test.html content', () => {
            expect($templateCache.get('test.html')).toBe(templateTest);
        });

        it('should return other-test.html content', () => {
            expect($templateCache.get('other-test.html')).toBe(templateOtherTest);
        });

        it('should not be undefined template undefined.html because is not cached', () => {
            expect($templateCache.get('undefined.html')).toBeUndefined();
        });
    });
});
