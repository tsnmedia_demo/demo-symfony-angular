const templateCacheService = ($templateCache) => {
    const eachTemplateCache = (obj) => {
        const key = Object.keys(obj)[0];

        $templateCache.put(key, obj[key]);
    };

    const setTemplateCache = (arr) => {
        arr.forEach(eachTemplateCache);
    };

    return {
        set: setTemplateCache
    };
};

export default templateCacheService;
