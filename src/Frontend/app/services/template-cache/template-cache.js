import angular from 'angular';
import templateCacheService from './template-cache.service';

const templateCacheModule = angular.module('app.service.templateCache', [])
    .service('templateCacheService', templateCacheService);

export default templateCacheModule;
