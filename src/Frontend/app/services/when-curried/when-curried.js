import angular from 'angular';
import whenCurriedService from './when-curried.service';

const whenCurriedModule = angular.module('app.service.whenCurried', [])
    .service('whenCurriedService', whenCurriedService);

export default whenCurriedModule;
