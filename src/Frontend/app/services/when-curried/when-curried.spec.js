import whenCurriedModule from './when-curried';
import servicesModule from 'services/services';

describe('whenCurriedService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.when-curried', () => {
            expect(whenCurriedModule.name).toBe('app.service.whenCurried');
        });
    });

    describe('Service', () => {
        let whenCurriedService;

        beforeEach(window.inject((_whenCurriedService_) => {
            whenCurriedService = _whenCurriedService_;
        }));

        it('should return test because is true', () => {
            const whenTrue = whenCurriedService(true);

            expect(whenTrue(() => 'test')).toBe('test');
        });

        it('should return null because is false', () => {
            const whenTrue = whenCurriedService(false);

            expect(whenTrue(() => 'test')).toBe(null);
        });
    });
});
