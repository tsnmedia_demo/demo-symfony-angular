const whenCurriedService = () => {
    return condition => callback => condition ? callback() : null;
};

export default whenCurriedService;
