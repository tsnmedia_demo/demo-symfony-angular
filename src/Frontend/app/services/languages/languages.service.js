const languagesService = (
    $window
) => {

    const CJKLANGUAGES = [
        'ja', 'zh', 'ko'
    ];

    const get = (languageCode) => {
        return $window.globalConfig.i18nLanguages[languageCode];
    };

    const isSet = (languageCode) => {
        return $window.globalConfig.i18nLanguages[languageCode] ? true : false;
    };

    const isCJKLanguage = (languagesCode) => {
        return CJKLANGUAGES.indexOf(languagesCode) !== -1;
    };

    return {
        get,
        isSet,
        isCJKLanguage
    };
};

export default languagesService;
