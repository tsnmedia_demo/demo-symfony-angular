import languagesModule from './languages';

describe('languagesService', () => {
    beforeEach(window.module(languagesModule.name));

    describe('Service', () => {
        let languagesService,
            windowService;

        beforeEach(window.inject((
            _languagesService_,
            $window
        ) => {
            languagesService = _languagesService_;
            windowService = $window;

            windowService.globalConfig = {
                i18nLanguages: {
                    en: 'enTest',
                    es: 'esTest'
                }
            }
        }));

        describe('on get', () => {
            it('tracks that service return language data', () => {
                const langData = languagesService.get('en');

                expect(langData).toBe(windowService.globalConfig.i18nLanguages.en);
            });
        });

        describe('on isset', () => {
            it('tracks that service return language data', () => {
                expect(languagesService.isSet('en')).toBe(true);
            });

            it('tracks that service return language data', () => {
                expect(languagesService.isSet('de')).toBe(false);
            });
        });

        describe('on is CJK languages', () => {
            it('tracks that service return CJK language', () => {
                expect(languagesService.isCJKLanguage('ja')).toBe(true);
            });

            it('tracks that service return CJK language', () => {
                expect(languagesService.isCJKLanguage('en')).toBe(false);
            });
        });
    });
});