import angular from 'angular';
import languagesService from './languages.service';

const languagesModule = angular.module('app.service.languages', [])
    .service('languagesService', languagesService);

export default languagesModule;
