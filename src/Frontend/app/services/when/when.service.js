const whenService = () => {
    return (condition, callback) => condition ? callback : () => null;
};

export default whenService;
