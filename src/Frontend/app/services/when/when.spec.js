import whenModule from './when';
import servicesModule from 'services/services';

describe('whenService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.when', () => {
            expect(whenModule.name).toBe('app.service.when');
        });
    });

    describe('Service', () => {
        let whenService;

        beforeEach(window.inject((_whenService_) => {
            whenService = _whenService_;
        }));

        it('should return test because is true', () => {
            const whenTrueTest = whenService(true, () => 'test');

            expect(whenTrueTest()).toBe('test');
        });

        it('should return null because is false', () => {
            const whenTrueTest = whenService(false, () => 'test');

            expect(whenTrueTest()).toBeNull();
        });
    });
});
