import angular from 'angular';
import whenService from './when.service';

const whenModule = angular.module('app.service.when', [])
    .service('whenService', whenService);

export default whenModule;
