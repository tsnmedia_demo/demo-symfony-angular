const notificationsService = (
    dataStoreService,
    constantsService,
    i18nService
) => {
    const serverErrorCodeStringBuilder = (description) => {
        let value;

        if (typeof description === 'string') {
            value = i18nService.get(description);
        } else if (description && description.message) {
            value = i18nService.get(description.message, { value: description.value });
        }

        return value;
    };

    const setNotificationToDataStoreService = (type, description, title) => {
        if (!/help|warning|error|success/.test(type)) {
            throw new Error(`notificationsService, ${type} is not one of the keys allow to use as a type`);
        }

        const dataStore = constantsService.get('DATA_STORE_NOTIFICATIONS');
        const message = {
            title: title || null,
            type: type,
            description: serverErrorCodeStringBuilder(description)
        };

        dataStoreService.setData(dataStore, message);
    };

    const getNotificationFromDataStoreService = () => {
        return dataStoreService.getData(
            constantsService.get('DATA_STORE_NOTIFICATIONS')
        );
    };

    return {
        set: setNotificationToDataStoreService,
        get: getNotificationFromDataStoreService
    };
};

export default notificationsService;
