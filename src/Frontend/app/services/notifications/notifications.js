import angular from 'angular';
import notificationsService from './notifications.service';

const notificationsModule = angular.module('app.service.notifications', [])
    .service('notificationsService', notificationsService);

export default notificationsModule;
