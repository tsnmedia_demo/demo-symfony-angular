import notificationsModule from './notifications';
import servicesModule from 'services/services';

describe('notificationsService', () => {

    beforeEach(window.module(
        servicesModule.name,
        ($provide) => {
            const windowObj = {};
            windowObj.globalConfig = {};
            windowObj.globalConfig.i18n = {
                '_message.help.settings_': '"${value}", test message help settings'
            };

            $provide.value('$window', windowObj);
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.notifications', () => {
            expect(notificationsModule.name).toBe('app.service.notifications');
        });
    });

    describe('Service', () => {
        let key,
            notificationsService,
            dataStoreService,
            constantsService,
            i18nService;

        const typeList = ['help', 'success', 'warning', 'error'];

        beforeEach(window.inject((
            _notificationsService_,
            _dataStoreService_,
            _constantsService_,
            _i18nService_
        ) => {
            notificationsService = _notificationsService_;
            dataStoreService = _dataStoreService_;
            constantsService = _constantsService_;
            i18nService = _i18nService_;

            key = constantsService.get('I18N_MESSAGES_HELP_SETTINGS');
        }));


        describe('set', () => {
            typeList.forEach((item) => {
                it(`should return an object with "${item}" as type`, () => {

                    notificationsService.set(item, key);

                    expect(
                        dataStoreService.getData(
                            constantsService.get('DATA_STORE_NOTIFICATIONS')
                        ).type
                    ).toBe(item);
                });
            });

            it('should return an exception when the type is not one of the keys allow to use as a type', () => {
                expect(() => {
                    notificationsService.set('test', key);
                }).toThrowError(/test is not one of the keys allow to use as a type/);
            });

            it('should return a error message builded with the value and the message', () => {
                notificationsService.set('error', {
                    message: '_message.help.settings_',
                    value: 'test text'
                });

                expect(notificationsService.get().description)
                    .toBe('"test text", test message help settings');
            });

            it('should return undefined because we are not setting anything', () => {
                notificationsService.set('error', {});

                expect(notificationsService.get())
                    .toEqual({ title: null, type: 'error', description: undefined });
            });
        });

        describe('get', () => {
            it(`should return an object with "${typeList[0]}" as type`, () => {
                notificationsService.set(typeList[0], key);
                expect(notificationsService.get().type).toBe(typeList[0]);
            });

            it('should return an object with the same description has teh translation', () => {
                notificationsService.set(typeList[0], key);
                expect(notificationsService.get().description)
                    .toBe(i18nService.get(key));
            });
        });
    });
});
