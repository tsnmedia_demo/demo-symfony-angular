const mappingSubmitDataService = (
    isEqualService,
    isPlainObjectService,
    objectReferenceService
) => {
    const trampoline = (fn) => {
        while (fn && fn instanceof Function) {
            fn = fn.apply(fn.context, fn.args);
        }

        return fn;
    };

    const checkingData = (data, oldData) => {
        return data === oldData ? undefined : data;
    };

    const dotNotationToObject = (data, arr) => {
        return arr.reduce((obj, root) => {
            return objectReferenceService.set(
                root, obj, objectReferenceService.get(root, data)
            );
        }, {});
    };

    const createObject = (obj) => {
        let dottedList = [];

        const {data, exclude = [], oldData} = obj;

        function dotNotationList(recursiveData, recursiveOldData, root) {
            Object.keys(recursiveData).forEach((key) => {
                const value = checkingData(
                    recursiveData[key], recursiveOldData[key]
                );
                const isObject = isPlainObjectService(recursiveData[key]) &&
                    Object.keys(recursiveData[key]).length;
                const isUndefined = value === undefined;
                const isExcluded = root === '' && exclude.indexOf(key) !== -1 ||
                    exclude.indexOf(root.split('.')[0]) !== -1;
                const isListed = dottedList.indexOf(root) === -1;
                const isAllowedToBeListed = isListed && !isUndefined &&
                    !isExcluded;

                if (isObject) {
                    dotNotationList(
                        recursiveData[key],
                        recursiveOldData[key],
                        root ? [root, key].join('.') : key
                    );
                } else if (isAllowedToBeListed) {
                    dottedList.push(root || key);
                }
            });
        }

        trampoline(() => {
            return dotNotationList(data, oldData, '');
        });

        return dotNotationToObject(data, dottedList);
    };

    return {
        set: createObject
    };
};

export default mappingSubmitDataService;
