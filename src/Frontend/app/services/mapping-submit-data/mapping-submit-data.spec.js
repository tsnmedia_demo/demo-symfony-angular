import mappingSubmitDataModule from './mapping-submit-data';
import servicesModule from 'services/services';

describe('mappingSubmitDataService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.mapping-submit-data', () => {
            expect(mappingSubmitDataModule.name).toBe('app.service.mappingSubmitData');
        });
    });

    describe('Service', () => {
        let mappingSubmitDataService, obj;

        beforeEach(window.inject((_mappingSubmitDataService_) => {
            mappingSubmitDataService = _mappingSubmitDataService_;
            obj = {
                item: 0,
                obj: {
                    item: 1,
                    child: {
                        inner: 2,
                        test: {
                            inner: 2
                        }
                    },
                    otherChild: {
                        item: 2
                    },
                    lastChild: {
                        item: 2
                    }
                }
            };
        }));

        it('should return a deep level item changed and his parents', () => {
            const data = {
                item: 0,
                obj: {
                    item: 1,
                    child: {
                        inner: 2,
                        test: {
                            inner: 'changed'
                        }
                    },
                    otherChild: {
                        item: 2
                    },
                    lastChild: {
                        item: 2
                    }
                }
            };

            const mappedData = {
                obj: {
                    child: {
                        test: {
                            inner: 'changed'
                        }
                    }
                }
            };

            expect(mappingSubmitDataService.set({
                oldData: obj,
                data: data
            })).toEqual(mappedData);
        });

        it('should return a first level item changed', () => {
            const data = {
                item: 'changed',
                obj: {
                    item: 1,
                    child: {
                        inner: 2,
                        test: {
                            inner: 2
                        }
                    },
                    otherChild: {
                        item: 2
                    },
                    lastChild: {
                        item: 2
                    }
                }
            };

            const mappedData = {
                item: 'changed'
            };

            expect(mappingSubmitDataService.set({
                oldData: obj,
                data: data
            })).toEqual(mappedData);
        });

        it('should exclude "obj.child" even his child changes', () => {
            const data = {
                item: 'changed',
                obj: {
                    item: 1,
                    child: {
                        inner: 2,
                        test: {
                            inner: 'changed'
                        }
                    },
                    otherChild: {
                        item: 2
                    },
                    lastChild: {
                        item: 2
                    }
                }
            };

            const mappedData = {};

            expect(mappingSubmitDataService.set({
                oldData: obj,
                data: data,
                exclude: 'obj.child'
            })).toEqual(mappedData);
        });
    });
});
