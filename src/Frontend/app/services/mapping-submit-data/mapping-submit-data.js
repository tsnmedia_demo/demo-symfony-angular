import angular from 'angular';
import mappingSubmitDataService from './mapping-submit-data.service';

const mappingSubmitDataModule = angular
    .module('app.service.mappingSubmitData', [])
    .service('mappingSubmitDataService', mappingSubmitDataService);

export default mappingSubmitDataModule;
