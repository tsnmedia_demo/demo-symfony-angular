import baseModule from './base';
import servicesModule from 'services/services';

describe('baseService', () => {
    beforeEach(window.module(
       servicesModule.name,
        ($provide) => {
            const windowObj = {};
            windowObj.globalConfig = {};
            windowObj.globalConfig.i18n = {};

            $provide.value('$window', windowObj);

            $provide.service('$state', () => {});
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.base', () => {
            expect(baseModule.name).toBe('app.service.base');
        });
    });

    describe('Service', () => {
        let baseService, context = {};

        beforeEach(window.inject((_baseService_) => {
            baseService = _baseService_;

            baseService.init(context);
        }));

        it('should has injected baseApi', () => {
            expect(typeof context.baseApi).toBe('object');
        });

        it('should has injected baseConstants', () => {
            expect(typeof context.baseConstants).toBe('object');
        });

        it('should has injected baseDataStore', () => {
            expect(typeof context.baseDataStore).toBe('object');
        });

        it('should has injected baseI18n', () => {
            expect(typeof context.baseI18n).toBe('object');
        });

        it('should has injected baseI18nGet', () => {
            expect(typeof context.baseI18nGet).toBe('function');
        });

        it('should has injected baseOnInit', () => {
            expect(typeof context.baseOnInit).toBe('function');
        });

        it('should has injected baseTimeout', () => {
            expect(typeof context.baseTimeout).toBe('function');
        });
    });
});
