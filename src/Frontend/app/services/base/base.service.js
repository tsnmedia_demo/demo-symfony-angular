const baseService = (
    $timeout,
    apiService,
    constantsService,
    dataStoreService,
    i18nService,
    onInitService,
    whenService,
    whenCurriedService
) => {
    const services = {
        i18n: i18nService.all(),
        i18nGet: i18nService.get,
        timeout: $timeout
    };

    const init = (obj, context) => {
        context.baseApi = apiService;
        context.baseConstants = constantsService;
        context.baseDataStore = dataStoreService;
        context.baseI18n = obj.i18n;
        context.baseI18nGet = obj.i18nGet;
        context.baseOnInit = onInitService;
        context.baseTimeout = obj.timeout;
        context.baseWhen = whenService;
        context.baseWhenCurried = whenCurriedService;
    };

    return {
        init: init.bind(null, services)
    };
};

export default baseService;
