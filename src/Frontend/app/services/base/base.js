import angular from 'angular';
import baseService from './base.service';

const baseModule = angular.module('app.service.base', [])
    .service('baseService', baseService);

export default baseModule;
