const mappingObjectService = (isPlainObjectService) => {
    const mappingObjectToAOneLevelObject = (obj, newObject) => {
        const output = newObject || {};

        Object.keys(obj).map((key) => {
            if (isPlainObjectService(obj[key])) {
                mappingObjectToAOneLevelObject(obj[key], output);
            } else {
                output[key] = obj[key];
            }
        });

        return output;
    };

    return {
        get: mappingObjectToAOneLevelObject
    };
};

export default mappingObjectService;
