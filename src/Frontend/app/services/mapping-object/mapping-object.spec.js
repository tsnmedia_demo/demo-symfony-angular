import mappingObjectModule from './mapping-object';
import servicesModule from 'services/services';

describe('mappingObjectService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.mappingObject', () => {
            expect(mappingObjectModule.name).toBe('app.service.mappingObject');
        });
    });

    describe('Service', () => {
        let mappingObjectService, obj;

        beforeEach(window.inject((_mappingObjectService_) => {
            mappingObjectService = _mappingObjectService_;

            obj = {
                item: 'test',
                list: [0, 1, 2],
                obj: {
                    child: {
                        inner: 'test'
                    }
                }
            };
        }));

        it('should return a one level object', () => {
            expect(JSON.stringify(mappingObjectService.get(obj)))
                .toBe(JSON.stringify({
                    item: 'test',
                    list: [0, 1, 2],
                    inner: 'test'
                }));
        });
    });
});
