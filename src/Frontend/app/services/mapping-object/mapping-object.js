import angular from 'angular';
import mappingObjectService from './mapping-object.service';

const mappingObjectModule = angular.module('app.service.mappingObject', [])
    .service('mappingObjectService', mappingObjectService);

export default mappingObjectModule;
