import angular from 'angular';
import yearsAndMonthsGeneratorService from './years-and-months-generator.service';

const yearsAndMonthsGeneratorModule = angular.module('app.service.yearsAndMonthsGenerator', [])
    .service('yearsAndMonthsGeneratorService', yearsAndMonthsGeneratorService);

export default yearsAndMonthsGeneratorModule;
