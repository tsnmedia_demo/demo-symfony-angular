import yearsAndMonthsGeneratorModule from './years-and-months-generator';
import servicesModule from 'services/services';

describe('yearsAndMonthsGeneratorService', () => {
    function zeroPrefixOneDigitNumbers(number) {
        return /\d\d/.test(number) ? number : `0${number}`;
    }

    beforeEach(window.module(
       servicesModule.name,($provide) => {
            const nowDateObj = {
                day: 15,
                month: 6,
                year: 2016
            };
            const currentDateService = {};

            currentDateService.dateObject = () => {
                return nowDateObj;
            };

            currentDateService.date = () => {
                return new Date(`${nowDateObj.year}-${zeroPrefixOneDigitNumbers(nowDateObj.month)}-${nowDateObj.day}T00:00:00`);
            };

            $provide.value('currentDateService', currentDateService);
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.years-and-months-generator', () => {
            expect(yearsAndMonthsGeneratorModule.name).toBe('app.service.yearsAndMonthsGenerator');
        });
    });

    describe('Service', () => {
        let yearsAndMonthsGeneratorService,
            currentDate,
            currentDateService,
            currentDay,
            currentMonth,
            currentYear,
            months;

        beforeEach(window.inject((
            _rangeService_,
            _currentDateService_,
            _yearsAndMonthsGeneratorService_
        ) => {
            currentDateService = _currentDateService_;

            yearsAndMonthsGeneratorService = _yearsAndMonthsGeneratorService_;

            const dateObject = currentDateService.dateObject();

            currentDay = dateObject.day;
            currentMonth = dateObject.month;
            currentYear = dateObject.year;

            currentDate = `${currentYear}-${currentMonth}-${currentDay}T00:00:00`;

            months = _rangeService_(1, 13);
        }));

        it('should return an exception when the starting date is not a valid date', () => {
            expect(() => {
                yearsAndMonthsGeneratorService.getDatesFromStartingDate('');
            }).toThrowError(/starting date is not a valid date/);
        });

        it('should return an exception when the starting date is greater than current date', () => {
            const startingDate = `${currentYear + 1}-${zeroPrefixOneDigitNumbers(currentMonth)}-02T00:00:00`;

            expect(() => {
                yearsAndMonthsGeneratorService.getDatesFromStartingDate(startingDate);
            }).toThrowError('The starting date ' + new Date(startingDate) + ' is greater than current date ' + currentDateService.date() + '.');
        });

        it('should return an array with one object with the current year and current month when the starting date is equal to the current date', () => {
            const startingDate = `${currentYear}-${zeroPrefixOneDigitNumbers(currentMonth)}-02T00:00:00`;

            expect(yearsAndMonthsGeneratorService.getDatesFromStartingDate(startingDate)).toEqual([{
                year: currentYear,
                months: [currentMonth]
            }]);
        });

        it('should return all the date after the starting date when the starting date is older that the current date on the same year', () => {
            const previousMonth = currentMonth - 4;
            const startingDate = `${currentYear}-${zeroPrefixOneDigitNumbers(previousMonth)}-02T00:00:00`;

            expect(yearsAndMonthsGeneratorService.getDatesFromStartingDate(startingDate)).toEqual([
                {
                    year: currentYear,
                    months: [2, 3, 4, 5, 6]
                }
            ]);
        });

        it('should return all the date after the starting date when the starting date is older that the current date on the previous year', () => {
            const previousMonth = currentMonth - 4;
            const startingDate = `${currentYear - 2}-${zeroPrefixOneDigitNumbers(previousMonth)}-02T00:00:00`;

            expect(yearsAndMonthsGeneratorService.getDatesFromStartingDate(startingDate)).toEqual([
                {
                    year: 2014,
                    months: months.slice(previousMonth - 1)
                },
                {
                    year: 2015,
                    months: months
                },
                {
                    year: 2016,
                    months: months.slice(0, currentMonth)
                }
            ]);
        });
    });
});
