const yearsAndMonthsGeneratorService = (currentDateService, rangeService) => {
    const nowDate = currentDateService.dateObject();
    const currentMonth = nowDate.month;
    const currentYear = nowDate.year;
    const months = rangeService(1, 13);

    const monthsFromTheStartMonthToEndMonth = (startMonth, endMonth) => {
        return months.slice(startMonth, endMonth || currentMonth);
    };

    const listsOfMonthsFromStartYearToCurrentYear = (
        startMonth,
        startYear,
        incrementalYear
    ) => {
        let listOfMonths;

        if (startYear === currentYear) {
            listOfMonths = monthsFromTheStartMonthToEndMonth(
                startMonth, currentMonth);
        } else if (startYear === incrementalYear) {
            listOfMonths = monthsFromTheStartMonthToEndMonth(startMonth, 12);
        } else if (currentYear === incrementalYear) {
            listOfMonths = monthsFromTheStartMonthToEndMonth(0, currentMonth);
        } else {
            listOfMonths = months;
        }

        return listOfMonths;
    };

    const startDateValidation = (date) => {
        const now = currentDateService.date();

        if (isNaN(date.getMonth())) {
            throw new Error('The starting date is not a valid date.');
        } else if (date > now) {
            throw new Error('The starting date ' + date + ' is greater than current date ' + now + '.');
        }
    };

    const getDatesFromStartingDate = (startingDate) => {
        let listOfDates = [];

        const date = new Date(startingDate);
        const startMonth = date.getMonth();
        const startYear = date.getFullYear();

        startDateValidation(date);

        for (
            let incrementalYear = startYear;
            currentYear >= incrementalYear;
            incrementalYear++
        ) {
            listOfDates.push({
                year: incrementalYear,
                months: listsOfMonthsFromStartYearToCurrentYear(startMonth,
                    startYear, incrementalYear)
            });
        }

        return listOfDates;
    };

    return {
        getDatesFromStartingDate
    };
};

export default yearsAndMonthsGeneratorService;
