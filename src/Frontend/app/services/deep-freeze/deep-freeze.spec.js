import deepFreezeModule from './deep-freeze';
import servicesModule from 'services/services';

describe('deepFreezeService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.deepFreeze', () => {
            expect(deepFreezeModule.name).toBe('app.service.deepFreeze');
        });
    });

    describe('Service', () => {
        let obj, deepFreezeObj, deepFreezeService;

        beforeEach(window.inject((_deepFreezeService_) => {
            deepFreezeService = _deepFreezeService_;
            obj = {
                test: 'test',
                deepTest: {
                    test: 'test'
                },
                links: [
                    {
                        href: 'test'
                    }
                ]
            };
            deepFreezeObj = deepFreezeService.set(obj);
        }));


        describe(`Nothing can be added to or removed from the properties set
        of a frozen object`, () => {
            it('has a first level frozen object', () => {
                expect(typeof deepFreezeObj === 'object' && Object.isFrozen(deepFreezeObj)).toBeTruthy();
            });

            it('has a deep level frozen object', () => {
                const deepTest = deepFreezeObj.deepTest;

                expect(typeof deepFreezeObj === 'object' && Object.isFrozen(deepTest)).toBeTruthy();
            });

            it('has a deep level frozen object child of an array', () => {
                const deepTest = deepFreezeObj.links[0];

                expect(typeof deepFreezeObj === 'object' && Object.isFrozen(deepTest)).toBeTruthy();
            });

            it('should return the same value of the argument when is typeof === object', () => {
                expect(deepFreezeService.set(obj)).toBe(obj);
            });

            [false, true, 1, '', undefined, null].forEach((item) => {
                const newItem = JSON.stringify(item);

                it(`should return ${newItem} when the parameter is not an object or is null`, () => {
                    expect(deepFreezeService.set(item)).toBe(item);
                });

                it('should return true for every type not allowed to be freeze', () => {
                    const testNoFrozen = deepFreezeService.set(item);

                    expect(Object.isFrozen(testNoFrozen)).toBeTruthy();
                });
            });
        });
    });
});
