/**
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/
 *      Global_Objects/Object/freeze
 */
const deepFreezeService = () => {
    function deepFreeze(obj) {
        Object.getOwnPropertyNames(obj).forEach(function(name) {
            const prop = obj[name];

            if (typeof prop === 'object' && prop !== null) {
                deepFreeze(prop);
            }
        });

        return Object.freeze(obj);
    }

    const setObject = (obj) => {
        return Object.isFrozen(obj) ? obj : deepFreeze(obj);
    };

    return {
        set: setObject
    };
};

export default deepFreezeService;
