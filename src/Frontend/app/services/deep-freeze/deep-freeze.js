import angular from 'angular';
import deepFreezeService from './deep-freeze.service';

const deepFreezeModule = angular.module('app.service.deepFreeze', [])
    .service('deepFreezeService', deepFreezeService);

export default deepFreezeModule;
