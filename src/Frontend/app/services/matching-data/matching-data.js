import angular from 'angular';
import matchingDataService from './matching-data.service';

const matchingDataModule = angular.module('app.service.matchingData', [])
    .service('matchingDataService', matchingDataService);

export default matchingDataModule;
