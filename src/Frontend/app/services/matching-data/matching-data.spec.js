import matchingDataModule from './matching-data';
import servicesModule from 'services/services';

describe('matchingDataService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.matching-data', () => {
            expect(matchingDataModule.name).toBe('app.service.matchingData');
        });
    });

    describe('Service', () => {
        let matchingDataService, obj;

        beforeEach(window.inject((_matchingDataService_) => {
            matchingDataService = _matchingDataService_;
            obj = {
                item: 0,
                obj: {
                    item: 1,
                    child: {
                        inner: 2
                    },
                    otherChild: {
                        item: 2
                    }
                }
            };
        }));

        it('should return the object of rootKey equal to "obj.child" with value equal to the key, "inner"', () => {
            const callback = (key, rootKey) => {
                return 'obj.child' === rootKey ? key : null;
            };

            expect(JSON.stringify(matchingDataService.set({
                callback: callback,
                data: obj
            }))).toBe(JSON.stringify({
                obj: {
                    child: {
                        inner: 'inner'
                    }
                }
            }));
        });

        it('should return the same object with each value as key plus one', () => {
            const callback = (key) => key + 1;

            expect(JSON.stringify(matchingDataService.set({
                callback: callback,
                data: obj
            }))).toBe(JSON.stringify({
                item: 'item1',
                obj: {
                    item: 'item1',
                    child: {
                        inner: 'inner1'
                    },
                    otherChild: {
                        item: 'item1'
                    }
                }
            }));
        });

        it('should return the same object with each value as key plus one, but not the ones that are null', () => {
            const callback = (key) => key === 'inner' ? key + 1 : null;

            expect(JSON.stringify(matchingDataService.set({
                callback: callback,
                data: obj
            }))).toBe(JSON.stringify({
                obj: {
                    child: {
                        inner: 'inner1'
                    }
                }
            }));
        });

        it('should return the same object with each value as key plus one, but not the ones that has the value "excludeMe"', () => {
            const callback = (key) => key === 'inner' ? key + 1 : 'excludeMe';

            expect(JSON.stringify(matchingDataService.set({
                callback: callback,
                data: obj,
                exclude: 'excludeMe'
            }))).toBe(JSON.stringify({
                obj: {
                    child: {
                        inner: 'inner1'
                    }
                }
            }));
        });
    });
});
