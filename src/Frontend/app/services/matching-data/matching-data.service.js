const matchingDataService = (isPlainObjectService) => {
    const setNewDataToTheObjectValues = (obj) => {
        const {callback, data, exclude = null, keyRoot = null} = obj;

        return Object.keys(data).reduce((newObject, key) => {
            const itemData = callback(key, keyRoot);

            if (isPlainObjectService(data[key])) {
                let newData = setNewDataToTheObjectValues({
                    callback: callback,
                    data: data[key],
                    exclude: exclude,
                    keyRoot: keyRoot ? [keyRoot, key].join('.') : key
                });

                if (newData && JSON.stringify(newData) !== '{}') {
                    newObject[key] = newData;
                }
            } else if (itemData && itemData !== exclude) {
                newObject[key] = itemData;
            }

            return newObject;
        }, {});
    };

    return {
        set: setNewDataToTheObjectValues
    };
};

export default matchingDataService;
