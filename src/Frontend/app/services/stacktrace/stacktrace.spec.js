import stacktraceModule from './stacktrace';
import servicesModule from 'services/services';

describe('stacktraceService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.stacktrace', () => {
            expect(stacktraceModule.name).toBe('app.service.stacktrace');
        });
    });
});
