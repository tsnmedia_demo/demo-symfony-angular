import angular from 'angular';
import stacktraceService from './stacktrace.service';

const stacktraceModule = angular.module('app.service.stacktrace', [])
    .service('stacktraceService', stacktraceService);

export default stacktraceModule;
