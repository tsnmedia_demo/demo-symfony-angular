import stacktrace from 'stacktrace-js';

const stacktraceService = () => {
    return stacktrace;
};

export default stacktraceService;
