import angular from 'angular';
import currencyFormatService from './currency-format.service';

const currencyFormatModule = angular.module('app.service.currencyFormat', [])
    .service('currencyFormatService', currencyFormatService);

export default currencyFormatModule;
