import currencyFormatModule from './currency-format';
import servicesModule from 'services/services';

describe('currencyFormatService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.currency-format', () => {
            expect(currencyFormatModule.name).toBe('app.service.currencyFormat');
        });
    });

    describe('Service', () => {
        let currencyFormatService, listOfNumbers;

        beforeEach(window.inject((_currencyFormatService_) => {
            currencyFormatService = _currencyFormatService_;

            listOfNumbers = [1, 12, 123, 1234, 12345, 123456, 1234567, 12345.67];
        }));

        describe('Currency Euro', () => {
            it('should add two decimals to the number 0 and add the Euro symbol', () => {
                expect(currencyFormatService.euro(0)).toBe('0,00 €');
            });

            it('should add two decimals to the number 1 and add the Euro symbol', () => {
                expect(currencyFormatService.euro(1)).toBe('1,00 €');
            });

            it('should add one decimals to the number -1.15 and add the Euro symbol', () => {
                expect(currencyFormatService.euro(-1.15)).toBe('-1,15 €');
            });

            it('should add one decimals to the number 10.1 and add the Euro symbol', () => {
                expect(currencyFormatService.euro(10.1)).toBe('10,10 €');
            });

            it('should add one decimals to the number -10.15 and add the Euro symbol', () => {
                expect(currencyFormatService.euro(-10.15)).toBe('-10,15 €');
            });

            it('should add two decimals to the number 100.15, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(100.15)).toBe('100,15 €');
            });

            it('should add two decimals to the number -100.15, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(-100.15)).toBe('-100,15 €');
            });

            it('should add two decimals to the number 1000.123, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(1000.123)).toBe('1.000,12 €');
            });

            it('should add two decimals to the number 1000.129, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(1000.129)).toBe('1.000,13 €');
            });

            it('should add two decimals to the number 1234, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(1234)).toBe('1.234,00 €');
            });

            it('should add two decimals to the number -100000, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(-100000)).toBe('-100.000,00 €');
            });

            it('should add two decimals to the number -1234, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(-1234.15)).toBe('-1.234,15 €');
            });

            it('should add two decimals to the number 12345.67, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(12345.67)).toBe('12.345,67 €');
            });

            it('should add two decimals to the number -12345.67, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(-12345.67)).toBe('-12.345,67 €');
            });

            it('should add two decimals to the number 1234567, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(1234567)).toBe('1.234.567,00 €');
            });

            it('should add two decimals to the number -1234567.15, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(-1234567.15)).toBe('-1.234.567,15 €');
            });

            it('should add two decimals to the number 1000000000, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(1000000000.00)).toBe('1.000.000.000,00 €');
            });

            it('should add two decimals to the number -1000000000, a dot for thousands and a Euro symbol', () => {
                expect(currencyFormatService.euro(-1000000000.00)).toBe('-1.000.000.000,00 €');
            });

            it('should throw an error if the type of the param is a string', () => {
                expect(() => {
                    currencyFormatService.euro('a');
                }).toThrowError(/is not a number/);
            });

            it('should throw an error if the type of the param is null', () => {
                expect(() => {
                    currencyFormatService.euro(undefined);
                }).toThrowError(/is not a number/);
            });
        })
    });
});
