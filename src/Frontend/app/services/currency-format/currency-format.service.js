const currencyFormatService = ($filter, $locale) => {

    const isNotANumber = (num) => {
        if (!num && num !== 0 || isNaN(num)) {
            throw new Error('The param key is not a number.');
        }
    };

    const currencyEuros = (num) => {
        isNotANumber(num);

        $locale.NUMBER_FORMATS.CURRENCY_SYM = '';
        $locale.NUMBER_FORMATS.DECIMAL_SEP = ',';
        $locale.NUMBER_FORMATS.GROUP_SEP = '.';

        return $filter('currency')(num) + ' \u20ac';
    };

    return {
        euro: currencyEuros
    };
};

export default currencyFormatService;
