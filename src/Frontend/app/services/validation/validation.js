import angular from 'angular';
import validationService from './validation.service';

const validationModule = angular.module('app.service.validation', [])
    .service('validationService', validationService);

export default validationModule;
