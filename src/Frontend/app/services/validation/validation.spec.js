import validationModule from './validation';
import servicesModule from 'services/services';

describe('validationService', () => {
    beforeEach(window.module(
        servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.validation', () => {
            expect(validationModule.name).toBe('app.service.validation');
        });
    });

    describe('Service', () => {
        let validationService;

        beforeEach(window.inject((_validationService_) => {
            validationService = _validationService_;
        }));

        it('should return an exception if the first argument is not an array', () => {
            expect(() => validationService.isNotValidIf('', '')).toThrowError();
        });

        it('should return an exception if the second argument is not a string', () => {
            expect(() => validationService.isNotValidIf([], {}))
                .toThrowError(/must to be a string/);
        });

        it('should return an exception if the array has an empty string', () => {
            expect(() => validationService.isNotValidIf([''], ''))
                .toThrowError(/data-validation is empty/);
        });

        it('should return a list with multiple values', () => {
            const validateMultipleItems = validationService.isNotValidIf(
                ['isAlphabetic', 'isEmpty', 'isEmail'], ' . '
            );

            expect(validateMultipleItems.list.length).toBe(2);
        });

        describe('isAlphabetic', () => {
            let validateAnAlphabeticStringWithSpaces,
                validateANoneAlphabeticString;

            beforeEach(() => {
                validateAnAlphabeticStringWithSpaces = validationService.isNotValidIf(
                    ['isAlphabetic'], 'not empty'
                );

                validateANoneAlphabeticString = validationService.isNotValidIf(
                    ['isAlphabetic'], '1 not empty'
                );
            });

            it('should return isValid true if is a alphabetic string with spaces', () => {
                expect(validateAnAlphabeticStringWithSpaces.isValid).toBeTruthy();
            });

            it('should return an empty list if is a alphabetic string with spaces', () => {
                expect(validateAnAlphabeticStringWithSpaces.list.length).toBe(0);
            });

            it('should return an empty error if is a alphabetic string with spaces', () => {
                expect(validateAnAlphabeticStringWithSpaces.message).toBe('');
            });

            it('should return isValid false if is a none alphabetic string', () => {
                expect(validateANoneAlphabeticString.isValid).toBeFalsy();
            });

            it('should return just one item on the list if is a none alphabetic string', () => {
                expect(validateANoneAlphabeticString.list.length).toBe(1);
            });

            it('should return isAlphabetic as first item of the list if is a none alphabetic string', () => {
                expect(validateANoneAlphabeticString.list[0]).toBe('isAlphabetic');
            });

            it('should return an error i18n key if is a none alphabetic string', () => {
                expect(validateANoneAlphabeticString.message).toBe('_message.error.0_');
            });
        });

        describe('isIban', () => {
            it('should be defined isIban', () => {
                expect(() => {
                    validationService.isNotValidIf(['isIban'], '');
                }).not.toThrowError();
            });

            describe('isANoneAlphanumericString', () => {
                let validateANoneAlphanumericString;

                beforeEach(() => {
                    validateANoneAlphanumericString = validationService.isNotValidIf(
                        ['isIban'], '///FR1420041010050500013M02606'
                    );
                });

                it('should return isValid false if is a none alphanumeric string', () => {
                    expect(validateANoneAlphanumericString.isValid).toBeFalsy();
                });

                it('should return just one item on the list if is a none alphanumeric string', () => {
                    expect(validateANoneAlphanumericString.list.length).toBe(1);
                });

                it('should return isIban as first item of the list if is a none alphanumeric string', () => {
                    expect(validateANoneAlphanumericString.list[0]).toBe('isIban');
                });

                it('should return an error i18n key if is a none alphanumeric string', () => {
                    expect(validateANoneAlphanumericString.message).toBe('_message.error.2_');
                });
            });
        });

        describe('isBic', () => {
            describe('are valid BICs', () => {
                // @see http://networking.mydesigntool.com/viewtopic.php?tid=307&id=31
                const listOfValidBics = [
                    'PSSTFRPPXXX',
                    'RBOSGGSX',
                    'RZTIAT22263',
                    'BCEELULL',
                    'MARKDEFF',
                    'GENODEF1JEV',
                    'UBSWCHZH80A',
                    'CEDELULLXXX',
                    'INGDESMMXXX'
                ];

                listOfValidBics.forEach((item) => {
                    let validateAStringWithBicFormat;

                    beforeEach(() => {
                        validateAStringWithBicFormat = validationService.isNotValidIf(
                            ['isBic'], item
                        );
                    });

                    it(`should return isValid true if is valid BIC, ${item}`, () => {
                        expect(validateAStringWithBicFormat.isValid).toBeTruthy();
                    });

                    it(`should return an empty list if is valid BIC, ${item}`, () => {
                        expect(validateAStringWithBicFormat.list.length).toBe(0);
                    });

                    it('should return an empty error if is valid BIC', () => {
                        expect(validateAStringWithBicFormat.message).toBe('');
                    });
                });
            });

            describe('isANoneValidBic', () => {
                let validateAStringWithoutBicFormat;

                beforeEach(() => {
                    validateAStringWithoutBicFormat = validationService.isNotValidIf(
                        ['isBic'], 'XXX'
                    );
                });

                it('should return isValid false if is a none valid BIC', () => {
                    expect(validateAStringWithoutBicFormat.isValid).toBeFalsy();
                });

                it('should return just one item on the list if is a none valid BIC', () => {
                    expect(validateAStringWithoutBicFormat.list.length).toBe(1);
                });

                it('should return isBic as first item of the list if is a none valid BIC', () => {
                    expect(validateAStringWithoutBicFormat.list[0]).toBe('isBic');
                });

                it('should return an error i18n key if is a none valid BIC', () => {
                    expect(validateAStringWithoutBicFormat.message).toBe('_message.error.1_');
                });
            });
        });

        describe('isEmail', () => {
            describe('are valid emails', () => {
                // @see https://github.com/ansman/validate.js/blob/master/specs/validators/email-spec.js#L22
                const listOfValidEmails = [
                    'niceandsimple@example.com',
                    'very.common@example.com',
                    'a.little.lengthy.but.fine@dept.example.com',
                    'disposable.style.email.with+symbol@example.com',
                    'other.email-with-dash@example.com',
                    'üñîçøðé@example.com',
                    'foo@some.customtld'
                ];

                listOfValidEmails.forEach((item) => {
                    let validateEmail;

                    beforeEach(() => {
                        validateEmail = validationService.isNotValidIf(
                            ['isEmail'], item
                        );
                    });

                    it(`should return isValid true with a valid email, ${item}`, () => {
                        expect(validateEmail.isValid).toBeTruthy();
                    });

                    it(`should return an empty list with a valid email, ${item}`, () => {
                        expect(validateEmail.list.length).toBe(0);
                    });

                    it('should return an empty list with a valid email', () => {
                        expect(validateEmail.message).toBe('');
                    });
                });
            });

            describe('are none valid emails', () => {
                // @see https://github.com/ansman/validate.js/blob/master/specs/validators/email-spec.js#L35
                const listOfNoValidEmails = [
                    'abc.example.com',
                    'a@b@c@example.com',
                    'a"b(c)d,e:f;g<h>i[j\\k]l@example.com',
                    'just"not"right@example.com',
                    'this is"not\\allowed@example.com',
                    'this\\ still\\"not\\\\allowed@example.com'
                ];

                listOfNoValidEmails.forEach((item) => {
                    let validateNoneValidEmail;

                    beforeEach(() => {
                        validateNoneValidEmail = validationService.isNotValidIf(
                            ['isEmail'], item
                        );
                    });

                    it(`should return isValid false with a none valid email, ${item}`, () => {
                        expect(validateNoneValidEmail.isValid).toBeFalsy();
                    });

                    it(`should return just one item on the list with a none valid email, ${item}`, () => {
                        expect(validateNoneValidEmail.list.length).toBe(1);
                    });

                    it(`should return isEmail as first item of the list with a none valid email, ${item}`, () => {
                        expect(validateNoneValidEmail.list[0]).toBe('isEmail');
                    });

                    it('should return an error i18n key if is a none valid email', () => {
                        expect(validateNoneValidEmail.message).toBe('_message.error.3_');
                    });
                });
            });
        });

        describe('isNotRequired', () => {
            let validateNotRequired, validateRequired;

            beforeEach(() => {
                validateNotRequired = validationService.isNotValidIf(
                    ['isNotRequired'], ''
                );

                validateRequired = validationService.isNotValidIf(
                    ['isNotRequired'], 'is required'
                );
            });


            it('should return isValid true if is an empty string', () => {
                expect(validateNotRequired.isValid).toBeTruthy();
            });

            it('should return isValid true if is an not empty string', () => {
                expect(validateRequired.isValid).toBeTruthy();
            });
        });


        describe('isEmpty', () => {
            let validateNotEmptyString, validateEmptyString;

            beforeEach(() => {
                validateNotEmptyString = validationService.isNotValidIf(
                    ['isEmpty'], 'not empty'
                );

                validateEmptyString = validationService.isNotValidIf(
                    ['isEmpty'], ''
                );
            });

            it('should return isValid true if is not an empty string', () => {
                expect(validateNotEmptyString.isValid).toBeTruthy();
            });

            it('should return an empty list if is not an empty string', () => {
                expect(validateNotEmptyString.list.length).toBe(0);
            });

            it('should return an error i18n key if is not an empty string', () => {
                expect(validateNotEmptyString.message).toBe('');
            });

            it('should return isValid false if is an empty string', () => {
                expect(validateEmptyString.isValid).toBeFalsy();
            });

            it('should return just one item on the list if is an empty string', () => {
                expect(validateEmptyString.list.length).toBe(1);
            });

            it('should return isEmpty as first item of the list if is an empty string', () => {
                expect(validateEmptyString.list[0]).toBe('isEmpty');
            });

            it('should return an error i18n key if is an empty string', () => {
                expect(validateEmptyString.message).toBe('_message.error.4_');
            });
        });

        describe('isPositiveNumber', () => {
            let validateAPositiveNumber;

            beforeEach(() => {
                validateAPositiveNumber = validationService.isNotValidIf(
                    ['isPositiveNumber'], '1'
                );
            });

            it('should return isValid true if is a positive number', () => {
                expect(validateAPositiveNumber.isValid).toBeTruthy();
            });

            it('should return an empty list if is a positive number', () => {
                expect(validateAPositiveNumber.list.length).toBe(0);
            });

            it('should return an empty error if is a positive number', () => {
                expect(validateAPositiveNumber.message).toBe('');
            });

            describe('are none valid positive numbers', () => {
                let validateANoneNumber,
                    validateANonePositiveNumber;

                beforeEach(() => {
                    validateANoneNumber = validationService.isNotValidIf(
                        ['isPositiveNumber'], 'test'
                    );

                    validateANonePositiveNumber = validationService.isNotValidIf(
                        ['isPositiveNumber'], '-1'
                    );
                });

                it('should return isValid false if is a none positive number', () => {
                    expect(validateANoneNumber.isValid).toBeFalsy();
                });

                it('should return just one item on the list if is a none positive number', () => {
                    expect(validateANoneNumber.list.length).toBe(1);
                });

                it('should return isPositiveNumber as first item of the list if is a none positive number', () => {
                    expect(validateANoneNumber.list[0]).toBe('isPositiveNumber');
                });

                it('should return an error i18n key if is a none positive number', () => {
                    expect(validateANoneNumber.message).toBe('_message.error.5_');
                });

                it('should return isValid false if is a none positive number', () => {
                    expect(validateANonePositiveNumber.isValid).toBeFalsy();
                });

                it('should return just one item on the list if is a none positive number', () => {
                    expect(validateANonePositiveNumber.list.length).toBe(1);
                });

                it('should return isPositiveNumber as first item of the list if is a none positive number', () => {
                    expect(validateANonePositiveNumber.list[0]).toBe('isPositiveNumber');
                });

                it('should return an error i18n key if is a none positive number', () => {
                    expect(validateANonePositiveNumber.message).toBe('_message.error.5_');
                });
            });
        });

        describe('isWords', () => {
            describe('are valid words in any language', () => {
                const listOfValidWords = 'Düsseldorf, Köln, Москва, 北京市, إسرائيل, Федерации, ームページ';

                listOfValidWords.split(',').forEach((item) => {
                    let validateAStringOfWordsInMultipleLanguages;

                    beforeEach(() => {
                        validateAStringOfWordsInMultipleLanguages = validationService.isNotValidIf(
                            ['isWords'], item
                        );
                    });

                    it(`should return isValid true if is valid word, ${item}`, () => {
                        expect(validateAStringOfWordsInMultipleLanguages.isValid).toBeTruthy();
                    });

                    it(`should return an empty list if is valid word, ${item}`, () => {
                        expect(validateAStringOfWordsInMultipleLanguages.list.length).toBe(0);
                    });

                    it('should return an empty error if is valid word', () => {
                        expect(validateAStringOfWordsInMultipleLanguages.message).toBe('');
                    });
                });
            });

            describe('are none valid words', () => {
                const listOfNoValidWords = [
                    '.',
                    ',',
                    '-',
                    '/',
                    '#',
                    '!',
                    '\\',
                    '?',
                    '¿',
                    '¡',
                    '$',
                    '%',
                    '^',
                    '&',
                    '*',
                    ';',
                    ':',
                    '{',
                    '}',
                    '=',
                    '-',
                    '_',
                    '`',
                    '~',
                    '(',
                    ')'
                ];

                listOfNoValidWords.forEach((item) => {
                    let validateNoneValidWord;

                    beforeEach(() => {
                        validateNoneValidWord = validationService.isNotValidIf(
                            ['isWords'], item
                        );
                    });

                    it(`should return isValid false with a none valid word, ${item}`, () => {
                        expect(validateNoneValidWord.isValid).toBeFalsy();
                    });

                    it(`should return just one item on the list with a none valid word, ${item}`, () => {
                        expect(validateNoneValidWord.list.length).toBe(1);
                    });

                    it(`should return isWords as first item of the list with a none valid word, ${item}`, () => {
                        expect(validateNoneValidWord.list[0]).toBe('isWords');
                    });

                    it('should return an error i18n key if is a none valid word', () => {
                        expect(validateNoneValidWord.message).toBe('_message.error.6_');
                    });
                });
            });
        });
    });
});
