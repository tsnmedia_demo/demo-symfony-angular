let cache = {};

const dataStoreService = (
    $rootScope,
    deepFreezeService
) => {
    const all = () => {
        return cache;
    };

    const clean = () => {
        cache = {};
    };

    const getData = (key) => {
        let data;

        if (cache.hasOwnProperty(key)) {
            data = cache[key];
        }

        return data;
    };

    const setDataToStored = (key, value) => {
        if (typeof key !== 'string') {
            throw new Error('The param key is not a string.');
        } else {
            cache[key] = value;

            $rootScope.$broadcast(key);
        }
    };

    const freeze = (key, value) => {
        setDataToStored(key, deepFreezeService.set(value));
    };

    const setData = (key, value) => {
        setDataToStored(key, value);
    };

    const nullifyByKeyDataStored = (key) => {
        if (cache.hasOwnProperty(key)) {
            cache[key] = null;
        }
    };

    const removeByKeyDataStored = (key) => {
        if (cache.hasOwnProperty(key)) {
            delete cache[key];
        }
    };

    return {
        all,
        clean,
        freeze,
        getData,
        nullify: nullifyByKeyDataStored,
        remove: removeByKeyDataStored,
        setData
    };
};

export default dataStoreService;
