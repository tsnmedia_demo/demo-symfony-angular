import isPlainObject from 'lodash.isplainobject';
import dataStoreModule from './data-store';
import servicesModule from 'services/services';

describe('dataStoreService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.dataStore', () => {
            expect(dataStoreModule.name).toBe('app.service.dataStore');
        });
    });

    describe('Service', () => {
        let $rootScope, dataStoreService;

        beforeEach(window.inject((_$rootScope_, _dataStoreService_) => {
            $rootScope = _$rootScope_;
            dataStoreService = _dataStoreService_;
        }));

        beforeEach(() => {
            spyOn($rootScope, '$broadcast').and.callThrough();

            dataStoreService.clean();
            dataStoreService.setData('test-str', 'test-value');
            dataStoreService.setData('test-object-no-frozen', {
                test: 'test',
                deepTest: {
                    test: 'test'
                }
            });
            dataStoreService.freeze('test-object-frozen', {
                test: 'test',
                deepTest: {
                    test: 'test'
                }
            });
        });

        it('should call $rootScope.$broadcast on setData test-str', () => {
            expect($rootScope.$broadcast).toHaveBeenCalledWith('test-str');
        });

        it('should call $rootScope.$broadcast on setData test-object-no-frozen', () => {
            expect($rootScope.$broadcast).toHaveBeenCalledWith('test-object-no-frozen');
        });

        it('should call $rootScope.$broadcast on freeze test-object-frozen', () => {
            expect($rootScope.$broadcast).toHaveBeenCalledWith('test-object-frozen');
        });

        it('has key test set with value test-value', () => {
            expect(dataStoreService.getData('test-str')).toBe('test-value');
        });

        [false, true, [], {}, undefined, null].forEach((item) => {
            const newItem = JSON.stringify(item);

            it(`should not set a ${newItem} as a key, is not a string`, () => {
                expect(() => { dataStoreService.setData(item); })
                    .toThrowError(/is not a string/);
            });
        });

        it('should clean the hole object stored', () => {
            dataStoreService.clean();

            expect(dataStoreService.all()).toEqual({});
        });

        it('should expose the hole object stored', () => {
            const obj = {};

            obj['test-str'] = dataStoreService.getData('test-str');
            obj['test-object-no-frozen'] = dataStoreService.getData('test-object-no-frozen');
            obj['test-object-frozen'] = dataStoreService.getData('test-object-frozen');

            expect(dataStoreService.all()).toEqual(obj);
        });

        it('should set undefined if the second param is not added', () => {
            dataStoreService.setData('test-undefined');

            expect(dataStoreService.getData('test-undefined')).toBeUndefined();
        });

        it('doesn\'t has a key if it is not added before', () => {
            expect(dataStoreService.getData('test2')).toBeFalsy();
        });

        it('should not get a item without key', () => {
            expect(dataStoreService.getData()).toBeUndefined();
        });

        it('should nullify the item with the key test', () => {
            dataStoreService.nullify('test-str');

            expect(dataStoreService.getData('test-str')).toBeNull();
        });

        it('should do not nullify the item if the key do not exist', () => {
            dataStoreService.nullify('__test__');

            expect(dataStoreService.getData('__test__')).toBeUndefined();
        });

        it('should remove the item with the key test', () => {
            dataStoreService.remove('test-str');

            expect(dataStoreService.getData('test-str')).toBeUndefined();
        });

        it('should do not remove the item if the key do not exist', () => {
            dataStoreService.remove('__test__');

            expect(dataStoreService.getData('__test__')).toBeUndefined();
        });

        it('should do not has a deep level frozen if used setData insted of freeze', () => {
            expect(Object.isFrozen(dataStoreService.getData('test-object-no-frozen'))).toBeFalsy();
        });

        describe(`Nothing can be added to or removed from the properties set
            because is a frozen object`, () => {
                it('has a deep level frozen object', () => {
                    const obj = dataStoreService.getData('test-object-frozen');

                    expect(
                        isPlainObject(obj) &&
                        Object.isFrozen(obj.deepTest)
                    ).toBeTruthy();
                });
        });
    });
});
