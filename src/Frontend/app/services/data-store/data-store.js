import angular from 'angular';
import dataStoreService from './data-store.service';

const dataStoreModule = angular.module('app.service.dataStore', [])
    .service('dataStoreService', dataStoreService);

export default dataStoreModule;
