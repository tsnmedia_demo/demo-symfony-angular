import onInitModule from './on-init';
import servicesModule from 'services/services';

describe('onInitService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.on-init', () => {
            expect(onInitModule.name).toBe('app.service.onInit');
        });
    });

    describe('Service', () => {
        let onInitService, scope, timeout, timerCallback;

        beforeEach(window.inject((_$rootScope_, _$timeout_, _onInitService_) => {
            onInitService = _onInitService_;
            timeout = _$timeout_;
            scope = _$rootScope_;

            timerCallback = jasmine.createSpy('timerCallback');
        }));

        it('should throw an error when scope is not added as a parameter', function() {
            expect(() => onInitService(timerCallback.bind())).toThrowError(/undefined is not an object/);
        });

        it('should throw an error when scope is not added as a parameter', function() {
            expect(() => onInitService('', scope)).toThrowError(/param callback is not a function/);
        });

        it('should causes a timeout to be called synchronously', function() {
            onInitService(timerCallback.bind(), scope);
            timeout.flush();
            expect(timerCallback).toHaveBeenCalled();
        });
    });
});
