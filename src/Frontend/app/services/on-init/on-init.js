import angular from 'angular';
import onInitService from './on-init.service';

const onInitModule = angular.module('app.service.onInit', [])
    .service('onInitService', onInitService);

export default onInitModule;
