const onInitService = ($rootScope, $timeout, constantsService) => {
    const destroy = (scope, init) => {
        scope.$on('$destroy', () => $timeout.cancel(init));
    };

    const onInit = (callback, scope) => {
        const init = $timeout(callback, 0);

        destroy(scope, init);
    };

    const callbackTypeFunction = (callback) => {
        if (typeof callback !== 'function') {
            throw new Error('The param callback is not a function.');
        }
    };

    const onReinitialize = (callback, scope) => {
        $rootScope.$broadcast(constantsService.get('EVENT_TYPE_NOTIFICATIONS_CLOSE'));
        onInit(callback, scope);
    };

    const reinitialize = (callback, scope) => {
        scope.$on('REINITIALIZE_ON_INIT', onReinitialize.bind(null, callback, scope));
    };

    const initialize = (callback, scope, context) => {
        callbackTypeFunction(callback);
        onInit(callback, scope);
        reinitialize(callback, scope, context);
    };

    return initialize;
};

export default onInitService;
