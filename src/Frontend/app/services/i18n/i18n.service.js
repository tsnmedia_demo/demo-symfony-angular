const i18nService = ($window, isPlainObjectService, formatStringService) => {
    const getTranslation = (i18n, obj) => {
        let str = $window.globalConfig.i18n[i18n];

        if (str === undefined) {
            throw new Error(`i18nService, "${i18n}" key do not exist on globalConfig.i18n.`);
        }

        if (isPlainObjectService(obj) && str) {
            str = formatStringService.add(str, obj);
        }

        return str;
    };

    return {
        get: getTranslation,
        all: () => $window.globalConfig.i18n
    };
};

export default i18nService;
