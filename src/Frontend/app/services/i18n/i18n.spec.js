import i18nModule from './i18n';
import servicesModule from '../services';

describe('i18nService', () => {
    const obj = {};
    obj.globalConfig = {};
    obj.globalConfig.i18n = {
        'test.string.num': 'Test ${str} or ${num}'
    };

    beforeEach(window.module(
        servicesModule.name,
        ($provide) => {
            $provide.value('$window', obj);

            $provide.service('formatStringService', () => {
                return {
                    add: value => value
                };
            });
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.i18n', () => {
            expect(i18nModule.name).toBe('app.service.i18n');
        });
    });

    describe('Service', () => {
        let i18nService,
            formatStringService;

        beforeEach(window.inject((
            _i18nService_,
            _formatStringService_
        ) => {
            i18nService = _i18nService_;
            formatStringService = _formatStringService_;
        }));

        it('should return the full object', () => {
            expect(i18nService.all()).toBe(obj.globalConfig.i18n);
        });

        it('should format string', () => {
            spyOn(formatStringService, 'add');

            i18nService.get('test.string.num', {test: 'test'});

            expect(formatStringService.add).toHaveBeenCalledWith('Test ${str} or ${num}', {test: 'test'});
        });

        it('should return undefined if the key do not exist', () => {
            expect(() => { i18nService.get('DO_NOT_EXIST'); })
                .toThrowError(/key do not exist/);
        });
    });
});
