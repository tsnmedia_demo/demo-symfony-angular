import angular from 'angular';
import i18nService from './i18n.service';

const i18nModule = angular.module('app.service.i18n', [])
    .service('i18nService', i18nService);

export default i18nModule;
