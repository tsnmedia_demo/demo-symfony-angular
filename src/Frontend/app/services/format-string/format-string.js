import angular from 'angular';
import formatStringService from './format-string.service';

const formatStringModule = angular.module('app.service.formatString', [])
    .service('formatStringService', formatStringService);

export default formatStringModule;
