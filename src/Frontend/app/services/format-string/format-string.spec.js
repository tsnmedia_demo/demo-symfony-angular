import formatStringModule from './format-string';

describe('formatStringService', () => {
    beforeEach(window.module(
        formatStringModule.name
    ));

    describe('Service', () => {
        let formatStringService;

        const testString = 'Test ${str} or ${num}';

        beforeEach(window.inject((_formatStringService_) => {
            formatStringService = _formatStringService_;
        }));

        it('should return the string with the nodes replaced', () => {
            expect(formatStringService.add(testString, {
                str: 'test',
                num: 0
            })).toBe('Test test or 0');
        });

        it('should not return the string with the nodes replaced', () => {
            expect(formatStringService.add(testString, {
                strFail: 'test',
                numFail: 0
            })).toBe(testString);
        });
    });
});
