const formatStringService = () => {
    const addValuesToString = (str, values) => {
        const arr = Object.keys(values);

        for (let k of arr) {
            let reg = new RegExp('\\$\\{' + k + '\\}', 'g');

            str = str.replace(reg, values[k]);
        }

        return str;
    };

    return {
        add: addValuesToString
    };
};

export default formatStringService;
