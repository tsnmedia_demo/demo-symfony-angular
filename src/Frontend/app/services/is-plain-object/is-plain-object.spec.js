import isPlainObjectModule from './is-plain-object';

describe('isPlainObjectService', () => {
    beforeEach(window.module(isPlainObjectModule.name));

    describe('Module', () => {
        it('has as a name app.service.isPlainObject', () => {
            expect(isPlainObjectModule.name).toBe('app.service.isPlainObject');
        });
    });
});
