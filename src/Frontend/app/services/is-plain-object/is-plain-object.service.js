import isPlainObject from 'lodash.isplainobject';

const isPlainObjectService = () => {
    return isPlainObject;
};

export default isPlainObjectService;
