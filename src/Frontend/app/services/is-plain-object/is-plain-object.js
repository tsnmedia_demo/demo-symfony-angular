import angular from 'angular';
import isPlainObjectService from './is-plain-object.service';

const isPlainObjectModule = angular.module('app.service.isPlainObject', [])
    .service('isPlainObjectService', isPlainObjectService);

export default isPlainObjectModule;
