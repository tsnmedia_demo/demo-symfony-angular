const hrefToStateService = () => {
    const replaceToUpperCase = (str, letter) => {
        return letter.toUpperCase();
    };

    const hrefToState = (str, parentState) => {
        const params = {};
        const projectIdResult = /(?:project\/)([^\/]*)(?=\/)/.exec(str);

        if (projectIdResult) {
            params.reference = projectIdResult[1];
            str = str.replace(new RegExp(`${params.reference}/`), '');
        }

        const state = str.replace(/^\//, '').replace(/\//g, '.').replace(/-(\w)/g, replaceToUpperCase);

        return {
            state: parentState ? `${parentState}.${state}` : state,
            params: params
        };
    };

    return {
        convert: hrefToState
    };
};

export default hrefToStateService;
