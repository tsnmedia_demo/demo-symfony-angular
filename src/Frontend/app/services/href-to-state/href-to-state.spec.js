import hrefToStateModule from './href-to-state';
import servicesModule from 'services/services';

describe('hrefToStateService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.href-to-state', () => {
            expect(hrefToStateModule.name).toBe('app.service.hrefToState');
        });
    });

    describe('Service', () => {
        let hrefToStateService;

        beforeEach(window.inject((_hrefToStateService_) => {
            hrefToStateService = _hrefToStateService_;
        }));

        it('should convert lower hyphen case to upper snake case', () => {
            expect(hrefToStateService.convert('test/test-test/test-test')).toEqual({
                state: 'test.testTest.testTest',
                params: {}
            });
        });

        it('should convert lower hyphen case to upper snake case', () => {
            expect(hrefToStateService.convert('/test/test-test', 'parentState')).toEqual({
                state: 'parentState.test.testTest',
                params: {}
            });
        });

        it('should convert with project reference params', () => {
            expect(hrefToStateService.convert('/test/project/123test/test-test')).toEqual({
                state: 'test.project.testTest',
                params: {
                    reference: '123test'
                }
            });
        });
    });
});
