import angular from 'angular';
import hrefToStateService from './href-to-state.service';

const hrefToStateModule = angular.module('app.service.hrefToState', [])
    .service('hrefToStateService', hrefToStateService);

export default hrefToStateModule;
