import merge from 'lodash.merge';

const mergeService = () => {
    return merge;
};

export default mergeService;
