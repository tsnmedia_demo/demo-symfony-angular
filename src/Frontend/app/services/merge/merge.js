import angular from 'angular';
import mergeService from './merge.service';

const mergeModule = angular.module('app.service.merge', [])
    .service('mergeService', mergeService);

export default mergeModule;
