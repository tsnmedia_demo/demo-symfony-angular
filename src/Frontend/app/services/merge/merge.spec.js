import mergeModule from './merge';

describe('mergeService', () => {
    beforeEach(window.module(mergeModule.name));

    describe('Module', () => {
        it('has as a name app.service.merge', () => {
            expect(mergeModule.name).toBe('app.service.merge');
        });
    });
});
