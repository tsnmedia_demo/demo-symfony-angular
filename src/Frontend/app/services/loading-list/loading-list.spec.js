import loadingListModule from './loading-list';
import servicesModule from 'services/services';

describe('loadingListService', () => {
    beforeEach(window.module(
       servicesModule.name
    ));

    describe('Module', () => {
        it('has as a name app.service.loading-list', () => {
            expect(loadingListModule.name).toBe('app.service.loadingList');
        });
    });

    describe('Service', () => {
        let loadingListService;

        beforeEach(window.inject((_loadingListService_) => {
            loadingListService = _loadingListService_;
        }));

        it('should return a list of 5 items', () => {
            var arr = [];

            loadingListService.build(5, arr);

            expect(arr.length).toBe(5);
        });

        it('should return a list of objects with id match "x-"', () => {
            var arr = [];

            loadingListService.build(5, arr);

            expect(arr[4].id).toMatch(/x-\d/);
        });
    });
});
