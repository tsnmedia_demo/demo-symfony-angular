const loadingListService = () => {
    const addFakeList = (len, arr) => {
        return Array.apply(null, Array(len)).map(Number.call, Number).forEach(
            (index) => {
                arr[index] = {};
                arr[index].id = `x-${index}`;
            });
    };

    return {
        build: addFakeList
    };
};

export default loadingListService;
