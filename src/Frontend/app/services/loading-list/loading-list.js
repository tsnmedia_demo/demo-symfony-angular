import angular from 'angular';
import loadingListService from './loading-list.service';

const loadingListModule = angular.module('app.service.loadingList', [])
    .service('loadingListService', loadingListService);

export default loadingListModule;
