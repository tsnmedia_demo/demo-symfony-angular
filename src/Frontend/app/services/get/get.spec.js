import getModule from './get';
import servicesModule from 'services/services';

describe('getService', () => {
    beforeEach(window.module(
        servicesModule.name,
        ($provide) => {
            $provide.service('$state', () => {});
        }
    ));

    describe('Module', () => {
        it('has as a name app.service.get', () => {
            expect(getModule.name).toBe('app.service.get');
        });
    });

    describe('Service', () => {
        let apiService,
            constantsService,
            dataStoreService,
            getService,
            httpBackend;

        beforeEach(window.inject((
            _$httpBackend_,
            _apiService_,
            _constantsService_,
            _dataStoreService_,
            _getService_
        ) => {
            apiService = _apiService_;
            constantsService = _constantsService_;
            dataStoreService = _dataStoreService_;
            getService = _getService_;
            httpBackend = _$httpBackend_;

            spyOn(apiService, 'get').and.callThrough();
        }));

        describe('get', () => {
            describe('without a request before', () => {
                beforeEach(() => {
                    dataStoreService.clean();
                });

                it('should be defined then, from a promise', () => {
                    expect(getService.get('GET_MAIN', 'API_MAIN_URI').then()).toBeDefined();
                });

                it('should be call the apiService to get the data', () => {
                    getService.get('GET_MAIN', 'API_MAIN_URI');

                    expect(apiService.get).toHaveBeenCalledWith(constantsService.get('API_MAIN_URI'));
                });
            });

            describe('with a request before', () => {
                beforeEach(() => {
                    getService.get('GET_MAIN', 'API_MAIN_URI');
                });

                it('should not be call the apiService to get the data', () => {
                    getService.get('GET_MAIN', 'API_MAIN_URI');

                    expect(apiService.get).not.toHaveBeenCalled();
                });

                it('should be defined then, from a promise', () => {
                    expect(getService.get('GET_MAIN', 'API_MAIN_URI').then()).toBeDefined();
                });
            });
        });
    });
});
