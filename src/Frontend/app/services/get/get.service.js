const getService = (
    apiService,
    constantsService,
    dataStoreService
) => {
    const setToTheDataStoreTheApiResponseAsAPromise = (
        getConstant, apiConstant
    ) => {
        let promise = dataStoreService.getData(constantsService.get(getConstant));

        if (!promise) {
            promise = apiService.get(constantsService.get(apiConstant));

            dataStoreService.setData(
                constantsService.get(getConstant), promise, true
            );
        }

        return promise;
    };

    return {
        get: setToTheDataStoreTheApiResponseAsAPromise
    };
};

export default getService;
