import angular from 'angular';

import getService from './get.service';

const getModule = angular.module('app.service.get', []).service(
    'getService', getService);

export default getModule;
