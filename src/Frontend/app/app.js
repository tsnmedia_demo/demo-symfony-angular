import angular from 'angular';
import ngAnimate from 'angular-animate';
import ngSanitize from 'angular-sanitize';
import uiRouter from 'angular-ui-router';
import uiSelect from 'ui-select';
import uiNotification from 'angular-ui-notification';
import AppComponent from './app.component';
import AppConfig from './app.config';
import AppRun from './app.run';
import Components from './components/components';
import Directives from './directives/directives';
import Services from './services/services';
import Views from './views/views';

angular.module('app', [
    ngAnimate,
    ngSanitize,
    uiRouter,
    uiSelect,
    uiNotification,
    Components.name,
    Directives.name,
    Services.name,
    Views.name
])
    .provider('$exceptionHandler', {
            $get: function(errorLogService) {
                return errorLogService.exceptionHandler;
            }
        }
    )
    .config(AppConfig)
    .run(AppRun)
    .directive('cApp', AppComponent);
