import controller from './submit-button-validation.controller';

const submitButtonValidationDirective = () => {
    return {
        restrict: 'A',
        scope: true,
        controller,
        controllerAs: 'dSubmitButtonValidation',
        bindToController: {
            enabledOnTyping: '=xenabledontyping'
        }
    };
};

export default submitButtonValidationDirective;
