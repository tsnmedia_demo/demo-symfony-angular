import submitButtonValidationEnabledOnTypingServiceModule from './submit-button-validation.enabled-on-typing.service';
import submitButtonValidationChangeStateServiceModule from './submit-button-validation.change-state.service';

describe('SubmitButtonValidationComponent', () => {
    beforeEach(window.module(
        submitButtonValidationEnabledOnTypingServiceModule.name,
        submitButtonValidationChangeStateServiceModule.name,
        $provide => {
            $provide.service('constantsService', () => {
                return {
                    get: value => value
                };
            });
        }
    ));

    describe('Services', () => {
        let context,
            rootScope;

        beforeEach(window.inject((
            $rootScope
        ) => {
            rootScope = $rootScope;

            context = {
                scope: $rootScope.$new(),
                form: {
                    name: 'testForm',
                    querySelectorAll: () => {}
                },
                submitButton: {}
            };
        }));

        describe('submitButtonValidationEnabledOnTypingService', () => {
            let submitButtonValidationEnabledOnTypingService,
                input;

            beforeEach(window.inject((
                _submitButtonValidationEnabledOnTypingService_
            ) => {
                submitButtonValidationEnabledOnTypingService = _submitButtonValidationEnabledOnTypingService_;

                input = {
                    getAttribute: () => 'text',
                    addEventListener: () => {},
                    removeEventListener: () => {}
                };

                spyOn(context.form, 'querySelectorAll').and.returnValue([input]);
            }));

            describe('on init', () => {
                it('tracks that service found all fields', () => {
                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(context.form.querySelectorAll).toHaveBeenCalledWith('[name]');
                });

                it('tracks that added input listener for element', () => {
                    let callback;
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        callback = fn;
                    });

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(input.addEventListener).toHaveBeenCalledWith('input', callback);
                });

                it('tracks that didn\'t add input listener for element if option disabled', () => {
                    spyOn(input, 'addEventListener');

                    submitButtonValidationEnabledOnTypingService.init(false, context.submitButton, context.form);

                    expect(input.addEventListener).not.toHaveBeenCalled();
                });

                it('tracks that added click listener for checkbox element', () => {
                    let callback;
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        callback = fn;
                    });

                    input.getAttribute = () => 'checkbox';

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(input.addEventListener).toHaveBeenCalledWith('click', callback);
                });

                it('tracks that added click listener for radio element', () => {
                    let callback;
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        callback = fn;
                    });

                    input.getAttribute = () => 'radio';

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(input.addEventListener).toHaveBeenCalledWith('click', callback);
                });

                it('tracks that button was enabled', () => {
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        fn();
                    });

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(context.submitButton.disabled).toBe(false);
                });

                it('tracks that removed input listener for element', () => {
                    let callback;
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        callback = fn;
                        fn();
                    });
                    spyOn(input, 'removeEventListener');

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(input.removeEventListener).toHaveBeenCalledWith('input', callback);
                });

                it('tracks that removed click listener for radio element', () => {
                    let callback;
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        callback = fn;
                        fn();
                    });
                    spyOn(input, 'removeEventListener');
                    input.getAttribute = () => 'radio';

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(input.removeEventListener).toHaveBeenCalledWith('click', callback);
                });

                it('tracks that removed click listener for checkbox element', () => {
                    let callback;
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        callback = fn;
                        fn();
                    });
                    spyOn(input, 'removeEventListener');
                    input.getAttribute = () => 'checkbox';

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);

                    expect(input.removeEventListener).toHaveBeenCalledWith('click', callback);
                });
            });

            describe('on onRemoveEnabledOnTyping', () => {
                it('tracks that added scope $on listener', () => {
                    let callback;
                    spyOn(context.scope, '$on').and.callFake((event, fn) => {
                        callback = fn;
                    });

                    submitButtonValidationEnabledOnTypingService
                        .onRemoveEnabledOnTyping(context.scope, context.form);

                    expect(context.scope.$on).toHaveBeenCalledWith(
                        'EVENT_SUBMIT_BUTTON_REMOVE_ENABLED_ON_TYPING',
                        callback
                    );
                });

                it('tracks that removed listener for element', () => {
                    let callback;
                    spyOn(input, 'addEventListener').and.callFake((event, fn) => {
                        callback = fn;
                    });
                    spyOn(input, 'removeEventListener');

                    submitButtonValidationEnabledOnTypingService.init(true, context.submitButton, context.form);
                    submitButtonValidationEnabledOnTypingService
                        .onRemoveEnabledOnTyping(context.scope, context.form);

                    context.scope.$broadcast('EVENT_SUBMIT_BUTTON_REMOVE_ENABLED_ON_TYPING');

                    expect(input.removeEventListener).toHaveBeenCalledWith('input', callback);
                });
            });
        });

        describe('submitButtonValidationChangeStateService', () => {
            let submitButtonValidationChangeStateService;

            beforeEach(window.inject((
                _submitButtonValidationChangeStateService_
            ) => {
                submitButtonValidationChangeStateService = _submitButtonValidationChangeStateService_;
            }));

            describe('on init', () => {
                it('tracks that added EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR event listener', () => {
                    let callback;
                    spyOn(context.scope, '$on').and.callFake((event, fn) => {
                        if(event == 'EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR') {
                            callback = fn;
                        }
                    });

                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    expect(context.scope.$on).toHaveBeenCalledWith(
                        'EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR',
                        callback
                    );
                });

                it('tracks that button enabled on EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR', () => {
                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    context.scope.$broadcast('EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR', 'testForm');

                    expect(context.submitButton.disabled).toBe(false);
                });

                it('tracks that emitted EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR', () => {
                    spyOn(context.scope, '$emit');

                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    context.scope.$broadcast('EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR', 'testForm');

                    expect(context.scope.$emit).toHaveBeenCalledWith('EVENT_SUBMIT_BUTTON_CHANGE_STATE');
                });

                it('tracks that emitted EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR', () => {
                    spyOn(context.scope, '$emit');

                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    context.scope.$broadcast('EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR', 'wrongForm');

                    expect(context.scope.$emit).not.toHaveBeenCalled();
                });

                it('tracks that added EVENT_FROM_VALIDATION_ON_ERROR event listener', () => {
                    let callback;
                    spyOn(context.scope, '$on').and.callFake((event, fn) => {
                        if(event == 'EVENT_FROM_VALIDATION_ON_ERROR'){
                            callback = fn;
                        }
                    });

                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    expect(context.scope.$on).toHaveBeenCalledWith('EVENT_FROM_VALIDATION_ON_ERROR', callback);
                });

                it('tracks that button enabled on EVENT_FROM_VALIDATION_ON_ERROR', () => {
                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    context.scope.$broadcast('EVENT_FROM_VALIDATION_ON_ERROR', 'testForm');

                    expect(context.submitButton.disabled).toBe(true);
                });

                it('tracks that emitted EVENT_FROM_VALIDATION_ON_ERROR', () => {
                    spyOn(context.scope, '$emit');

                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    context.scope.$broadcast('EVENT_FROM_VALIDATION_ON_ERROR', 'testForm');

                    expect(context.scope.$emit).toHaveBeenCalledWith('EVENT_SUBMIT_BUTTON_CHANGE_STATE');
                });

                it('tracks that added EVENT_API_ERROR_500 event listener', () => {
                    let callback;
                    spyOn(context.scope, '$on').and.callFake((event, fn) => {
                        if(event == 'EVENT_API_ERROR_500') {
                            callback = fn;
                        }
                    });

                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    expect(context.scope.$on).toHaveBeenCalledWith(
                        'EVENT_API_ERROR_500',
                        callback
                    );
                });

                it('tracks that events were deregistered on EVENT_API_ERROR_500', () => {
                    const callbackSpy = jasmine.createSpy('callbackSpy', () => {});
                    const newScope = rootScope.$new();
                    spyOn(context.scope, '$on').and.callFake((event, fn) => {
                        newScope.$on(event, fn);
                        return callbackSpy;
                    });

                    submitButtonValidationChangeStateService.init(
                        context.scope,
                        context.submitButton,
                        context.form
                    );

                    newScope.$broadcast('EVENT_API_ERROR_500');

                    expect(callbackSpy).toHaveBeenCalled();
                });
            });
        });
    });
});