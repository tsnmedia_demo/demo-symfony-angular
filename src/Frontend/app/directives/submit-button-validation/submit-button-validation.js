import angular from 'angular';
import submitButtonValidationDirective from './submit-button-validation.directive';
import submitButtonValidationEnabledOnTypingService from './submit-button-validation.enabled-on-typing.service';
import submitButtonValidationChangeStateService from './submit-button-validation.change-state.service';

const submitButtonValidationModule = angular.module('app.directive.submitButtonValidation', [
        submitButtonValidationEnabledOnTypingService.name,
        submitButtonValidationChangeStateService.name
    ]).directive('dSubmitButtonValidation', submitButtonValidationDirective);

export default submitButtonValidationModule;
