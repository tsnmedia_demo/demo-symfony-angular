import angular from 'angular';

const submitButtonValidationChangeStateService = (
    constantsService
) => {

    /*
     * @see http://stackoverflow.com/questions/14898296/how-to-unsubscribe-to-a-broadcast-event-in-angularjs-how-to-remove-function-reg
     */
    const removeChangeState = (options) => {
        Object.keys(options.offCallMeFn).forEach(name => options.offCallMeFn[name]());
    };

    const listenError500 = (options) => {
        options.scope.$on(constantsService.get('EVENT_API_ERROR_500'), removeChangeState.bind(null, options));
    };

    const changeState = (options, valid, ev, formName) => {
        if (formName === options.form.name) {
            options.submitButton.disabled = valid;
            options.scope.$emit(constantsService.get('EVENT_SUBMIT_BUTTON_CHANGE_STATE'));
        }
    };

    const listenFormValidationEvent = (options, obj) => {
        const name = constantsService.get(obj.name);

        options.offCallMeFn[name] = options.scope.$on(name, changeState.bind(this, options, obj.valid));
    };

    const eachFormValidationEvent = (options) => {
        [
            {name: 'EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR', valid: false},
            {name: 'EVENT_FROM_VALIDATION_ON_ERROR', valid: true}
        ].forEach(listenFormValidationEvent.bind(null, options));
    };

    const init = (scope, submitButton, form) => {
        const options = {
            scope: scope,
            submitButton: submitButton,
            form: form,
            offCallMeFn: {}
        };

        eachFormValidationEvent(options);
        listenError500(options);
    };

    return {
        init
    };
};

export default angular.module('app.directive.submitButtonValidationChangeState.service', [])
    .service('submitButtonValidationChangeStateService', submitButtonValidationChangeStateService);
