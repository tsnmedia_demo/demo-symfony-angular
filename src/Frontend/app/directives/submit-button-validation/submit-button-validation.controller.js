export default class SubmitButtonValidationController {
    constructor(
        $scope,
        $element,
        domUtilsService,
        submitButtonValidationEnabledOnTypingService,
        submitButtonValidationChangeStateService,
        onInitService
    ) {
        this.scope = $scope;
        this.submitButton = $element[0];
        this.submitButtonValidationEnabledOnTypingService = submitButtonValidationEnabledOnTypingService;
        this.submitButtonValidationChangeStateService = submitButtonValidationChangeStateService;
        this.form = domUtilsService.closest(this.submitButton, 'form');

        this.beforeOnInit();

        onInitService(this.initialize.bind(this), $scope);
    }

    initialize() {
        this.submitButtonValidationEnabledOnTypingService.init(
            this.enabledOnTyping,
            this.submitButton,
            this.form
        );
    }

    beforeOnInit() {
        this.submitButtonValidationEnabledOnTypingService.onRemoveEnabledOnTyping(this.scope, this.form);
        this.submitButtonValidationChangeStateService.init(
            this.scope,
            this.submitButton,
            this.form
        );
    }
}
