import angular from 'angular';

const submitButtonValidationEnabledOnTypingService = (constantsService) => {
    let enableSubmitButtonListener;

    const REMOVE_ENABLED_ON_TYPING = constantsService.get('EVENT_SUBMIT_BUTTON_REMOVE_ENABLED_ON_TYPING');

    function setEventListener(el, type) {
        ['input', 'click'].forEach((eventType) => {
            const elementType = el.getAttribute('type');
            const isClickable = /^radio|checkbox/.test(elementType) && eventType === 'click';

            if (isClickable || eventType !== 'click') {
                el[`${type}EventListener`](eventType, enableSubmitButtonListener);
            }
        });
    }

    function setEventListenersToEachField(options, type) {
        Array.from(options.form.querySelectorAll('[name]'))
            .forEach(el => setEventListener(el, type));
    }

    function enableSubmitButton(options) {
        options.submitButton.disabled = false;

        setEventListenersToEachField(options, 'remove');
    }

    const onRemoveEnabledOnTyping = (scope, form) => {
        const options = {
            form: form
        };

        scope.$on(REMOVE_ENABLED_ON_TYPING, setEventListenersToEachField.bind(null, options, 'remove'));
    };

    const initialize = (enabledOnTyping, submitButton, form) => {
        const options = {
            submitButton: submitButton,
            form: form
        };

        if (enabledOnTyping) {
            enableSubmitButtonListener = enableSubmitButton.bind(null, options);

            setEventListenersToEachField(options, 'add');
        }
    };

    return {
        init: initialize,
        onRemoveEnabledOnTyping
    };
};

export default angular.module('app.directive.submitButtonValidationEnabledOnTyping.service', [])
    .service('submitButtonValidationEnabledOnTypingService', submitButtonValidationEnabledOnTypingService);
