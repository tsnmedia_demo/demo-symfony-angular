import template from './form-validation.html';

export default class formValidationController {
    constructor(
        $timeout,
        $compile,
        $element,
        $scope,
        debounceService,
        validationService,
        constantsService,
        i18nService,
        onInitService
    ) {
        this.timeout = $timeout;
        this.compile = $compile;
        this.scope = $scope;
        this.element = $element[0];
        this.validationService = validationService;
        this.debounceService = debounceService;
        this.i18n = i18nService.all();
        this.errorCodes = {};
        this.errorClass = {};
        this.dataValidation = {};
        this.onInitService = onInitService;
        this.CLASS_INVALID = constantsService.get('CLASS_INVALID');
        this.EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR = constantsService.get('EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR');
        this.EVENT_FROM_VALIDATION_ON_ERROR = constantsService.get('EVENT_FROM_VALIDATION_ON_ERROR');
        this.EVENT_FROM_VALIDATION_VALIDATE = constantsService.get('EVENT_FROM_VALIDATION_VALIDATE');
        this.EVENT_TO_VALIDATION_UPDATE = constantsService.get('EVENT_TO_VALIDATION_UPDATE');
        this.updateValidates = false;

        this.beforeOnInit();

        onInitService(this.initialize.bind(this), $scope);
    }

    beforeOnInit() {
        this.scope.$on(
            this.EVENT_FROM_VALIDATION_VALIDATE, this.triggerValidateEachField.bind(this));

        this.scope.$on(
            this.EVENT_TO_VALIDATION_UPDATE, this.updateValidation.bind(this));
    }

    updateValidation() {
        this.onInitService(this.initialize.bind(this), this.scope);
    }

    initialize() {
        this.fieldsToValidate();
        this.addMessageForEachField();

        if(this.validateOnSubmit){
            this.addListenerForEachField(this.clearMessage.bind(this));
        } else {
            this.addListenerForEachField(this.validateEachField.bind(this));
        }

        this.updateValidates = true;
    }

    fieldsToValidate() {
        this.fields = Array.from(this.element.querySelectorAll('[data-validation]'));
    }

    addMessageForEachField() {
        this.eachField(this.addFieldMessage.bind(this));
    }

    addListenerForEachField(callback) {
        const debounceCallback = this.debounceService(callback.bind(this), 75);

        this.eachField(el => {
            this.fieldListener(el, 'remove', debounceCallback);
            this.fieldListener(el, 'add', debounceCallback.bind(null, el));
            this.scope.$on('$destroy', this.fieldListener.bind(this, el, 'remove', debounceCallback));
        });
    }

    fieldListener(el, name, callback) {
        const eventName = el.nodeName.toLowerCase() === 'select' ? 'change' : 'input';

        el[`${name}EventListener`](eventName, callback);
    }

    toggleElementClass(obj) {
        const elClassList = obj.el.classList;
        const action = obj.action;
        const className = obj.className;
        const contains = elClassList.contains(className);

        if (contains && action === 'remove' || !contains && action === 'add') {
            elClassList[action](className);
        }
    }

    clearMessage(el) {
        this.messageByField({
            field: this.form[el.name],
            dataValidation: this.dataValidation[el.name],
            el: el,
            name: el.name,
            isValid: true,
            errorMessage: ''
        });
        this.scope.$broadcast(this.EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR, this.form.$name);
        this.scope.$emit(this.EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR, this.form.$name);
    }

    validateEachField() {
        this.eachField(this.messageByFieldChecked.bind(this));
        this.triggerErrorState();
    }

    messageByFieldChecked(el) {
        this.messageByField(this.checkValidityByField(el));
    }

    triggerErrorState() {
        const eventName = Object.keys(this.form.$error).length < 1 ?
            this.EVENT_FROM_VALIDATION_ON_WITHOUT_ERROR :
            this.EVENT_FROM_VALIDATION_ON_ERROR;

        this.scope.$broadcast(eventName, this.form.$name);
        this.scope.$emit(eventName, this.form.$name);
    }

    checkValidityByField(el) {
        let errorMessage;
        let isValid;

        const name = el.name;
        const field = this.form[name];
        const modelValue = field.$modelValue;
        const modelValueAsString = modelValue ? modelValue.toString() : '';
        const dataValidation = this.dataValidation[name];
        const customMessage = el.getAttribute('data-message-validation');

        if (dataValidation) {
            const excludedFieldValidation = el.getAttribute('data-ignore-validation') === 'true' ||
                el.disabled;
            const error = this.validationService.isNotValidIf(dataValidation, modelValueAsString);

            isValid = excludedFieldValidation || error.isValid;
            errorMessage = customMessage ? customMessage : error.message;

            field.$setValidity(name, isValid);
        }

        return {
            field: field,
            dataValidation: dataValidation,
            el: el,
            name: name,
            isValid: isValid === undefined ? true : isValid,
            errorMessage: errorMessage
        };
    }

    messageByField(obj) {
        let hasMessage = false;

        const {field, dataValidation, el, name, isValid, errorMessage} = obj;

        if (dataValidation && field) {
            const action = isValid ? 'remove' : 'add';

            this.errorClass[name] = isValid ? '' : this.CLASS_INVALID;
            this.errorCodes[name] = isValid ? '' : this.i18n[errorMessage];

            this.toggleElementClass({
                el: el,
                action: action,
                className: this.CLASS_INVALID
            });
            this.toggleElementClass({
                el: el.parentNode, // label
                action: action
            });

            hasMessage = true;
        }

        return hasMessage;
    }

    addFieldMessage(el) {
        const tempEl = document.createElement('div');
        const label = el.parentNode;
        const name = el.name;
        const dataValidationList = el.getAttribute('data-validation').replace(/\s/g, '').split(',');
        const isValidList = !(dataValidationList.length === 1 && dataValidationList[0] === '');
        const hasMessage = label.classList.contains('has-message');

        if (name && isValidList && (!hasMessage || this.updateValidates)) {
            this.dataValidation[name] = dataValidationList;
            tempEl.innerHTML = template
                .replace(/errorCodes/g, `errorCodes.${name}`)
                .replace(/errorClass/g, `errorClass.${name}`);

            if (!this.updateValidates) {
                const message = tempEl.querySelector('span');

                this.compile(message)(this.scope);
                label.classList.add('d-form-validation');
                label.classList.add('has-message');
                label.insertBefore(message, el.nextSibling);
            }
        }
    }

    triggerValidateEachField(ev, formName) {
        if (formName === this.form.$name) {
            if (this.validateOnSubmit) {
                this.eachField(this.messageByFieldChecked.bind(this));
            } else {
                this.validateEachField();
            }
        }
    }

    eachField(callback) {
        this.timeout(this.fields.map((el) => callback(el)));
    }
}
