import controller from './form-validation.controller';

const formValidationDirective = () => {
    return {
        restrict: 'A',
        scope: true,
        controller,
        controllerAs: 'dFormValidation',
        bindToController: {
            form: '=name',
            validateOnSubmit: '@xvalidateOnSubmit'
        }
    };
};

export default formValidationDirective;
