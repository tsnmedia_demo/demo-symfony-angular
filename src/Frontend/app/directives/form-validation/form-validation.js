import angular from 'angular';
import formValidationDirective from './form-validation.directive';

const formValidationModule = angular.module('app.directive.formValidation', [])

    .directive('dFormValidation', formValidationDirective);

export default formValidationModule;
