import angular from 'angular';

import formValidation from './form-validation/form-validation';
import submitButtonValidation from './submit-button-validation/submit-button-validation';

export default angular.module('app.directives', [
    formValidation.name,
    submitButtonValidation.name
]);
