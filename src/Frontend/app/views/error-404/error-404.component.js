import template from './error-404.html';
import controller from './error-404.controller';

export default {
    template,
    controller,
    controllerAs: 'vError404'
};
