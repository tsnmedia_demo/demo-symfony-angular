import angular from 'angular';
import uiRouter from 'angular-ui-router';
import error404Component from './error-404.component';

const error404Module = angular.module('app.view.error404', [
    uiRouter
])

    .config(($stateProvider, constantsServiceProvider) => {
        const constants = constantsServiceProvider.$get();

        $stateProvider
            .state(constants.get('STATE_404'), {
                template: '<v-error-404><v-error-404>'
            });
    })

    .component('vError404', error404Component);

export default error404Module;
