import angular from 'angular';

import error404 from './error-404/error-404';
import error503 from './error-503/error-503';
import settings from './settings/settings';

export default angular.module('app.views', [
    error404.name,
    error503.name,
    settings.name
]);
