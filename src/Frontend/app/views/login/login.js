(function (window, document) {
    const body = document.getElementsByTagName('body')[0];

    function asyncLoadCss() {
        const link = document.createElement('link');

        link.rel = 'stylesheet';
        link.href = '${STYLE_CSS}';
        link.media = 'only x';

        body.appendChild(link);
    }

    function asyncLoadJs() {
        const script = document.createElement('script');

        script.src = '${MAIN_JS}';
        script.async = true;

        body.appendChild(script);
    }

    window.onload = () => {
        asyncLoadCss();
        asyncLoadJs();
    };
}(window, document));
