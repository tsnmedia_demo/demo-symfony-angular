import angular from 'angular';

const settingsPayoutService = () => {

    const resetIfHasChanged = (context) => {
        return context.currentData;
    };

    const initialize = (context) => {
    };

    return {
        init: initialize,
        resetHiddenFields: resetIfHasChanged
    };
};

export default angular.module('app.view.settingsPayout.service', [])
    .service('settingsPayoutService', settingsPayoutService);
