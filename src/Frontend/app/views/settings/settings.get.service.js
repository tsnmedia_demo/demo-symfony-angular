import angular from 'angular';

const settingsGetService = (
    assignService,
    constantsService,
    apiService
) => {
    const API_SETTINGS_URI = constantsService.get('API_SETTINGS_URI');

    const getSettings = () => {
        return apiService.get(API_SETTINGS_URI);
    };

    return {
        init: getSettings
    };
};

export default angular.module('app.view.settingsGet.service', [])
    .service('settingsGetService', settingsGetService);
