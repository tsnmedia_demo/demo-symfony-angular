import template from './settings.html';
import controller from './settings.controller';

export default {
    template,
    controller,
    controllerAs: 'vSettings'
};
