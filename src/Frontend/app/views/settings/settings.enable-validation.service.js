import angular from 'angular';

const settingsEnableValidationService = (
    constantsService,
    $timeout
) => {
    const EVENT_FROM_VALIDATION_VALIDATE = constantsService.get('EVENT_FROM_VALIDATION_VALIDATE');

    const init = (context) => {};

    return {
        init
    };
};

export default angular.module('app.view.settingsEnableValidation.service', [])
    .service('settingsEnableValidationService', settingsEnableValidationService);
