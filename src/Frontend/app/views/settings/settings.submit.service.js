import angular from 'angular';

const settingsSubmitService = (
    mappingSubmitDataService,
    isEqualService,
    notificationsService,
    dataStoreService,
    constantsService,
    apiService,
    whenService
) => {

    const DATA_STORE_SERVICE = dataStoreService;
    const DATA_SETTINGS_OLD = constantsService.get('DATA_STORE_SETTINGS_OLD');
    const EVENT_FROM_VALIDATION_VALIDATE = constantsService.get('EVENT_FROM_VALIDATION_VALIDATE');

    const getDataToSend = (data, exclude) => {
        return mappingSubmitDataService.set({
            data: data,
            exclude: ['links', 'selfHref', 'updateSettingsHref'].concat(exclude),
            oldData: DATA_STORE_SERVICE.getData(DATA_SETTINGS_OLD)
        });
    };

    const notifyDataIsNotUpdated = () => {
        notificationsService.set('warning', constantsService.get('I18N_MESSAGES_WARNING_0'));
    };

    const sendData = (context, data, form) => {
        const updateSettingsHref = DATA_STORE_SERVICE.getData(DATA_SETTINGS_OLD).updateSettingsHref;

        toggleButtonDataEnabled(form);

        apiService.put(updateSettingsHref, data)
            .then(showMessageByForm.bind(null, context, form))
            .then(context.storeResponse)
            .then(toggleButtonDataEnabled.bind(null, form));
    };

    const isDataEqual = (data) => {
        return isEqualService(data, DATA_STORE_SERVICE.getData(DATA_SETTINGS_OLD));
    };

    const isFormValid = (context, form) => {
        context.scope.$broadcast(EVENT_FROM_VALIDATION_VALIDATE, form.$name);

        return form.$valid;
    };

    const submit = (context, form) => {
        const preSendData = context.resetHiddenFields(context);
        const isDataUpdated = !isDataEqual(preSendData);
        const data = getDataToSend(preSendData);

        whenService(isFormValid(context, form) && isDataUpdated, sendData.bind(null, context, data, form))();
        whenService(!isDataUpdated, notifyDataIsNotUpdated)();
    };

    const submitAccountInfo = (context) => {
        submit(context, context.accountForm);
    };

    const showMessageByForm = (context, form, response) => {
        notificationsService.set(
            'success',
            constantsService.get(`I18N_MESSAGES_SUCCESSFUL_SETTINGS_ACCOUNT_FORM`)
        );

        return response;
    };

    const toggleButtonDataEnabled = (form) => {
        const button = document.querySelector(`form[name="${form.$name}"] button[type="submit"]`);
        const state = !(button.getAttribute('data-enabled') === 'true');

        button.setAttribute('data-enabled', state);
    };

    return {
        submitAccountInfo
    };
};

export default angular.module('app.view.settingsSubmit.service', [])
    .service('settingsSubmitService', settingsSubmitService);
