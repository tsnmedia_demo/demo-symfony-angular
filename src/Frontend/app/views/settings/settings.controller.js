export default class SettingsController {
    constructor(
        $scope,
        settingsService,
        i18nService
    ) {
        this.scope = $scope;
        this.i18nService = i18nService;
        this.settingsService = settingsService;
    }

    $onInit() {
        this.i18n = this.i18nService.all();

        this.currentData = {};

        this.settingsService.init(this);
    }
}
