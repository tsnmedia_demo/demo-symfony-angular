import angular from 'angular';

const settingsStoreResponseService = (
    mergeService,
    linksListToObjectService,
    constantsService,
    dataStoreService
) => {

    const DATA_STORE_SERVICE = dataStoreService;
    const DATA_STORE_SETTINGS_OLD = constantsService.get('DATA_STORE_SETTINGS_OLD');

    const simpleClone = (obj) => {
        return JSON.parse(JSON.stringify(obj));
    };

    const defineCurrentData = (response) => {
        const links = linksListToObjectService.get(response.links);
        const oldData = DATA_STORE_SERVICE.getData(DATA_STORE_SETTINGS_OLD);
        const oldDataCloned = oldData ? simpleClone(oldData) : {};

        return mergeService(oldDataCloned, response, links);
    };

    const storeCurrentData = (context, data) => {
        context.currentData = mergeService(data, context.currentData);
    };

    const storeOldData = (data) => {
        DATA_STORE_SERVICE.freeze(DATA_STORE_SETTINGS_OLD, data);
    };

    const updateDataHeader = (context) => {
        const user = context.currentData.user;

        DATA_STORE_SERVICE.freeze(constantsService.get('DATA_STORE_USER_NAME'), {
            firstName: user.firstName,
            lastName: user.lastName
        });
    };

    const storeResponse = (context, response) => {
        const currentData = defineCurrentData(response.settings || response);
        storeCurrentData(context, simpleClone(currentData));
        storeOldData(simpleClone(currentData));
        updateDataHeader(context);
    };

    return {
        init: storeResponse
    };
};

export default angular.module('app.view.settingsStoreResponse.service', [])
    .service('settingsStoreResponseService', settingsStoreResponseService);
