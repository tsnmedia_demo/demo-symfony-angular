import settingsEnableValidationServiceModule from './settings.enable-validation.service';
import settingsGetServiceModule from './settings.get.service';
import settingsServiceModule from './settings.service';
import settingsStoreResponseServiceModule from './settings.store-response.service';
import settingsSubmitServiceModule from './settings.submit.service';

describe('SettingsView', () => {
    let isEqual = false;

    beforeEach(window.module(
        settingsEnableValidationServiceModule.name,
        settingsGetServiceModule.name,
        settingsServiceModule.name,
        settingsStoreResponseServiceModule.name,
        settingsSubmitServiceModule.name,
        $provide => {

            $provide.service('$timeout', () => {
                return fn => fn();
            });

            $provide.service('$state', () => {
                return {
                    is: () => {},
                    go: () => {}
                };
            });

            $provide.service('constantsService', () => {
                return {
                    get: value => value
                };
            });

            $provide.service('dataStoreService', () => {
                return {
                    getData: () => {},
                    freeze: () => {}
                };
            });

            $provide.service('assignService', () => {});

            $provide.service('i18nService', () => {
                return {
                    get: value => value,
                    all: () => {}
                };
            });

            $provide.service('mergeService', () => ((...objects) => Object.assign(...objects)));

            $provide.service('isEqualService', () => (() => {
                    return isEqual;
                })
            );

            $provide.service('whenService', () => {
                return (cond, fn) => cond ? fn : () => {};
            });

            $provide.service('linksListToObjectService', () => {
                return {
                    get: () => {}
                };
            });

            $provide.service('mappingSubmitDataService', () => {
                return {
                    set: () => {}
                }
            });

            $provide.service('notificationsService', () => {
                return {
                    set: () => {}
                }
            });

            $provide.service('objectReferenceService', () => {
                return {
                    get: () => {},
                    update: () => {}
                }
            });

            $provide.service('apiService', () => {
                return {
                    get: () => {},
                    put: () => {},
                    post: () => {}
                };
            });
        }
    ));

    describe('Service', () => {
        describe('settingsService', () => {
            let i18nService,
                settingsService,
                settingsGetService,
                context,
                deferred,
                promise;

            beforeEach(window.inject((
                _i18nService_,
                _settingsGetService_,
                _settingsService_,
                _$rootScope_,
                _$q_
            ) => {
                i18nService = _i18nService_;
                settingsService = _settingsService_;
                settingsGetService = _settingsGetService_;
                context = {
                    scope: _$rootScope_.$new()
                };

                deferred = _$q_.defer();
                promise = deferred.promise;
            }));

            it('tracks that resetHiddenFields function added', () => {
                spyOn(settingsGetService, 'init').and.returnValue(promise);
                settingsService.init(context);
                expect(context.resetHiddenFields).toEqual(jasmine.any(Function));
            });

            it('tracks that submitAccountInfo function added', () => {
                spyOn(settingsGetService, 'init').and.returnValue(promise);
                settingsService.init(context);
                expect(context.submitAccountInfo).toEqual(jasmine.any(Function));
            });

            it('tracks that storeResponse function added', () => {
                spyOn(settingsGetService, 'init').and.returnValue(promise);
                settingsService.init(context);
                expect(context.storeResponse).toEqual(jasmine.any(Function));
            });

        });

        describe('settingsEnableValidationService', () => {
            let countriesService,
                settingsEnableValidationService,
                context;

            beforeEach(window.inject((
                _settingsEnableValidationService_,
                _$rootScope_
            ) => {
                settingsEnableValidationService = _settingsEnableValidationService_;
                context = {
                    accountForm: {
                        $name: 'testAccountForm'
                    },
                    scope: _$rootScope_.$new(),
                    currentData: {
                        user: {}
                    }
                };
            }));

            it('tracks that event EVENT_FROM_VALIDATION_VALIDATE called', () => {
                spyOn(context.scope, '$broadcast');
                settingsEnableValidationService.init(context);

                expect(context.scope.$broadcast).toHaveBeenCalledWith('EVENT_FROM_VALIDATION_VALIDATE', 'testAccountForm');
            });

            it('tracks that event EVENT_FROM_VALIDATION_VALIDATE did not call', () => {
                spyOn(context.scope, '$broadcast');

                settingsEnableValidationService.init(context);

                expect(context.scope.$broadcast).not.toHaveBeenCalledWith('EVENT_FROM_VALIDATION_VALIDATE', 'testAccountForm');
            });
        });

        describe('settingsStoreResponseService', () => {
            let dataStoreService,
                settingsStoreResponseService,
                response,
                context;

            beforeEach(window.inject((
                _dataStoreService_,
                _settingsStoreResponseService_,
                _$rootScope_
            ) => {
                dataStoreService = _dataStoreService_;
                settingsStoreResponseService = _settingsStoreResponseService_;
                context = {
                    scope: _$rootScope_.$new()
                };
                response = {
                    settings: {
                        links: {
                            0: {
                                href: "/api/entry-point/settings",
                                method: "GET",
                                name: "self",
                                ref: "self"
                            }
                        },
                        password: {
                            href: "http://"
                        },
                        user: {
                            city: "Chalandri",
                            email: "Maria_Smith@yahoo.com",
                            firstName: "Christoph",
                            id: 99894804,
                            lastName: "Weil",
                            postcode: "12345",
                            street: "8 North Brook Lane"
                        }
                    }
                };
            }));

            it('track that update data header', () => {
                spyOn(dataStoreService, 'freeze');

                settingsStoreResponseService.init(context, response);
                expect(dataStoreService.freeze).toHaveBeenCalledWith('DATA_STORE_SETTINGS_OLD', jasmine.any(Object));
            });

            it('track that currentData updated from response', () => {
                spyOn(dataStoreService, 'getData').and.returnValue(null);
                settingsStoreResponseService.init(context, response.settings);
                expect(context.currentData.user).toEqual(response.settings.user);
            });
        });

        describe('settingsGetService', () => {
            let apiService,
                settingsGetService,
                context;

            beforeEach(window.inject((
                _apiService_,
                _settingsGetService_
            ) => {
                apiService = _apiService_;
                settingsGetService = _settingsGetService_;
                context = {};
            }));

            it('track that request API_SETTINGS_URI sent', () => {
                spyOn(apiService, 'get');

                settingsGetService.init(context);
                expect(apiService.get).toHaveBeenCalledWith('API_SETTINGS_URI');
            });
        });

        describe('settingsSubmitService', () => {
            let apiService,
                dataStoreService,
                mappingSubmitDataService,
                notificationsService,
                settingsSubmitService,
                response,
                responseGetData,
                context,
                deferred,
                promise;

            const querySelector = document.querySelector;

            beforeEach(window.inject((
                _dataStoreService_,
                _notificationsService_,
                _apiService_,
                _settingsSubmitService_,
                _mappingSubmitDataService_,
                _$rootScope_,
                _$q_
            ) => {
                dataStoreService = _dataStoreService_;
                mappingSubmitDataService = _mappingSubmitDataService_;
                notificationsService = _notificationsService_;
                apiService = _apiService_;
                settingsSubmitService = _settingsSubmitService_;

                context = {
                    scope: _$rootScope_.$new(),
                    accountForm: {
                        $name: 'testAccountForm',
                        $valid: true
                    },
                    resetHiddenFields: () => {
                        return response.data;
                    },
                    storeResponse: () => {}
                };

                response = {
                    data: {
                        links: {
                            0: {
                                href: "/api/entry-point/settings",
                                method: "GET",
                                name: "self",
                                ref: "self"
                            }
                        },
                        password: {
                            href: "http://"
                        },
                        user: {
                            city: "Chalandri",
                            email: "Maria_Smith@yahoo.com",
                            firstName: "Christoph",
                            id: 99894804,
                            lastName: "Weil",
                            postcode: "12345",
                            street: "8 North Brook Lane"
                        }
                    }
                };
                responseGetData = {
                    updateSettingsHref: 'test'
                };
                deferred = _$q_.defer();
                promise = deferred.promise;

                spyOn(document, 'querySelector').and.returnValue({
                    getAttribute: () => {
                        return true;
                    },
                    setAttribute: () => {}
                });
            }));

            afterEach(() => {
                document.querySelector = querySelector;
                isEqual = false;
            });

            it('track that account info section call get data for send', () => {
                spyOn(dataStoreService, 'getData').and.returnValue(responseGetData);
                spyOn(apiService, 'put').and.returnValue(promise);
                spyOn(mappingSubmitDataService, 'set');
                settingsSubmitService.submitAccountInfo(context);

                expect(mappingSubmitDataService.set).toHaveBeenCalled();
            });

            it('track that account info section sent data', () => {
                spyOn(dataStoreService, 'getData').and.returnValue(responseGetData);
                spyOn(apiService, 'put').and.returnValue(promise);
                spyOn(mappingSubmitDataService, 'set').and.returnValue('test');

                settingsSubmitService.submitAccountInfo(context);

                expect(apiService.put).toHaveBeenCalledWith('test', 'test');
            });

            it('track that account info section show success message', () => {
                spyOn(dataStoreService, 'getData').and.returnValue(responseGetData);
                spyOn(notificationsService, 'set');
                spyOn(apiService, 'put').and.returnValue(promise);

                settingsSubmitService.submitAccountInfo(context);
                deferred.resolve(response);
                context.scope.$digest();

                expect(notificationsService.set).toHaveBeenCalledWith('success', 'I18N_MESSAGES_SUCCESSFUL_SETTINGS_ACCOUNT_FORM');
            });

            it('track that account info section show warning message', () => {
                spyOn(notificationsService, 'set');

                isEqual = true;
                settingsSubmitService.submitAccountInfo(context);

                expect(notificationsService.set).toHaveBeenCalledWith('warning', 'I18N_MESSAGES_WARNING_0');
            });
        });
    });
});
