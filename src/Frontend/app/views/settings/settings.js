import angular from 'angular';
import uiRouter from 'angular-ui-router';
import services from 'services/services';
import settingsComponent from './settings.component';
import settingsService from './settings.service';
import settingsEnableValidationService from './settings.enable-validation.service';
import settingsGetService from './settings.get.service';
import settingsStoreResponseService from './settings.store-response.service';
import settingsSubmitService from './settings.submit.service';
import settingsPayoutService from './settings.payout.service';

const settingsModule = angular.module('app.view.settings', [
    uiRouter,
    services.name,
    settingsService.name,
    settingsEnableValidationService.name,
    settingsGetService.name,
    settingsStoreResponseService.name,
    settingsPayoutService.name,
    settingsSubmitService.name
])

    .config(($stateProvider, constantsServiceProvider) => {
        const constants = constantsServiceProvider.$get();

        $stateProvider
            .state(constants.get('STATE_SETTINGS'), {
                url: constants.get('URI_SETTINGS'),
                template: '<v-settings></v-settings>'
            });
    })

    .component('vSettings', settingsComponent);

export default settingsModule;
