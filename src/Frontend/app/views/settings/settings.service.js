import angular from 'angular';

const settingsService = (
    settingsGetService,
    settingsStoreResponseService,
    settingsSubmitService,
    settingsPayoutService,
    settingsEnableValidationService
) => {
    const init = (context) => {
        context.submitAccountInfo = settingsSubmitService.submitAccountInfo.bind(null, context);
        context.storeResponse = settingsStoreResponseService.init.bind(null, context);
        context.resetHiddenFields = settingsPayoutService.resetHiddenFields.bind(null, context);

        settingsGetService.init()
            .then(context.storeResponse)
            .then(settingsEnableValidationService.init.bind(null, context));
    };

    return {
        init
    };
};

export default angular.module('app.view.settings.service', [])
    .service('settingsService', settingsService);
