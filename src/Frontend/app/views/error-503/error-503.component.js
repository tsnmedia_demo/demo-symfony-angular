import template from './error-503.html';
import controller from './error-503.controller';

export default {
    template,
    controller,
    controllerAs: 'vError503'
};
