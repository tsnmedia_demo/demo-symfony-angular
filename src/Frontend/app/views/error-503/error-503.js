import angular from 'angular';
import uiRouter from 'angular-ui-router';
import error503Component from './error-503.component';

const error503Module = angular.module('app.view.error503', [
    uiRouter
])
    .config(($stateProvider, constantsServiceProvider) => {
        const constants = constantsServiceProvider.$get();

        $stateProvider
            .state(constants.get('STATE_503'), {
                url: constants.get('URI_503'),
                template: '<v-error-503><v-error-503>'
            });
    })
    .component('vError503', error503Component);

export default error503Module;
