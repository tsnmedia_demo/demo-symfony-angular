export default class Error503Controller {
    constructor(
        i18nService,
        constantsService
    ) {
        this.i18nService = i18nService;
        this.constantsService = constantsService;
    }

    $onInit() {
        this.i18n = this.i18nService.all();
        this.link = this.constantsService.get('URI_BASE');
    }
}
