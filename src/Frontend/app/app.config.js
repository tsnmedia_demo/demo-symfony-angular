const appConfig = (
    $qProvider,
    $httpProvider,
    $compileProvider,
    $locationProvider,
    $stateProvider,
    $provide,
    $urlRouterProvider,
    constantsServiceProvider,
    NotificationProvider
) => {
    const constants = constantsServiceProvider.$get();

    $qProvider.errorOnUnhandledRejections(false);

    $compileProvider.debugInfoEnabled(false);

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: true,
        rewriteLinks: false
    });

    $stateProvider
        .state(constants.get('STATE_DASHBOARD'), {
            url: constants.get('URI_DASHBOARD')
        })
        .state(constants.get('STATE_BASE'), {
            url: constants.get('URI_BASE')
        })
        .state(constants.get('STATE_LOGOUT'), {
            onEnter: function($window) {
                $window.open(constants.get('URI_LOGOUT'), '_self');
            }
        });

    $urlRouterProvider.otherwise(($injector) => {
        const $state = $injector.get('$state');

        $state.transitionTo(constants.get('STATE_404'), {}, {location: false});
    });

    NotificationProvider.setOptions({
        delay: 10000,
        startTop: 80,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'top',
        closeOnClick: true
    });
};

export default appConfig;
