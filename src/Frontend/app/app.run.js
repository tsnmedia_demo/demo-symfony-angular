const appRun = ($state, dataStoreService) => {
    dataStoreService.setData('$state', $state);
};

export default appRun;
