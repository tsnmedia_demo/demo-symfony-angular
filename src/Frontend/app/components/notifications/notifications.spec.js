import notificationsStateServiceModule from './notifications.state.service';

describe('NotificationsCompoennt', () => {
    beforeEach(window.module(
        notificationsStateServiceModule.name,
        $provide => {
            $provide.service('constantsService', () => {
                return {
                    get: value => value
                };
            });

            $provide.service('notificationsService', () => {
                return {
                    get: () => {}
                };
            });

            $provide.service('$timeout', () => {
                return fn => fn();
            });
        }
    ));

    describe('Service', () => {
        let notificationsStateService,
            notificationsService,
            rootScope,
            context,
            notification;

        const spyOnScope$on = (event, rememberCallback) => {
            spyOn(context.scope, '$on').and.callFake((ev, fn) => {
                if(ev === event){
                    context.callback = rememberCallback ? fn : fn();
                }
            });
        };

        beforeEach(window.inject((
            _notificationsStateService_,
            _notificationsService_,
            $rootScope
        ) => {
            notificationsStateService = _notificationsStateService_;
            notificationsService = _notificationsService_;
            rootScope = $rootScope;

            context = {
                scope: {
                    $on: () => {}
                },
                addNotification: () => {},
                addClasses: () => {},
                callback: () => {}
            };
            notification = {
                type: '',
                title: 'test'
            };

            spyOn(notificationsService, 'get').and.returnValue(notification);
        }));

        describe('on hideNotification', () => {
            it('tracks that method return allow data', () => {
                expect(notificationsStateService.hideNotification()).toEqual({
                    hide: 'u-display--none'
                });
            });
        });

        describe('on closeNotification', () => {
            it('tracks that service handed classes', () => {
                spyOn(context, 'addClasses');

                notificationsStateService.closeNotification(context.addClasses);

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: 'u-display--none'
                });
            });
        });

        describe('on addListeners', () => {
            it('tracks that added DATA_STORE_NOTIFICATIONS listener', () => {
                const event = 'DATA_STORE_NOTIFICATIONS';
                spyOnScope$on(event, true);
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.scope.$on).toHaveBeenCalledWith(event, context.callback);
            });

            it('tracks that scroll top on DATA_STORE_NOTIFICATIONS', () => {
                const originScrollTo = window.scrollTo;
                spyOn(window, 'scrollTo');

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(window.scrollTo).toHaveBeenCalledWith(0, 0);
                window.scrollTo = originScrollTo;
            });

            it('tracks that broadcasted modal close on DATA_STORE_NOTIFICATIONS', () => {
                spyOn(rootScope, '$broadcast');

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(rootScope.$broadcast).toHaveBeenCalledWith('EVENT_TYPE_MODAL_CLOSE');
            });

            it('tracks that got notification on DATA_STORE_NOTIFICATIONS', () => {
                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(notificationsService.get).toHaveBeenCalled();
            });

            it('tracks that addNotification have been called on DATA_STORE_NOTIFICATIONS', () => {
                spyOn(context, 'addNotification');

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addNotification).toHaveBeenCalledWith(notification);
            });

            it('tracks that added help css classes on DATA_STORE_NOTIFICATIONS', () => {
                spyOn(context, 'addClasses');
                notification.type = 'help';

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: '',
                    x: `u-font-color--01-dark`,
                    background: `trv-maincolor-01-very-light`,
                    title: `u-font-color--01-dark`,
                    description: null,
                    animation: 'u-animation--fade-in-down'
                });
            });

            it('tracks that added help css classes without title on DATA_STORE_NOTIFICATIONS', () => {
                spyOn(context, 'addClasses');
                notification.type = 'help';
                notification.title = '';

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: '',
                    x: `u-font-color--01-dark`,
                    background: `trv-maincolor-01-very-light`,
                    title: `u-font-color--01-dark`,
                    description: null,
                    animation: 'u-animation--fade-in-down'
                });
            });

            it('tracks that added warning css classes on DATA_STORE_NOTIFICATIONS', () => {
                spyOn(context, 'addClasses');
                notification.type = 'warning';

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: '',
                    x: `u-font-color--02-dark`,
                    background: `trv-maincolor-02-very-light`,
                    title: `u-font-color--02-dark`,
                    description: 'u-font-color--02-dark',
                    animation: 'u-animation--fade-in-down'
                });
            });

            it('tracks that added error css classes on DATA_STORE_NOTIFICATIONS', () => {
                spyOn(context, 'addClasses');
                notification.type = 'error';

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: '',
                    x: `u-font-color--03-dark`,
                    background: `trv-maincolor-03-very-light`,
                    title: `u-font-color--03-dark`,
                    description: 'u-font-color--03-dark',
                    animation: 'u-animation--fade-in-down'
                });
            });

            it('tracks that added success css classes on DATA_STORE_NOTIFICATIONS', () => {
                spyOn(context, 'addClasses');
                notification.type = 'success';

                spyOnScope$on('DATA_STORE_NOTIFICATIONS');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: '',
                    x: `u-font-color--05-dark`,
                    background: `trv-maincolor-05-very-light`,
                    title: `u-font-color--05-dark`,
                    description: 'u-font-color--05-dark',
                    animation: 'u-animation--fade-in-down'
                });
            });

            it('tracks that added EVENT_TYPE_NOTIFICATIONS_CLOSE listener', () => {
                const event = 'EVENT_TYPE_NOTIFICATIONS_CLOSE';
                spyOnScope$on(event, true);
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.scope.$on).toHaveBeenCalledWith(event, context.callback);
            });

            it('tracks that hide notification on EVENT_TYPE_NOTIFICATIONS_CLOSE', () => {
                spyOn(context, 'addClasses');
                spyOnScope$on('EVENT_TYPE_NOTIFICATIONS_CLOSE');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: 'u-display--none'
                });
            });

            it('tracks that added $stateChangeStart listener', () => {
                const event = '$stateChangeStart';
                spyOnScope$on(event, true);
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.scope.$on).toHaveBeenCalledWith(event, context.callback);
            });

            it('tracks that hide notification on $stateChangeStart', () => {
                spyOn(context, 'addClasses');
                spyOnScope$on('$stateChangeStart');
                notificationsStateService.addListeners(
                    context.scope,
                    context.addNotification,
                    context.addClasses
                );

                expect(context.addClasses).toHaveBeenCalledWith({
                    hide: 'u-display--none'
                });
            });
        });
    });
});