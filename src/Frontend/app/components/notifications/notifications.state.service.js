import angular from 'angular';

const notificationsStateService = (
    $timeout,
    $rootScope,
    constantsService,
    notificationsService
) => {
    const addListeners = (scope, addNotification, addClasses) => {
        scope.$on(
            constantsService.get('DATA_STORE_NOTIFICATIONS'),
            setNotifications.bind(null, addNotification, addClasses)
        );
        scope.$on(
            constantsService.get('EVENT_TYPE_NOTIFICATIONS_CLOSE'),
            closeNotification.bind(null, addClasses)
        );
        scope.$on(
            '$stateChangeStart',
            closeNotification.bind(null, addClasses)
        );
    };

    const closeNotification = (addClasses) => {
        $timeout(() => { addClasses(hideNotification()); });
    };

    const hideNotification = () => {
        return {
            hide: 'u-display--none'
        };
    };

    const setNotifications = (addNotification, addClasses) => {
        window.scrollTo(0, 0);
        $rootScope.$broadcast(constantsService.get('EVENT_TYPE_MODAL_CLOSE'));
        $timeout(() => {
            const notification = notificationsService.get();
            addNotification(notification);
            setClassesByType(addClasses, notification);
        });
    };

    const setClassesByType = (addClasses, notification) => {
        let classes;

        const type = notification && notification.type ?
            notification.type : null;
        const colors = {
            help: '01',
            warning: '02',
            error: '03',
            success: '05'
        };

        if (type) {
            const color = colors[type];

            classes = {
                hide: '',
                x: `u-font-color--${color}-dark`,
                background: `trv-maincolor-${color}-very-light`,
                title: `u-font-color--${color}-dark`,
                description: type === 'help' ? null : `u-font-color--${color}-dark`,
                animation: 'u-animation--fade-in-down'
            };

            addClasses(classes);
        }
    };

    return {
        addListeners,
        hideNotification,
        closeNotification
    };
};

export default angular.module('app.components.notificationsState.service', [])
    .service('notificationsStateService', notificationsStateService);