export default class NotificationsController {
    constructor(
        $scope,
        i18nService,
        notificationsStateService
    ) {
        this.scope = $scope;
        this.i18nService = i18nService;
        this.notificationsStateService = notificationsStateService;
        this.closeNotification = notificationsStateService.closeNotification.bind(null, this.addClassesData.bind(this));
        this.classes = notificationsStateService.hideNotification();

        this.beforeOnInit();
    }

    $onInit() {
        this.i18n = this.i18nService.all();
    }

    beforeOnInit() {
        this.notificationsStateService.addListeners(
            this.scope,
            this.addNotificationData.bind(this),
            this.addClassesData.bind(this)
        );
    }

    addNotificationData(notification) {
        this.notification = notification;
    }

    addClassesData(classes) {
        this.classes = classes;
    }
}
