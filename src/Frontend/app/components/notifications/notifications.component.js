import template from './notifications.html';
import controller from './notifications.controller';

export default {
    template,
    controller,
    controllerAs: 'cNotification'
};
