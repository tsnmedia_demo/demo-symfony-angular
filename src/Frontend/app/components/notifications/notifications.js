import angular from 'angular';
import notificationsComponent from './notifications.component';
import notificationsStateService from './notifications.state.service';

const notificationsModule = angular.module('app.component.notifications', [notificationsStateService.name])

    .component('cNotifications', notificationsComponent);

export default notificationsModule;
