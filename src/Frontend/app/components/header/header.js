import angular from 'angular';
import headerComponent from './header.component';

const headerModule = angular.module('app.component.header', [])
    .component('cHeader', headerComponent);

export default headerModule;
