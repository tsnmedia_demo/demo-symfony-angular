export default class HeaderController {

    constructor(
        $scope,
        constantsService,
        i18nService
    ) {
        this.scope = $scope;
        this.i18nService = i18nService;
        this.baseUri = constantsService.get('URI_BASE');
    }

    $onInit() {
        this.i18n = this.i18nService.all();
    }
}
