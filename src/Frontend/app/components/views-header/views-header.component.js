import template from './views-header.html';
import controller from './views-header.controller';

export default {
    template,
    controller,
    controllerAs: 'cViewsHeader',
    bindings: {
        view: '@xview',
        message: '@xmessage',
        hideButton: '=xhidebutton'
    }
};
