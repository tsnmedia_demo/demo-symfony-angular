export default class ViewsHeaderController {
    constructor(
        i18nService,
        notificationsService,
        constantsService
    ) {
        this.i18nService = i18nService;
        this.constantsService = constantsService;
        this.notificationsService = notificationsService;
        this.onClick = this.onClickButtonShowHelpInformation;
    }

    $onInit() {
        this.title = this.i18nService.get(this.view);
        this.helpButton = this.i18nService.get('need.help');
    }

    onClickButtonShowHelpInformation() {
        const message = this.message || this.view;

        this.notificationsService.set('help', this.constantsService.get(
            `I18N_MESSAGES_HELP_${message.toUpperCase()}`
        ), this.title);
    }
}
