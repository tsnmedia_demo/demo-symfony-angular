import angular from 'angular';
import viewsHeaderComponent from './views-header.component';

const viewsHeaderModule = angular.module('app.component.viewsHeader', [])

    .component('cViewsHeader', viewsHeaderComponent);

export default viewsHeaderModule;
