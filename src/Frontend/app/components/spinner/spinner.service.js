import angular from 'angular';

const spinnerService = (
    constantsService
) => {
    const C_SPINNER_WATCH = constantsService.get('C_SPINNER_WATCH');

    const watchingPromises = {};

    const getUniqueID = () => {
        return Math.random().toString(16).substr(2);
    };

    const hideSpinner = (promiseId, hide) => {
        delete watchingPromises[promiseId];

        if (!Object.keys(watchingPromises).length) {
            hide();
        }
    };

    const showSpinner = (show) => {
        const promiseId = getUniqueID();
        watchingPromises[promiseId] = true;
        show();

        return promiseId;
    };

    const watch = (showMethod, hideMethod, ev, promise) => {
        const promiseId = showSpinner(showMethod);

        promise.finally(hideSpinner.bind(null, promiseId, hideMethod));
    };

    const init = (scope, showMethod, hideMethod) => {
        scope.$on(C_SPINNER_WATCH, watch.bind(null, showMethod, hideMethod));
    };

    return {
        init
    };
};

export default angular.module('app.component.spinner.service', [])
    .service('spinnerService', spinnerService);
