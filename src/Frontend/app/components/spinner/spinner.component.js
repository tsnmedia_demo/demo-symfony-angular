import template from './spinner.html';
import controller from './spinner.controller';

export default {
    template,
    controller,
    controllerAs: 'cSpinner'
};
