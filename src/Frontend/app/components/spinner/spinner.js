import angular from 'angular';
import spinnerComponent from './spinner.component';
import spinnerService from './spinner.service';

const spinnerModule = angular.module('app.component.spinner', [
    spinnerService.name
])

    .component('cSpinner', spinnerComponent);

export default spinnerModule;
