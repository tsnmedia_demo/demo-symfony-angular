export default class SpinnerController {
    constructor(
        $scope,
        onInitService,
        spinnerService
    ) {
        this.scope = $scope;
        this.spinnerService = spinnerService;

        this.show = false;

        onInitService(this.onInit.bind(this), $scope);
    }

    onInit() {
        this.spinnerService.init(
            this.scope,
            this.showSpinner.bind(this),
            this.hideSpinner.bind(this)
        );
    }

    showSpinner() {
        this.show = true;
    }

    hideSpinner() {
        this.show = false;
    }
}
