import spinnerServiceModule from './spinner.service';

describe('SpinnerComponent', () => {

    beforeEach(window.module(
        spinnerServiceModule.name,
        $provide => {
            $provide.service('constantsService', () => {
                return {
                    get: value => value
                };
            });
        }
    ));

    describe('Service', () => {
        let spinnerService,
            q,
            scope,
            showMethod,
            hideMethod,
            deferred,
            secondDeferred;

        beforeEach(window.inject((
            $q,
            $rootScope,
            _spinnerService_
        ) => {
            spinnerService = _spinnerService_;
            q = $q;

            scope = $rootScope.$new();

            deferred = $q.defer();
            secondDeferred = $q.defer();

            showMethod = jasmine.createSpy('showMethodSpy', () => {});
            hideMethod = jasmine.createSpy('hideMethodSpy', () => {});
        }));

        it('should listen C_SPINNER_WATCH scope event', () => {
            spyOn(scope, '$on');

            spinnerService.init(scope, showMethod, hideMethod);

            expect(scope.$on).toHaveBeenCalledWith('C_SPINNER_WATCH', jasmine.any(Function));
        });

        it('should call show callback', () => {
            spinnerService.init(scope, showMethod, hideMethod);

            scope.$broadcast('C_SPINNER_WATCH', deferred.promise);

            expect(showMethod).toHaveBeenCalled();
        });

        it('shouldn\'t call hide callback, while there are unresolved promises', () => {
            spinnerService.init(scope, showMethod, hideMethod);

            scope.$broadcast('C_SPINNER_WATCH', deferred.promise);
            scope.$broadcast('C_SPINNER_WATCH', secondDeferred.promise);

            deferred.resolve();
            scope.$digest();

            expect(hideMethod).not.toHaveBeenCalled();
        });

        it('should call hide callback', () => {
            spinnerService.init(scope, showMethod, hideMethod);

            scope.$broadcast('C_SPINNER_WATCH', deferred.promise);
            scope.$broadcast('C_SPINNER_WATCH', secondDeferred.promise);

            deferred.resolve({});
            secondDeferred.resolve({});
            scope.$digest();

            expect(hideMethod).toHaveBeenCalled();
        });
    });
});
