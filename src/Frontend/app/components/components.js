import angular from 'angular';

import footer from './footer/footer';
import notifications from './notifications/notifications';
import pushNotifications from './push-notifications/push-notifications';
import spinner from './spinner/spinner';
import viewsHeader from './views-header/views-header';
import header from './header/header';

export default angular.module('app.components', [
    footer.name,
    notifications.name,
    pushNotifications.name,
    spinner.name,
    header.name,
    viewsHeader.name
]);
