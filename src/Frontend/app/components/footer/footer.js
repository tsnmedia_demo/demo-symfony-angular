import angular from 'angular';
import footerComponent from './footer.component';

export default angular.module('app.component.footer', [])
    .component('cFooter', footerComponent);
