import template from './footer.html';
import controller from './footer.controller';

export default {
    template,
    controller,
    controllerAs: 'cFooter'
};
