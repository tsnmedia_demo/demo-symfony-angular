import controller from './push-notifications.controller';

export default {
    restrict: 'E',
    controller,
    controllerAs: 'cPushNotifications'
};