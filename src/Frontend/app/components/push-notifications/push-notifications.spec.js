import pushNotificationsPendingTaskServiceModule from './push-notifications.pending-tasks.service';

describe('PushNotificationsComponent', () => {

    beforeEach(window.module(
        pushNotificationsPendingTaskServiceModule.name,
        $provide => {
            $provide.service('constantsService', () => {
                return {
                    get: value => {
                        const constants = {
                            API_SEO_NOTIFY_UNCOMPLETED_QUEUES_URI: 'API_SEO_NOTIFY_UNCOMPLETED_QUEUES_URI',
                            SEO_PUSH_NOTIFICATION_TIME: 0
                        };
                        
                        return constants[value];
                    }
                };
            });

            $provide.service('i18nService', () => {
                return {
                    get: value => value
                };
            });

            $provide.service('apiService', () => {
                return {
                    get: () => {}
                };
            });

            $provide.service('formatStringService', () => {
                return {
                    add: value => value
                }
            });

            $provide.service('Notification', () => {
                return {
                    primary: value => value
                }
            });

            $provide.service('$interval', () => {
                return fn => fn();
            });
        }
    ));

    describe('Service', () => {
        let interval,
            i18nService,
            apiService,
            formatStringService,
            Notification,
            constantsService,
            pushNotificationsPendingTaskService;

        beforeEach(window.inject((
            _$interval_,
            _i18nService_,
            _apiService_,
            _formatStringService_,
            _Notification_,
            _constantsService_,
            _pushNotificationsPendingTaskService_
        ) => {
            interval = _$interval_;
            i18nService = _i18nService_;
            apiService = _apiService_;
            formatStringService = _formatStringService_;
            Notification = _Notification_;
            constantsService = _constantsService_;
            pushNotificationsPendingTaskService = _pushNotificationsPendingTaskService_;
        }));

        describe('on pushNotificationsPendingTaskService', () => {

            const responseEmpty = {
                data: []
            };

            const responseFilled = {
                data: [
                    {
                        countTasks: 2,
                        queueName: 'test'
                    }
                ]
            };

            const thenFunction = (response, callback) => {
                const result = callback(response);

                return {
                    then: thenFunction.bind(null, result)
                };
            };

            it('should not display notifications message', () => {
                spyOn(Notification, 'primary');
                spyOn(apiService, 'get').and.returnValue({
                    then: thenFunction.bind(null, responseEmpty)
                });

                pushNotificationsPendingTaskService.pushPadingTasks();

                expect(Notification.primary).not.toHaveBeenCalledWith('');
            });

            it('should display notifications message', () => {
                spyOn(Notification, 'primary');
                spyOn(apiService, 'get').and.returnValue({
                    then: thenFunction.bind(null, responseFilled)
                });

                pushNotificationsPendingTaskService.pushPadingTasks();
                expect(Notification.primary).toHaveBeenCalledWith('push.notification.new.rejected.tasks');
            });
        });
    });
});
