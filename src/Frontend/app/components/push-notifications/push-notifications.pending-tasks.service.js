import angular from 'angular';

const pushNotificationsPendingTaskService = (
    $interval,
    i18nService,
    apiService,
    formatStringService,
    Notification,
    constantsService
) => {
    const API_SEO_NOTIFY_UNCOMPLETED_QUEUES_URI = constantsService.get('API_SEO_NOTIFY_UNCOMPLETED_QUEUES_URI');
    const SEO_PUSH_NOTIFICATION_TIME = constantsService.get('SEO_PUSH_NOTIFICATION_TIME');
    const SEO_PUSH_NOTIFICATION_FOR_TOOL_ID = constantsService.get('SEO_PUSH_NOTIFICATION_FOR_TOOL_ID');
    const EVENT_C_PUSH_NOTIFICATION_REJECTED_TASKS =
        constantsService.get('EVENT_C_PUSH_NOTIFICATION_REJECTED_TASKS');
    let interval = null;

    const getPendingTasks = (time) => {
        const url = formatStringService.add(API_SEO_NOTIFY_UNCOMPLETED_QUEUES_URI, {secondes: time});
        return apiService.get(url);
    };

    const pushPadingTasks = () => {
        if (interval === null) {
            interval = $interval(() =>
                getPendingTasks(SEO_PUSH_NOTIFICATION_TIME).then(parseNotifications.bind(this)),
                SEO_PUSH_NOTIFICATION_TIME * 1000);
        }
    };

    const parseNotifications = (response) => {
        response.data.forEach(pushNotification.bind(this));
    };

    const pushNotification = (notification) => {
        Notification.primary(
            i18nService.get('push.notification.new.rejected.tasks', {
                tasksCount: notification.countTasks,
                projectName: notification.queueName
            })
        );
    };

    const init = (rootScope) => {
        rootScope.$on(EVENT_C_PUSH_NOTIFICATION_REJECTED_TASKS, (event, args) => {
            checkTool(args[0]);
        });
    };

    const checkTool = (data) => {
        data.items.forEach(item => {
            if (item.toolId === SEO_PUSH_NOTIFICATION_FOR_TOOL_ID) {
                pushPadingTasks();
            }
        })
    };

    return {
        pushPadingTasks,
        init
    };
};

export default angular.module('app.components.pushNotificationsPendingTaskService.service', [])
    .service('pushNotificationsPendingTaskService', pushNotificationsPendingTaskService);