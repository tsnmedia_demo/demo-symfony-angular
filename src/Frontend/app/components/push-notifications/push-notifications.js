import angular from 'angular';
import pushNotificationsComponent from './push-notifications.component';
import pushNotificationsPendingTaskService from './push-notifications.pending-tasks.service';

const pushNotificationsModule = angular.module('app.component.pushNotifications', [
    pushNotificationsPendingTaskService.name
])

    .component('cPushNotifications', pushNotificationsComponent);

export default pushNotificationsModule;
