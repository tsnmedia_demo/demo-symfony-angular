export default class PushNotificationsController {
    constructor(
        $rootScope,
        pushNotificationsPendingTaskService
    ) {
        this.rootScope = $rootScope;
        this.pushNotificationsPandingTaskService = pushNotificationsPendingTaskService;
    }

    $onInit() {
        this.pushNotificationsPandingTaskService.init(this.rootScope);
    }
}
