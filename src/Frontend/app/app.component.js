import template from './app.html';

let appComponent = () => {
    return {
        restrict: 'E',
        scope: {},
        template
    };
};

export default appComponent;
