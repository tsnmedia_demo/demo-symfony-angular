#!/bin/bash

echo "Starting login-add-main-js.sh";

# We should execute npm run deploy before this script
cd ../../web

echo "Replacing main.*.js file name in login.*.js"
MAIN_JS_FILE=$(find assets/ -name "main.*.js")
LOGIN_JS_FILE=$(find assets/ -name "login.*.js")
TEMP_LOGIN_JS_FILE="tmp-login.js"
sed "s|\${MAIN_JS}|$MAIN_JS_FILE|g" "$LOGIN_JS_FILE" > "$TEMP_LOGIN_JS_FILE" || exit $?
mv "$TEMP_LOGIN_JS_FILE" "$LOGIN_JS_FILE" || exit $?
echo "Replaced main.*.js file name in login.*.js"

exit 0
