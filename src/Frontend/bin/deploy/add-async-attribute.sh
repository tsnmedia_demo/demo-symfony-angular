#!/bin/bash

echo "Starting add-async-attribute.sh";

DIR="$( cd -P "$( dirname "$SOURCE" )/../../" && pwd )"
WEB=$DIR"/web/assets/"

replace() {
    FILE=$WEB$1;
    sed 's/<script src/<script async src/g' "$FILE.html" > "$FILE-temp.html";
    mv "$FILE-temp.html" "$FILE.html";
    echo "Added async attribute to $FILE.html";
}

replace "login";
replace "main";

exit 0
