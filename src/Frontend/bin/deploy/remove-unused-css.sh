#!/bin/bash

##
# Unused CSS
# @see: https://davidwalsh.name/uncss
# @see: https://github.com/giakki/uncss
##

echo "Starting remove-unused-css.sh";

# We should execute npm run deploy before this script
DIR="$( cd -P "$( dirname "$SOURCE" )/../../" && pwd )"
ASSETS_FOLDER="../../web/assets"
STYLE_CSS=$(find $ASSETS_FOLDER -name "style.*.css")
TMP_CSS=$ASSETS_FOLDER"/temp.css"
# List of classes to do not remove. Most of them have been used on JS files
DO_NOT_REMOVE=$(echo "
/::-ms-clear/,
/::-ms-expand/,
/[disabled]/,
.angucomplete-description,
.angucomplete-dropdown,
.angucomplete-holder,
.angucomplete-row,
.angucomplete-selected-row,
.angucomplete-searching,
.angucomplete-title,
/\.c-form-validation/,
/\.c-vote-box/,
.e-icon--clock-line-dark,
.e-icon--poi-citycentre-line-dark,
.e-icon--settings-04-lighter,
.e-icon--settings-grey,
.e-icon--settings-light,
.e-icon--tools-grey,
.e-icon--tools-light,
/\.is-active/,
/\.is-invalid/,
.trv-maincolor-01-very-light,
.trv-maincolor-02-very-light,
.trv-maincolor-03-very-light,
.trv-maincolor-05-very-light,
.u-align--container-vertical,
.u-align--vertical,
.u-animation--fade-in-down,
.u-display--none,
.u-font-color--01-dark,
.u-font-color--02-dark,
.u-font-color--03-dark,
.u-font-color--05-dark,
.u-padding-bottom,
.u-padding-top
" | tr -d '\n')

echo "Removing unused CSS";
# Added an extra "folder" ../ because uncss is in app/ to get html files
# Exclude main.html because a template "error" window.globalConfig.i18n =  {%# o.htmlWebpackPlugin.options.language.enGB %}
shopt -s extglob
node_modules/.bin/uncss -i $DO_NOT_REMOVE -s "../"$STYLE_CSS app/**/!(main).html > $TMP_CSS
shopt -u extglob
echo "Removed unused CSS";

echo "Cleaning CSS";
node_modules/.bin/cleancss $TMP_CSS --s0 -o $STYLE_CSS
echo "Cleaned CSS";

echo "Remove temp.css";
rm $TMP_CSS
echo "Removed temp.css";

exit 0
