#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"

echo "Starting copy-images.sh"

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )/../../" && pwd )" # one folder out
cd $DIR

IMAGE_FOLDER=../../web/assets/images

# remove folder
rm -rf $IMAGE_FOLDER;
if [[ $? != 0 ]]; then
    exit $?
fi;
echo "Removed "$IMAGE_FOLDER;

# create folder
mkdir $IMAGE_FOLDER;
if [[ $? != 0 ]]; then
    exit $?
fi;
echo "Created "$IMAGE_FOLDER;

# copy images
cp -r app/images/* $IMAGE_FOLDER;
if [[ $? != 0 ]]; then
    exit $?
fi;
echo "Copied own images to "$IMAGE_FOLDER;

exit 0
