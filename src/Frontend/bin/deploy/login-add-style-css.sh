#!/bin/bash

echo "Starting login-add-style-css.sh";

# We should execute npm run deploy before this script
cd ../../web

echo "Replacing style.*.css file name in login.*.js"
STYLE_CSS_FILE=$(find assets/ -name "style.*.css")
LOGIN_JS_FILE=$(find assets/ -name "login.*.js")
TEMP_LOGIN_JS_FILE="tmp-login.js"

sed "s|\${STYLE_CSS}|$STYLE_CSS_FILE|g" "$LOGIN_JS_FILE" > "$TEMP_LOGIN_JS_FILE" || exit $?
mv "$TEMP_LOGIN_JS_FILE" "$LOGIN_JS_FILE" || exit $?
echo "Replaced style.*.css file name in login.*.js"

exit 0
