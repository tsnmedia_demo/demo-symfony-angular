#!/bin/bash

##
# Unused CSS
# @see: https://davidwalsh.name/uncss
# @see: https://github.com/giakki/uncss
##

echo "Starting login-critical-css.sh";

# We should execute npm run deploy before this script
DIR="$( cd -P "$( dirname "$SOURCE" )/../../" && pwd )"
ASSETS_FOLDER="../../web/assets"
STYLE_CSS=$(find $ASSETS_FOLDER -name "style.*.css")
TMP_CSS=$ASSETS_FOLDER"/temp-login.css"
# List of classes to do not remove. Most of them have been used on JS files
DO_NOT_REMOVE="/v-login/,/is-loading/,/u-animation--fade-in/"

echo "Removing unused CSS";
# Added an extra "folder" ../../../ because uncss is in app/app/views/login to get html files
node_modules/.bin/uncss -i $DO_NOT_REMOVE -s "../../../"$STYLE_CSS app/views/login/login.html > $TMP_CSS
echo "Removed unused CSS";

echo "Cleaning login CSS";
node_modules/.bin/cleancss $TMP_CSS --s0 -o $TMP_CSS
echo "Cleaned login CSS";

echo "Adding critical path to login.html";
cd $ASSETS_FOLDER
sed -i "/\${CRITICAL_CSS}/{
    s/\${CRITICAL_CSS}//g
    r temp-login.css
}" "login.html";
echo "Critical path added";

echo "Remove temp-login.css";
rm $TMP_CSS
echo "Removed temp-login.css";

exit 0
