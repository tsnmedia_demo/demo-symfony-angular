'use strict';

module.exports = function (test) {
    var obj = {
        login: './app/views/login/login.js',
        style: './.tmp/app.css',
        main: './app/app.js'
    };

    return test ? {} : obj;
};
