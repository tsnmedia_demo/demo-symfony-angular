'use strict';

const fs = require('fs');
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const NgAnnotatePlugin = require('ng-annotate-webpack-plugin');
const webpack = require('webpack');

module.exports = function (test, production, live) {

    const htmlMinify = {
        removeComments: true,
        collapseWhitespace: true,
        removeRedundantAttributes: true,
        collapseBooleanAttributes: true
    };

    const googleTagManager = live ? fs.readFileSync(
        './app/components/google-tag-manager/google-tag-manager.html'
    ) : null;

    const googleAnalytics = live ? fs.readFileSync(
        './app/components/google-analytics/google-analytics.html'
    ) : null;

    /**
     * Generating asset manifests
     * @see https://www.npmjs.com/package/webpack-manifest-plugin
     * @see https://medium.com/@okonetchnikov/long-term-caching-of-static-assets-with-webpack-1ecb139adb95
     */
    const manifest = production ? new ManifestPlugin({
        fileName: '../webpack.manifest.json'
    }) : null;

    /**
     * Allows exporting a manifest that maps entry chunk names to their output files
     * @see https://github.com/diurnalist/chunk-manifest-webpack-plugin
     * @see https://medium.com/@okonetchnikov/long-term-caching-of-static-assets-with-webpack-1ecb139adb95
     */
    const chunkManifest = production ? new ChunkManifestPlugin({
        filename: '../webpack.chunk-manifest.json',
        manifestVariable: 'webpackManifest'
    }) : null;

    /**
     * Assign the module and chunk ids by occurrence count.
     * @type {webpack.optimize.OccurenceOrderPlugin}
     * @see http://webpack.github.io/docs/list-of-plugins.html#occurenceorderplugin
     */
    const occurenceOrderPlugin = production ?
        new webpack.optimize.OccurenceOrderPlugin() : null;

    /**
     * Explicit login chunk
     * Split login.js.
     * @type {webpack.optimize.CommonsChunkPlugin}
     * @see https://webpack.github.io/docs/list-of-plugins.html#2-explicit-vendor-chunk
     */
    const loginCommonsChunkPlugin = test ? null :
        new webpack.optimize.CommonsChunkPlugin({
            name: ['login'],
            minChunks: Infinity,
            chunks: ['login']
        });

    /**
     * Render login.html
     * Skip rendering login.html in test mode
     * @see https://github.com/ampedandwired/html-webpack-plugin
     */
    const loginHtml = test ? null : new HtmlWebpackPlugin({
        filename: 'login.html',
        production: production,
        googleTagManager: googleTagManager,
        googleAnalytics: googleAnalytics,
        template: './app/views/login/login.html',
        inject: 'body',
        excludeChunks: ['main', 'style']
    });

    /**
     * Render main.html
     * Skip rendering main.html in test mode
     * @see https://github.com/ampedandwired/html-webpack-plugin
     */
    const mainHtml = test ? null : new HtmlWebpackPlugin({
        // fakes index page for dev
        filename: production ? 'main.html' : 'index.html',
        template: './app/main.html',
        inject: 'body',
        env: production ? 'production' : 'dev',
        googleTagManager: googleTagManager,
        googleAnalytics: googleAnalytics,
        language: {
            enGB: fs.readFileSync('./i18n/en-GB.json'),
            enGBcountries: fs.readFileSync('./i18n/en-GB-countries.json'),
            enGBlanguages: fs.readFileSync('./i18n/en-GB-languages.json')
        },
        excludeChunks: ['login'],
        minify: production ? htmlMinify : null
    });

    /**
     * Runs ng-annotate on your bundles
     * @see https://github.com/jeffling/ngmin-webpack-plugin
     */
    const ngAnnotatePlugin = new NgAnnotatePlugin({add: true});

    /**
     * Only emit files when there are no errors
     * @see http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin
     */
    const noErrorsPlugin = production ? new webpack.NoErrorsPlugin() : null;

    /**
     * Dedupe modules in the output
     * @see http://webpack.github.io/docs/list-of-plugins.html#dedupeplugin
     */
    const dedupePlugin = production ? new webpack.optimize.DedupePlugin() : null;

    /**
     * Minify all javascript, switch loaders to minimizing mode
     * @see http://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
     */
    const uglifyJsPlugin = production ? new webpack.optimize.UglifyJsPlugin({
            mangle: false
        }) : null;

    /**
     * @see https://webpack.github.io/docs/stylesheets.html#styles-from-initial-chunks-into-separate-css-output-file
     */
    const cssExtractTextPlugin = production ?
        new ExtractTextPlugin('[name].[chunkhash].css') : null;

    return [
        chunkManifest,
        cssExtractTextPlugin,
        dedupePlugin,
        loginCommonsChunkPlugin,
        loginHtml,
        mainHtml,
        manifest,
        ngAnnotatePlugin,
        noErrorsPlugin,
        occurenceOrderPlugin,
        uglifyJsPlugin
    ].filter(function (item) {
        return item !== null;
    });
};

