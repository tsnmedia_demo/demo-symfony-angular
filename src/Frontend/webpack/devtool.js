'use strict';

module.exports = function (test, production) {
    var devTool = production ? 'source-map' : 'eval';

    console.log('production', production, 'test', test);

    // @see: https://github.com/karma-runner/karma/issues/2024
    return test ? 'inline-cheap-source-map' : devTool;
};
