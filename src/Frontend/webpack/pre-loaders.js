'use strict';

module.exports = function (test) {
    /**
     * Isparta loader
     * Istanbul instrumenter loader for webpack
     * coverage reporting
     * @see https://github.com/deepsweet/istanbul-instrumenter-loader
     */
    var arr = [
        {
            test: /\.service\.js$/,
            exclude: [
                /\.init\.service\.js$/
            ],
            include: [
                process.cwd() + '/app/components',
                process.cwd() + '/app/services',
                process.cwd() + '/app/views'
            ],
            loader: 'isparta'
        }
    ];

    return test ? arr : [];
};
