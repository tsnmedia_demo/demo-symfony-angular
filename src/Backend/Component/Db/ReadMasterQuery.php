<?php

namespace Backend\Component\Db;

class ReadMasterQuery extends ReadQueryAbstract
{
    /**
     * Get Db Pool.
     *
     * @return string
     */
    public function getDbPool()
    {
        return DbPoolAbstract::APP_READ_MASTER;
    }
}
