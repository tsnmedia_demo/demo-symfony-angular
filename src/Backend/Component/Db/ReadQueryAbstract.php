<?php

namespace Backend\Component\Db;

use Backend\Component\Util\DateTimeUtil;
use Doctrine\DBAL\DBALException;

abstract class ReadQueryAbstract extends QueryAbstract
{
    const TIME_VAR_NAME          = 'Time';
    const ROWS_AFFECTED_VAR_NAME = 'RowsAffected';

    /**
     * Check if is read query.
     *
     * @return bool
     */
    public function isReadOnly()
    {
        return true;
    }

    /**
     * Returns the first row of the result
     * as an associative array.
     *
     * @param string $query
     *
     * @return array
     */
    protected function fetchAssoc($query)
    {
        $startTimeMs = DateTimeUtil::getTime();

        $result = $this->getConnection()->fetchAssoc($query);

        $endTimeMs = DateTimeUtil::getTime();

        $this->logQuery($query, $endTimeMs, $startTimeMs, $result);

        return $result;
    }

    /**
     * Return a object.
     *
     * @param string $query
     * @param string $class Full name class (with namespace)
     *
     * @return mixed
     *
     * @throws DBALException
     */
    protected function fetchObject($query, $class = '\stdClass')
    {
        $startTimeMs = DateTimeUtil::getTime();

        $result = $this->getConnection()->executeQuery($query, [], [])->fetchObject($class);

        $endTimeMs = DateTimeUtil::getTime();

        $this->logQuery($query, $endTimeMs, $startTimeMs, $result);

        return $result;
    }

    /**
     * Returns the result as an associative array.
     *
     * @param string $query
     *
     * @return array
     */
    protected function fetchAll($query)
    {
        $startTimeMs = DateTimeUtil::getTime();

        $result = $this->getConnection()->fetchAll($query);

        $endTimeMs = DateTimeUtil::getTime();

        $this->logQuery($query, $endTimeMs, $startTimeMs, $result);

        return $result;
    }

    /**
     * Returns the result as an associative array of objects.
     *
     * @param string      $query
     * @param string|null $class Full name class (with namespace).
     *
     * @return array
     */
    protected function fetchAllObject($query, $class = '\stdClass')
    {
        $startTimeMs = DateTimeUtil::getTime();

        $result = $this->getConnection()->executeQuery($query)->fetchAll(\PDO::FETCH_CLASS, $class);

        $endTimeMs = DateTimeUtil::getTime();

        $this->logQuery($query, $endTimeMs, $startTimeMs, $result);

        return $result;
    }

    /**
     * Returns column.
     *
     * @param string $query
     *
     * @return mixed
     */
    protected function fetchColumn($query)
    {
        $startTimeMs = DateTimeUtil::getTime();

        $result = $this->getConnection()->fetchColumn($query);

        $endTimeMs = DateTimeUtil::getTime();

        $this->logQuery($query, $endTimeMs, $startTimeMs, $result);

        return $result;
    }

    /**
     * Returns the result as an associative array with arrays of object indexed by the column name.
     *
     * @param string      $query
     * @param string      $objectProperty
     * @param string|null $class          Full name class (with namespace).
     *
     * @return array
     */
    protected function fetchAllIndexedObjects($query, $objectProperty, $class = '\stdClass')
    {
        $startTimeMs = DateTimeUtil::getTime();

        $result = $this->getConnection()->executeQuery($query)->fetchAll(\PDO::FETCH_CLASS, $class);

        $endTimeMs = DateTimeUtil::getTime();

        $indexedObjects = [];
        foreach ($result as $object) {
            $indexedObjects[$object->{$objectProperty}][] = $object;
        }

        $this->logQuery($query, $endTimeMs, $startTimeMs, $result);

        return $indexedObjects;
    }

    /**
     * Returns the result as an associative array of objects indexed by the column name.
     *
     * @param string      $query
     * @param string      $objectProperty
     *
     * @return array
     */
    protected function fetchIndexedObjects($query, $objectProperty)
    {
        $startTimeMs = DateTimeUtil::getTime();

        $result = $this->getConnection()->executeQuery($query)->fetchAll(\PDO::FETCH_CLASS);

        $endTimeMs = DateTimeUtil::getTime();

        $indexedObjects = [];
        foreach ($result as $object) {
            $indexedObjects[$object->{$objectProperty}] = $object;
        }

        $this->logQuery($query, $endTimeMs, $startTimeMs, $result);

        return $indexedObjects;
    }

    /**
     * @param $query
     * @param $endTimeMs
     * @param $startTimeMs
     * @param $result
     */
    protected function logQuery($query, $endTimeMs, $startTimeMs, $result)
    {
        $affectedRows = ($result) ? count($result) : 0;

        $extraData = [
            static::TIME_VAR_NAME => ($endTimeMs - $startTimeMs) / 1000,
            static::ROWS_AFFECTED_VAR_NAME => $affectedRows,
        ];
        $this->writeLog($query, array_merge($this->getExtraDataLog(), $extraData));
    }
}
