<?php

namespace Backend\Component\Db;

class DbFormat
{
    const DATE_FORMAT      = 'Y-m-d';
    const DATE_TIME_FORMAT = 'Y-m-d H:i:s';
}
