<?php

namespace Backend\Component\Db;

class GlobalWriteQuery extends WriteQueryAbstract
{
    /**
     * Get Db Pool.
     *
     * @return string
     */
    public function getDbPool()
    {
        return DbPoolAbstract::APP_GLOBAL_WRITE;
    }
}
