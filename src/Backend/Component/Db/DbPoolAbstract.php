<?php

namespace Backend\Component\Db;

abstract class DbPoolAbstract
{
    const APP_GLOBAL_READ   = 'app_global_read_connection';
    const APP_GLOBAL_WRITE  = 'app_global_write_connection';
    const APP_READ_MASTER   = 'app_read_master_connection';
    const APP_READ_SLAVE    = 'app_read_slave_connection';
    const APP_WRITE_DEFAULT = 'app_write_default_connection';
}
