<?php

namespace Backend\Component\Db;

class WriteDefaultQuery extends WriteQueryAbstract
{
    /**
     * Get Db Pool.
     *
     * @return string
     */
    public function getDbPool()
    {
        return DbPoolAbstract::APP_WRITE_DEFAULT;
    }
}
