<?php

namespace Backend\Component\Db;

class DbName
{
    const APP        = 'app.';
    const APP_GLOBAL = 'app_global.';
}
