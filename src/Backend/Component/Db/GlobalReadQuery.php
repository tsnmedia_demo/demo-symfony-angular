<?php

namespace Backend\Component\Db;

class GlobalReadQuery extends ReadQueryAbstract
{
    /**
     * Get Db Pool.
     *
     * @return string
     */
    public function getDbPool()
    {
        return DbPoolAbstract::APP_GLOBAL_READ;
    }
}
