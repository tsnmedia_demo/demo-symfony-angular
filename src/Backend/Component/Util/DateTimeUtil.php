<?php

namespace Backend\Component\Util;

use DateTime;

class DateTimeUtil
{
    const DATE_YEAR_HOUR_MINUTES_SECOND_FORMAT = 'Y-m-d H:i:s';
    const INVALID_DATE_MSG = 'Year/month/day are not set or are not valid';

    /**
     * Get time (ms).
     *
     * @return float
     */
    public static function getTime()
    {
        return microtime(true) * 1000;
    }

    /**
     * Get diff date with format passed like argument.
     *
     * @param string $dateTimeStart
     * @param string $dateTimeEnd
     *
     * @return int
     */
    public static function getDateDiff($dateTimeStart, $dateTimeEnd)
    {
        $dateTimeStart = new DateTime($dateTimeStart);
        $dateTimeEnd = new DateTime($dateTimeEnd);

        return $dateTimeEnd->getTimestamp() - $dateTimeStart->getTimestamp();
    }

    /**
     * @param int $year
     * @param int $month
     * @param int $day
     *
     * @return DateTime
     *
     * @throws InvalidDateException
     */
    public static function getDate($year, $month, $day)
    {
        $date = DateTime::createFromFormat('Y-m-d h', sprintf('%d-%d-%d 0', $year, $month, $day));
        $dateTimeErrors = DateTime::getLastErrors();

        if (!empty($dateTimeErrors['warning_count']) || !empty($dateTimeErrors['error_count'])) {
            throw new InvalidDateException(static::INVALID_DATE_MSG);
        }

        return $date;
    }

    /**
     * Given a year and month, it returns a DateTime object with the last day and last second of the given month.
     *
     * i.e.: Given 2016, 2 it will return a DateTime object with '2016-02-29 23:59:59'
     *
     * @param int $year
     * @param int $month
     *
     * @return DateTime
     *
     * @throws InvalidDateException
     */
    public static function getLastSecondOfMonthDate($year, $month)
    {
        /* @var DateTime $firstDayOfNextMonthInSecondsDate */
        $firstDayOfNextMonthDate = static::getDate($year, $month, 1)->modify('first day of next month');

        return DateTime::createFromFormat('U', $firstDayOfNextMonthDate->format('U') - 1);
    }
}
