<?php

namespace Backend\Component\Util;

use Iterator;

class ArrayUtil
{
    /**
     * Count words in array including arrays with multibyte encodings via splitting by space.
     *
     * @param Iterator $iterator
     * @param int      $startNum
     *
     * @return bool;
     */
    public static function getWordsCount(Iterator $iterator, &$startNum)
    {
        $words = preg_split('/\s+/', trim($iterator->current()), -1, PREG_SPLIT_NO_EMPTY);
        $startNum = (int) $startNum + count($words);

        return true;
    }

    /**
     * Count characters in array including arrays with multibyte encodings.
     *
     * @param Iterator $iterator
     * @param int      $startNum
     *
     * @return bool;
     */
    public static function getCharactersCount(Iterator $iterator, &$startNum)
    {
        $characters = mb_strlen(trim($iterator->current()));
        $startNum = (int) $startNum + $characters;

        return true;
    }

    /**
     * Getting values recursive from array and return single array.
     *
     * @param array $array
     *
     * @return array
     */
    public static function arrayValuesRecursive(array $array)
    {
        $flat = [];
        foreach ($array as $value) {
            if (is_array($value)) {
                $flat = array_merge($flat, static::arrayValuesRecursive($value));
            } else {
                $flat[] = $value;
            }
        }

        return $flat;
    }

    /**
     * Makes html_entity_decode recursive
     *
     * @param array $array
     *
     * @return array
     */
    public static function arrayHtmlEntityDecode(array $array)
    {
        array_walk_recursive(
            $array,
            function (&$value) {
                $value = is_string($value) ? html_entity_decode($value) : $value;
            }
        );

        return $array;
    }

    /**
     * Will return Formatted Queues by languageCode
     *
     * @param array $queues
     *
     * @return array
     */
    public static function queuesFormatByLanguage(array $queues)
    {
        $formattedQueues = [];
        foreach ($queues as $queue) {
            $language = $queue['languageCode'];
            unset($queue['languageCode']);

            if (!isset($formattedQueues[$language])) {
                $formattedQueues[$language] = [];
            }

            array_push($formattedQueues[$language], $queue);
        }

        return $formattedQueues;
    }

    /**
     * @param array $types
     *
     * @return string
     */
    public static function getContentTypeHash(array $types)
    {
        sort($types);

        return implode('_', $types);
    }

    /**
     * @param array  $array
     * @param string $index
     *
     * @return array
     */
    public static function getIndexArrayOfObjects($array, $index)
    {
        $array = array_combine(array_map(function ($obj) use ($index) {
            return $obj->{'get'.ucfirst($index)}();
        }, $array), $array);

        return $array;
    }
}
