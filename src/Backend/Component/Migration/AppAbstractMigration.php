<?php

namespace Backend\Component\Migration;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AppAbstractMigration extends AbstractMigration implements ContainerAwareInterface
{
    /** @var ContainerInterface $container */
    protected $container;

    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritdoc}
     */
    public function preUp(Schema $schema)
    {
        $condition = $this->connection->getDatabase() !== $this->getDatabaseName();
        $this->skipIf($condition, 'Skipped because it isn\'t a migration for this connection');
    }

    /**
     * {@inheritdoc}
     */
    public function preDown(Schema $schema)
    {
        $condition = $this->connection->getDatabase() !== $this->getDatabaseName();
        $this->skipIf($condition, 'Skipped because it isn\'t a migration for this connection');
    }

    /**
     * @return string
     */
    abstract public function getDatabaseName();
}
