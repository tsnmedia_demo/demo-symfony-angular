<?php

namespace Backend\Component\Migration;

abstract class AppGlobalMigration extends AppAbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function getDatabaseName()
    {
        return $this->container->getParameter('global_database_name');
    }
}
