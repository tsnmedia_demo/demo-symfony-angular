<?php

namespace Backend\Component\Migration;

abstract class AppDefaultMigration extends AppAbstractMigration
{
    /**
     * {@inheritdoc}
     */
    public function getDatabaseName()
    {
        return $this->container->getParameter('master_database_name');
    }
}
