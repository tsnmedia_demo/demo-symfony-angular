<?php

namespace Backend\Component\Menu;

interface MenuProviderInterface
{
    const DIC_NAME = 'Backend.Component.Menu.MenuProviderInterface';

    /**
     * @return array
     */
    public function getMenu();

    /**
     * @return string
     */
    public function getLastToolLabel();
}
