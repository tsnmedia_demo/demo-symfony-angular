<?php

namespace Backend\Component\Menu;

interface MenuPermissionInterface
{
    const DIC_NAME = 'Backend.Component.Menu.MenuPermissionInterface';

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasNodePermission($role);
}
