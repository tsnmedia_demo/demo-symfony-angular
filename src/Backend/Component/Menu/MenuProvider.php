<?php

namespace Backend\Component\Menu;

use Backend\Entity\Security\Role;
use Backend\Entity\Security\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MenuProvider implements MenuProviderInterface
{
    /** @var MenuPermissionInterface $menuPermission */
    protected $menuPermission;

    /** @var TokenStorageInterface $securityContext */
    protected $tokenStorage;

    /**
     * @var array list of menu items. Each menu item should be an array of the following structure:
     *
     * - label: string, optional, specifies the menu item label.
     * - url: string, optional, specifies the URL of the menu item.
     * - visible: boolean, optional, whether this menu item is visible. Defaults to true.
     * - items: array, optional, specifies the sub-menu items. Its format is the same as the parent items.
     */
    public $items = [];

    /**
     * @var boolean whether to hide empty menu items. An empty menu item is one whose `url` option is not
     * set and which has no visible child menu items.
     */
    public $hideEmptyItems = true;

    /** @var User $user */
    public $user;

    /**
     * MenuProvider constructor.
     *
     * @param MenuPermissionInterface $menuPermission
     * @param TokenStorageInterface   $tokenStorage
     */
    public function __construct(
        MenuPermissionInterface $menuPermission,
        TokenStorageInterface $tokenStorage
    ) {
        $this->menuPermission = $menuPermission;
        $this->tokenStorage = $tokenStorage;

        $this->user = $this->tokenStorage->getToken()->getUser();

        $this->items = [
            'user'  => [
                'label'   => $this->user->getFullName(),
                'visible' => $this->menuPermission->hasNodePermission(Role::USER),
                'items'   => [
                    [
                        'label'   => 'settings',
                        'url'     => '/settings',
                        'visible' => $this->menuPermission->hasNodePermission(Role::USER),
                    ],
                    [
                        'label' => 'logout',
                        'url'   => '/oauth-logout',
                    ],
                ],
            ],
            'tools' => [
                'label'   => 'tools',
                'items'   => $this->getToolItems(),
                'visible' => $this->menuPermission->hasNodePermission(Role::USER),
            ],
            'admin' => [
                'label'   => 'menu.admin',
                'state'   => 'STATE_ADMIN_DASHBOARD',
                'visible' => $this->menuPermission->hasNodePermission(Role::ADMIN),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getMenu()
    {
        $items = $this->normalizeItems($this->items);

        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function getLastToolLabel()
    {

        $toolLabel = null;
        $toolId = $this->user->getLastTool();

        if ($toolId) {
            $tools = array_column($this->getToolItems(), 'label', 'toolId');
            $toolLabel = isset($tools[$toolId]) ? $tools[$toolId] : null;
        }

        return $toolLabel;
    }

    /**
     * @param array $items
     * @param bool  $isFirstLevel
     *
     * @return array
     */
    protected function normalizeItems($items, $isFirstLevel = true)
    {

        foreach ($items as $i => $item) {
            if (isset($item['visible']) && !$item['visible']) {
                unset($items[$i]);
                continue;
            } else {
                unset($items[$i]['visible']);
            }

            if (isset($item['items'])) {
                $items[$i]['items'] = $this->normalizeItems($item['items'], false);
                if (empty($items[$i]['items']) && $this->hideEmptyItems) {
                    unset($items[$i]['items']);
                    if (!isset($item['url'])) {
                        unset($items[$i]);
                        continue;
                    }
                }
            }
        }

        return $isFirstLevel ? $items : array_values($items);
    }

    protected function getToolItems()
    {
        return [
            [
                'label'   => 'menu.item.1',
                'url'     => '/tools/menu-item-1',
                'visible' => $this->menuPermission->hasNodePermission(Role::USER),
                'toolId'  => Role::USER_ID,
            ],
            [
                'label'   => 'menu.item.2',
                'url'     => '/tools/menu-item-2',
                'visible' => $this->menuPermission->hasNodePermission(Role::USER),
                'toolId'  => Role::USER_ID,
            ]
        ];
    }
}
