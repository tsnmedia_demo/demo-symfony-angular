<?php

namespace Backend\Component\Menu;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MenuPermission implements MenuPermissionInterface
{
    /** @var AuthorizationCheckerInterface $authorizationChecker */
    protected $authorizationChecker;

    /**
     * MenuPermission constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     */
    public function __construct(
        AuthorizationCheckerInterface $authorizationChecker
    ) {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function hasNodePermission($role)
    {
        $isGranted = false;

        if ($this->authorizationChecker->isGranted($role)) {
            $isGranted = true;
        }

        return $isGranted;
    }
}
