<?php

namespace Backend\Repository\User;

use Backend\Entity\Security\User;

interface UserRepositoryInterface
{
    const DIC_NAME = 'Backend.Repository.User.UserRepositoryInterface';

    /**
     * @param int $id
     *
     * @return User
     */
    public function getUser($id);

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getUserRoles($userId);

    /**
     * @param int $userId
     *
     * @return User
     */
    public function getUserWithRoles($userId);

    /**
     * @param User $user
     *
     * @return int
     */
    public function createUser(User $user);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function updateUser(User $user);

    /**
     * @param int $userId
     *
     * @return int
     */
    public function deleteAllUserRoles($userId);

    /**
     * @param array $userIds
     *
     * @return array
     */
    public function getUsersName($userIds);
}
