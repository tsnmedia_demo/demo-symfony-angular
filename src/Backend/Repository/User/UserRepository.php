<?php

namespace Backend\Repository\User;

use Backend\Entity\Security\User;
use Backend\Query\User\GetUser;
use Backend\Query\User\GetUserRoles;
use Backend\Query\User\GetUsersName;
use Backend\Query\User\StoreUser;
use Backend\Query\User\UpdateUser;
use Backend\Repository\RepositoryAbstract;

class UserRepository extends RepositoryAbstract implements UserRepositoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function getUser($id)
    {
        /** @var GetUser $query */
        $query = $this->getQuery(GetUser::DIC_NAME);

        return $query->getResult($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getUserRoles($userId)
    {
        /** @var GetUserRoles $query */
        $query = $this->getQuery(GetUserRoles::DIC_NAME);

        return array_column($query->getResult($userId), 'roleId');
    }

    /**
     * {@inheritdoc}
     */
    public function getUserWithRoles($userId)
    {
        /** @var User $user */
        $user = $this->getUser($userId);

        if ($user) {
            $user->setRoles($this->getUserRoles($user->getId()));
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function createUser(User $user)
    {
        /** @var StoreUser $query */
        $query = $this->getQuery(StoreUser::DIC_NAME);

        return $query->getResult($user);
    }

    /**
     * {@inheritdoc}
     */
    public function updateUser(User $user)
    {
        /** @var UpdateUser $query */
        $query = $this->getQuery(UpdateUser::DIC_NAME);

        return $query->getResult($user);
    }

    /**
     * {@inheritdoc}
     */
    public function deleteAllUserRoles($userId)
    {
        /** @var DeleteAllUserRoles $query */
        $query = $this->getQuery(DeleteAllUserRoles::DIC_NAME);

        return $query->getResult($userId);
    }

    /**
     * {@inheritdoc}
     */
    public function getUsersName($userIds)
    {
        /** @var GetUsersName $query */
        $query = $this->getQuery(GetUsersName::DIC_NAME);

        return $query->getResult($userIds);
    }
}
