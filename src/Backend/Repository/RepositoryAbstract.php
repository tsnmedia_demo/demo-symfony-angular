<?php

namespace Backend\Repository;

use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class RepositoryAbstract
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * RepositoryAbstract constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Gets query for given service id from DIC.
     *
     * @param string $serviceId
     *
     * @return object
     */
    protected function getQuery($serviceId)
    {
        return $this->container->get($serviceId);
    }
}
