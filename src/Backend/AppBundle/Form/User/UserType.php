<?php

namespace Backend\AppBundle\Form\User;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Backend\Entity\Security\User;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class UserType extends AbstractType
{
    /**
     * The first column the method ->add belong to mapping put in frontend.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class)
            ->add('id', IntegerType::class)
            ->add('lastName', TextType::class)
            ->add('email', EmailType::class)
            ->add('city', TextType::class)
            ->add('street', TextType::class)
            ->add('postcode', TextType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $defaults = [
            'data_class' => User::class,
        ];

        $resolver->setDefaults($defaults);
    }

    /**
     * The request we get field user type information with key returned with this method.
     *
     * @return string
     */
    public static function getAliasName()
    {
        return 'user';
    }
}
