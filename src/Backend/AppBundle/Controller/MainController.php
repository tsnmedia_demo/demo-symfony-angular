<?php

namespace Backend\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Backend\BusinessCase\MainBusinessCaseInterface;
use Backend\ValueObject\Api\Response\MainResponse;
use Symfony\Component\HttpFoundation\JsonResponse;

class MainController extends Controller
{
    /**
     * @Nelmio\ApiDocBundle\Annotation\ApiDoc(
     *  section="Main",
     *  resource=true,
     *  description="Get needed information for the first request, as menu and user",
     *  statusCodes={
     *     200 = "Returned when successful"
     *  }
     * )
     *
     * @return JsonResponse
     */
    public function indexAction()
    {
        /** @var MainBusinessCaseInterface $mainBusinessCase */
        $mainBusinessCase = $this->get(MainBusinessCaseInterface::DIC_NAME);

        /** @var MainResponse $mainResponse */
        $mainResponse = $mainBusinessCase->getMainResponse();

        return new JsonResponse($mainResponse->getResponse(), JsonResponse::HTTP_OK);
    }
}
