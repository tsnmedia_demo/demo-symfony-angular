<?php

namespace Backend\AppBundle\Controller\User;

use Backend\AppBundle\Form\Settings\SettingsType;
use Backend\AppBundle\Form\User\UserType;
use Backend\AppBundle\Controller\Base\BaseController;
use Backend\BusinessCase\User\UserSettingsBusinessCaseInterface;
use Backend\Entity\Security\User;
use Backend\ValueObject\Api\Link;
use Backend\ValueObject\Settings\Settings;
use Backend\Service\Form\InvalidFormException;
use Backend\Service\LinkBuilder\LinkBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserSettingsController extends BaseController
{
    /** Is main key to response data to the view */
    const ROOT_PARAMETER = 'settings';

    /**
     * @Nelmio\ApiDocBundle\Annotation\ApiDoc(
     *  section="User",
     *  resource=true,
     *  description="Get info user settings",
     *  statusCodes={
     *     200 = "Returned when successful",
     *     400 = "Returned when user data can not be found"
     *  }
     * )
     *
     * @return JsonResponse
     */
    public function getInfoSettingsAction()
    {
        /** @var UserSettingsBusinessCaseInterface $userSettingsBusinessCase */
        $userSettingsBusinessCase = $this->getUserSettingsBC();

        /** @var User $user */
        $user = $userSettingsBusinessCase->getUser(1);

        /** @var Settings $settings */
        $settings = $userSettingsBusinessCase->getSettings($user);
        $settingsResponse = $this->createViewResponse($settings, SettingsType::class);

        $settingsResponse[static::ROOT_PARAMETER] = array_merge(
            $settingsResponse[static::ROOT_PARAMETER],
            [],
            $this->getApiLinks()
        );

        return new JsonResponse($settingsResponse, JsonResponse::HTTP_OK);
    }

    /**
     * @Nelmio\ApiDocBundle\Annotation\ApiDoc(
     *  section="User",
     *  resource=true,
     *  description="Save user settings",
     *  statusCodes={
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors",
     *     500 = "Returned when there is an error with invoicing API"
     *  }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setInfoSettingsAction(Request $request)
    {
        $contentRequest = json_decode($request->getContent(), true);

        try {
            $settingsResponse = $this->updateSettings($contentRequest);
            $status = JsonResponse::HTTP_OK;
        } catch (InvalidFormException $exception) {
            $settingsResponse = $this->getErrorResponse($exception);
            $status = JsonResponse::HTTP_BAD_REQUEST;
        }

        return new JsonResponse($settingsResponse, $status);
    }

    /**
     * @param array $contentRequest
     *
     * @return array
     */
    protected function updateSettings(array $contentRequest)
    {
        $settingsResponse = [];

        if (isset($contentRequest[UserType::getAliasName()])) {
            $settingsResponse = $this->updateUser($contentRequest[UserType::getAliasName()]);
        }

        return $settingsResponse;
    }

    /**
     * @param array $userData
     *
     * @return array
     */
    protected function updateUser(array $userData)
    {
        $this->saveUser($userData);

        $userUpdated = $this->getUserSettingsBC()->getUser(1);

        return $this->createViewResponse($userUpdated, UserType::class);
    }

    /**
     * @param array $userData
     */
    protected function saveUser(array $userData)
    {
        $user = new User();

        /** @var User $userEntity */
        $userEntity = $this->processFormValidation($user, UserType::class, $userData);
        $userEntity->setId(1);

        $this->getUserSettingsBC()->saveUser($userEntity);
    }

    /**
     * @return array
     */
    protected function getApiLinks()
    {
        /** @var LinkBuilderInterface $linkBuilder */
        $linkBuilder = $this->get(LinkBuilderInterface::DIC_NAME);

        $selfLink = new Link(
            'self',
            'user_settings_get_info',
            'self',
            [],
            'GET'
        );

        $updateSettingsLink = new Link(
            'update-settings',
            'user_settings_set_info',
            'update-settings',
            [],
            'PUT'
        );

        return $linkBuilder->getFormattedLinks(
            [
                $selfLink,
                $updateSettingsLink,
            ]
        );
    }

    /**
     * @return UserSettingsBusinessCaseInterface
     */
    protected function getUserSettingsBC()
    {
        return $this->get(UserSettingsBusinessCaseInterface::DIC_NAME);
    }

    /**
     * @return array
     */
    private function getOauthSettingsLink()
    {
        return [
            'password' => ['href' => $this->getParameter('oauth_settings')],
        ];
    }
}
