<?php

namespace Backend\AppBundle\Controller;

use Backend\ValueObject\Frontend\JavascriptError;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Backend\Service\Frontend\FrontendServiceInterface;
use Symfony\Component\HttpFoundation\Request;
use Backend\Entity\Security\User;
use Backend\Repository\User\UserRepositoryInterface;

class FrontendController extends Controller
{
    /**
     * @Nelmio\ApiDocBundle\Annotation\ApiDoc(
     *  section="Logs",
     *  resource=true,
     *  description="Log javascript error",
     *  statusCodes={
     *     200 = "Returned when successful"
     *  }
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logAction(Request $request)
    {
        $userRepository = $this->get(UserRepositoryInterface::DIC_NAME);

        /** @var User $user */
        $user = $userRepository->getUser(1);

        $message = $request->getContent();

        /** @var JavascriptError $javascriptError */
        $javascriptError = new JavascriptError(
            $user->getId(),
            $request->getClientIp(),
            $request->headers->get('user-agent'),
            $message
        );

        /** @var FrontendServiceInterface $frontendService */
        $frontendService = $this->get(FrontendServiceInterface::DIC_NAME);
        $frontendService->log($javascriptError);

        return new JsonResponse(null, JsonResponse::HTTP_OK);
    }
}
