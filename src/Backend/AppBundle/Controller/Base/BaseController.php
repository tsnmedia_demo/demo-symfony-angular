<?php

namespace Backend\AppBundle\Controller\Base;

use Backend\Service\Form\FormServiceInterface;
use Backend\Service\Form\InvalidFormException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BaseController extends Controller
{
    /**
     * @param mixed  $entity
     * @param string $type
     * @param array  $parameters
     * @param array  $options
     *
     * @return mixed $entity with data included in $parameters
     *
     * @throws InvalidFormException
     */
    protected function processFormValidation($entity, $type, array $parameters, array $options = [])
    {
        /** @var FormServiceInterface $formService */
        $formService = $this->get(FormServiceInterface::DIC_NAME);

        return $formService->processForm($entity, $type, $parameters, $options);
    }

    /**
     * @param $entity
     * @param string $type
     *
     * @return array
     */
    protected function createViewResponse($entity, $type)
    {
        /** @var FormServiceInterface $formService */
        $formService = $this->get(FormServiceInterface::DIC_NAME);

        return $formService->createViewResponse($entity, $type);
    }

    /**
     * @param InvalidFormException $exception
     *
     * @return array
     */
    protected function getErrorResponse(InvalidFormException $exception)
    {
        /** @var FormServiceInterface $formService */
        $formService = $this->get(FormServiceInterface::DIC_NAME);

        return $formService->getErrorResponse($exception);
    }
}
