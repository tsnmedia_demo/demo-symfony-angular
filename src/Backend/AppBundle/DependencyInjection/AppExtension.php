<?php

namespace Backend\AppBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AppExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('app.driver', $config['driver']);
        $container->setParameter('app.authorized.query', $config['authorized']['query']);
        $container->setParameter('app.authorized.cookie', $config['authorized']['cookie']);
        $container->setParameter('app.authorized.attributes', $config['authorized']['attributes']);
        $container->setParameter('app.response.http_code', $config['response']['code']);
        $container->setParameter('app.response.http_status', $config['response']['status']);
        $container->setParameter('app.types', $config['types']);

        if (isset($config['driver']['options']['dsn'])) {
            $this->registerDsnconfiguration($config['driver']['options']);
        }
    }
}
