<?php

namespace Backend\AppBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class LogCompiler implements CompilerPassInterface
{
    /**
     * Process.
     *
     * Method setLogger come from Backend\Component\Log\LogTrait.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        $taggedServices = $container->findTaggedServiceIds('app.logger');

        foreach ($taggedServices as $serviceId => $tagAttributes) {
            if (!isset($tagAttributes[0]['channel'])) {
                throw new \RuntimeException('Hey dude! if you put tag app.logger put attribute channel as well ;-)');
            }

            $definition = $container->getDefinition($serviceId);
            $nameChannel = $tagAttributes[0]['channel'];
            $nameLoggerChannelService = 'monolog.logger.'.$nameChannel;
            $definition->addMethodCall('setLogger', [new Reference($nameLoggerChannelService)]);
        }
    }
}
