<?php

namespace Backend\AppBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode = $treeBuilder->root('app');

        $rootNode
            ->addDefaultsIfNotSet()
                ->children()
                    ->arrayNode('authorized')
                        ->addDefaultsIfNotSet()
                            ->children()
                                ->variableNode('query')
                                    ->defaultValue(array())
                                ->end()
                                ->variableNode('cookie')
                                    ->defaultValue(array())
                                ->end()
                                ->scalarNode('routes')
                                    ->defaultValue(array())
                                ->end()
                                ->variableNode('attributes')
                                    ->defaultValue(array())
                                ->end()
                            ->end()
                        ->end()
                ->arrayNode('driver')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('class')
                                ->defaultNull()
                            ->end()
                            ->variableNode('options')
                                ->defaultValue(array())
                            ->end()
                        ->end()
                    ->end()
                ->arrayNode('types')
                    ->addDefaultsIfNotSet()
                        ->children()
                            ->arrayNode('full_down')
                                ->addDefaultsIfNotSet()
                                    ->children()
                                        ->variableNode('commands')
                                            ->defaultValue(array())
                                        ->end()
                                        ->variableNode('routes')
                                            ->defaultValue(array())
                                        ->end()
                                    ->end()
                                ->end()
                            ->arrayNode('prepare_to_shutdown')
                                ->addDefaultsIfNotSet()
                                    ->children()
                                        ->variableNode('commands')
                                            ->defaultValue(array())
                                        ->end()
                                        ->variableNode('routes')
                                            ->defaultValue(array())
                                        ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->arrayNode('response')
                    ->addDefaultsIfNotSet()
                        ->children()
                        ->integerNode('code')
                            ->defaultValue(503)
                        ->end()
                        ->scalarNode('status')
                            ->defaultValue('Service Temporarily Unavailable')
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
