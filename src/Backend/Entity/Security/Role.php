<?php

namespace Backend\Entity\Security;

class Role
{
    // Default role, needs to be assigned to every logged user or symfony will consider them anonymous
    const ADMIN    = 'ROLE_ADMIN';
    const USER     = 'ROLE_USER';
    const ADMIN_ID = 0;
    const USER_ID  = 1;

    const ROLE_ID_ROLE_NAME = [
        self::ADMIN_ID  => self::ADMIN,
        self::USER_ID   => self::USER,
    ];

    /**
     * @param int $roleId
     *
     * @return string
     */
    public static function getRole($roleId)
    {
        if ($roleId == static::ADMIN_ID) {
            return static::ADMIN;
        } else {
            return static::ROLE_ID_ROLE_NAME[$roleId];
        }
    }

    /**
     * @param string $roleName
     *
     * @return int|bool
     */
    public static function getRoleId($roleName)
    {
        if ($roleName == static::ADMIN) {
            return static::ADMIN_ID;
        } else {
            return array_search($roleName, static::ROLE_ID_ROLE_NAME);
        }
    }

    /**
     * @return array
     */
    public static function getAllRoleIds()
    {
        return array_keys(static::ROLE_ID_ROLE_NAME);
    }
}
