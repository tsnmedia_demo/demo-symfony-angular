<?php

namespace Backend\Entity\Security;

use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

class User implements UserInterface, EquatableInterface, \Serializable
{
    const SYSTEM_USER_ID = 0;

    /**
     * Array with user attributes.
     * This can be used form modules to store logged in user specific data into session.
     *
     * @var array
     */
    protected $attributes;

    /**
     * List of assigned roles.
     *
     * @var string[]
     */
    protected $roles;

    /**
     * Logged in user id.
     *
     * @var int
     */
    public $id;

    /**
     * User display username.
     *
     * @var string
     */
    protected $username;

    /**
     * @var string
     *
     * @Assert\Type(type="string", message="ALPHABETIC|{{ value }}")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @Assert\Type(type="string", message="ALPHABETIC|{{ value }}")
     */
    protected $lastName;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     *
     * @Assert\Type(type="string", message="ALPHABETIC|{{ value }}")
     * @Assert\NotBlank(message="NOT_BLANK|City")
     */
    protected $city;

    /**
     * @var string
     *
     * @Assert\Type(type="string", message="ALPHABETIC|{{ value }}")
     * @Assert\NotBlank(message="NOT_BLANK|Street")
     */
    protected $street;

    /**
     * @var string
     *
     * @Assert\Type(type="string", message="ALPHABETIC|{{ value }}")
     * @Assert\NotBlank(message="NOT_BLANK|Postcode")
     */
    protected $postcode;

    /**
     * User encrypted password.
     *
     * @var string
     */
    protected $password;

    /**
     * This property is not saved in database
     *
     * @var int
     */
    protected $level;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = $this->initRoles();
    }

    /**
     * @param array $attributes
     *
     * @return User
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param [] $roleIds
     *
     * @return User
     */
    public function setRoles(array $roleIds)
    {
        foreach ($roleIds as $roleId) {
            $this->roles[] = Role::getRole($roleId);
        }

        return $this;
    }

    /**
     * Reset all custom roles to default
     */
    public function resetRoles()
    {
        $this->roles = $this->initRoles();
    }

    /**
     * Get user_id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->firstName.' '.$this->lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     *
     * @return User
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     *
     * @return User
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * This property is not saved in database, is only in memory.
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * This property is not saved in database, is only in memory.
     *
     * @param int $level
     */
    public function setLevel($level)
    {
        $this->level = (int) $level;
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return ''; // not used
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        // nothing to do here
    }

    /**
     * {@inheritdoc}
     */
    public function isEqualTo(UserInterface $otherUser)
    {
        if (!$otherUser instanceof self) {
            return false;
        }

        return $this->serialize() === $otherUser->serialize();
    }

    /**
     * {@inheritdoc}
     */
    public function serialize()
    {
        return serialize(
            [
                $this->attributes,
                $this->roles,
                $this->id,
                $this->username,
                $this->firstName,
                $this->lastName,
                $this->email,
                $this->city,
                $this->street,
                $this->postcode,
                $this->password,
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized)
    {
        $unserialized = unserialize($serialized);

        $this->attributes = $unserialized[0];
        $this->roles = $unserialized[1];
        $this->id = $unserialized[2];
        $this->username = $unserialized[3];
        $this->firstName = $unserialized[4];
        $this->lastName = $unserialized[5];
        $this->email = $unserialized[6];
        $this->city = $unserialized[7];
        $this->street = $unserialized[8];
        $this->postcode = $unserialized[9];
        $this->password = $unserialized[10];
    }

    /**
     * Check if User filled required for payments fields.
     *
     * @return bool
     */
    public function areRequiredSettingsFilled()
    {
        $fields = [
            $this->firstName,
            $this->lastName,
            $this->email,
            $this->city,
            $this->street,
            $this->postcode,
        ];
        if (array_filter(
            $fields,
            function ($field) {
                return empty($field);
            }
        )) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Get init roles.
     *
     * @return array
     */
    protected function initRoles()
    {
        return [Role::USER];
    }
}
