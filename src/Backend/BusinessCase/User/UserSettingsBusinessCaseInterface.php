<?php

namespace Backend\BusinessCase\User;

use Backend\Entity\Security\User;
use Backend\ValueObject\Settings\Settings;

interface UserSettingsBusinessCaseInterface
{
    const DIC_NAME = 'Backend.BusinessCase.User.UserSettingsBusinessCaseInterface';

    /**
     * @param User $user
     *
     * @return Settings
     */
    public function getSettings(User $user);

    /**
     * @param User $user
     */
    public function saveUser(User $user);

    /**
     * @param int $userId
     *
     * @return User
     */
    public function getUser($userId);
}
