<?php

namespace Backend\BusinessCase\User;

use Backend\Entity\Security\User;
use Backend\Repository\User\UserRepositoryInterface;
use Backend\ValueObject\Settings\Settings;

class UserSettingsBusinessCase implements UserSettingsBusinessCaseInterface
{
    /** @var UserRepositoryInterface $userRepository */
    protected $userRepository;

    /**
     * @param UserRepositoryInterface    $userRepository
     */
    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function getSettings(User $user)
    {
        $settings = new Settings();
        $settings->setUser($this->normalizeUser($user));

        return $settings;
    }

    /**
     * {@inheritdoc}
     */
    public function saveUser(User $user)
    {
        $this->userRepository->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function getUser($userId)
    {
        return $this->userRepository->getUserWithRoles($userId);
    }

    /**
     * @param User $user
     *
     * @return User $user
     */
    protected function normalizeUser($user)
    {
        if (is_null($user->getFirstName())) {
            $user->setFirstName('');
        }
        if (is_null($user->getLastName())) {
            $user->setLastName('');
        }

        return $user;
    }
}
