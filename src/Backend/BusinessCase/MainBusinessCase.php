<?php

namespace Backend\BusinessCase;

use Backend\Service\Mapper\Api\Response\MainResponseMapper;

class MainBusinessCase implements MainBusinessCaseInterface
{
    /** @var  MainResponseMapper $mainResponseMapper */
    protected $mainResponseMapper;

    /**
     * @param MainResponseMapper $mainResponseMapper
     */
    public function __construct(MainResponseMapper $mainResponseMapper)
    {
        $this->mainResponseMapper = $mainResponseMapper;
    }

    /**
     * {@inheritdoc}
     */
    public function getMainResponse()
    {
        return $this->mainResponseMapper->map();
    }
}
