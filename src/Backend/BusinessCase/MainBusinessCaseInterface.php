<?php

namespace Backend\BusinessCase;

use Backend\ValueObject\Api\Response\MainResponse;

interface MainBusinessCaseInterface
{
    const DIC_NAME = 'Backend.BusinessCase.User.MainBusinessCaseInterface';

    /**
     * @return MainResponse
     */
    public function getMainResponse();
}
