<?php

namespace Backend\ValueObject\Api\Response;

class MainResponse extends Response
{
    protected $menu;

    /** @var  string $lastToolLabel */
    protected $lastToolLabel;

    /**
     * @return string
     */
    public function getLastToolLabel()
    {
        return $this->lastToolLabel;
    }

    /**
     * @param string $lastToolLabel
     */
    public function setLastToolLabel($lastToolLabel)
    {
        $this->lastToolLabel = $lastToolLabel;
    }

    /**
     * @return array
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param array $menu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        $response = [
            'lastToolLabel' => $this->getLastToolLabel(),
            'menu'     => $this->getMenu(),
        ];

        return $response;
    }
}
