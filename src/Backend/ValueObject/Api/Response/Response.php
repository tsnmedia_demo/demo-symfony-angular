<?php

namespace Backend\ValueObject\Api\Response;

abstract class Response
{
    /** @var array */
    protected $links;

    /** @var array */
    protected $locked;

    /**
     * @return array
     */
    public function getLinks()
    {
        return $this->links ? $this->links : [];
    }

    /**
     * @param array $links
     */
    public function setLinks(array $links)
    {
        $this->links = $links;
    }

    /**
     * @return string
     */
    public function getLocked()
    {
        return $this->links;
    }

    /**
     * @param string $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * This has to build the needed response in every response class.
     *
     * @return array
     */
    abstract public function getResponse();
}
