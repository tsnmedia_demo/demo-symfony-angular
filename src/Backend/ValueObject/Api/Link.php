<?php

namespace Backend\ValueObject\Api;

class Link
{
    protected $name;
    protected $route;
    protected $rel;
    protected $routeParameters;
    protected $method;

    /**
     * Link constructor.
     *
     * @param string $name
     * @param string $route
     * @param string $rel
     * @param array  $routeParameters
     * @param null   $method
     */
    public function __construct($name, $route, $rel, array $routeParameters = [], $method = null)
    {
        $this->name = $name;
        $this->route = $route;
        $this->rel = $rel;
        $this->routeParameters = $routeParameters;
        $this->method = $method ? strtoupper($method) : null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return string
     */
    public function getRel()
    {
        return $this->rel;
    }

    /**
     * @return array
     */
    public function getRouteParameters()
    {
        return $this->routeParameters;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }
}
