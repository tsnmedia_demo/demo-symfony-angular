<?php

namespace Backend\ValueObject\Settings;

use Symfony\Component\Validator\Constraints as Assert;
use Backend\Entity\Security\User;

class Settings
{
    /**
     * @var User
     *
     * @Assert\Type(type="Backend\ValueObject\Payment\User")
     * @Assert\Valid()
     */
    protected $user;

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }
}
