<?php

namespace Backend\ValueObject\Error;

/**
 * These number errors are using in assert annotations and controller in some cases.
 */
class ErrorMessage
{
    const ALPHABETIC       = 0;
    const BIC              = 1;
    const IBAN             = 2;
    const EMAIL            = 3;
    const NOT_BLANK        = 4;
    const ONLY_NUMBERS     = 5;
    const LETTER_AND_SPACE = 6;
    const UNDEFINED        = 7;
    const NOT_TASK         = 8;
    /*
     * This error message is returned when trying to save a bank account and
     * the invoicing api throws an ApiInvalidCall exception (Bad request)
     */
    const IBAN_BIC                         = 9;
    const WORDS_NUMBER_NOT_SATISFIED       = 10;
    const MAX_PROPERTY_LENGTH_EXCEED       = 11; // Exceeded the maximum limit of characters
    const LANGUAGE_NOT_SET                 = 12; // Language needs to be set
    const INVALID_ACTION                   = 13; // Invalid Action
    const UNEXPECTED_REQUEST               = 14; // Required variables not specified or not supported by this action
    const INVALID_DATE                     = 15; // Year/month/day are not set or are not valid
    const MIN_CITY_LENGTH                  = 16; // Minimum characters length for city name not reached
    const AM_ERROR_SAVE                    = 17; // Hotel ID is not valid or task has already been submitted
    const INVALID_ITEM                     = 18; // Item ID is not valid.
    const REVIEW_TITLE_OR_TEXT_EMPTY       = 19; // Title and text must not be empty in group (TAF-717)
    const TASK_ALREADY_SUBMITTED_EXCEPTION = 20; // Task has already been submitted.
    const MIN_PROPERTY_LENGTH_LESS         = 21; // Less than the minimum limit of characters
    const INFORMATION_ON_PAGE_NOT_ACTUAL   = 22; // The information on this page is no longer up to date.
    const FEEDBACK_TEXT_EMPTY              = 23; // Feedback field can be empty if rejections less than max number (TAF-1140).
}
