<?php

namespace Backend\ValueObject\Frontend;

class JavascriptError
{
    protected $userId;
    protected $ip;
    protected $userAgent;
    protected $message;

    /**
     * JavascriptError constructor.
     *
     * @param int    $userId
     * @param string $ip
     * @param string $userAgent
     * @param string $message
     */
    public function __construct($userId, $ip, $userAgent, $message)
    {
        $this->userId = $userId;
        $this->ip = $ip;
        $this->userAgent = $userAgent;
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @return string
     */
    public function getUserAgent()
    {
        return $this->userAgent;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }
}
