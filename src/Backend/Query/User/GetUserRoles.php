<?php

namespace Backend\Query\User;

use Backend\Component\Db\GlobalReadQuery;

class GetUserRoles extends GlobalReadQuery
{
    const DIC_NAME = 'Backend.Query.User.GetUserRoles';

    /**
     * @param int $userId
     *
     * @return array
     */
    public function getResult($userId)
    {
        $query = 'SELECT';
        $query .=  ' ur.role_id AS roleId';
        $query .= ' FROM';
        $query .=  ' user_role ur';
        $query .= ' WHERE';
        $query .=  ' ur.user_id = '.(int) $userId;

        return $this->fetchAll($query);
    }
}
