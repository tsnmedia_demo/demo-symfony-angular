<?php

namespace Backend\Query\User;

use Backend\Component\Db\GlobalReadQuery;
use Backend\Entity\Security\User;

class GetUser extends GlobalReadQuery
{
    const DIC_NAME = 'Backend.Query.User.GetUser';

    /**
     * @param int $id
     *
     * @return User
     */
    public function getResult($id)
    {
        $query = 'SELECT';
        $query .=  ' u.id';
        $query .=  ', u.username';
        $query .=  ', u.first_name AS firstName';
        $query .=  ', u.last_name AS lastName';
        $query .=  ', u.email';
        $query .=  ', u.city';
        $query .=  ', u.street';
        $query .=  ', u.postcode';
        $query .= ' FROM';
        $query .=  ' user u';
        $query .= ' WHERE';
        $query .=  ' u.id = '.(int) $id;

        return $this->fetchObject($query, User::class);
    }
}
