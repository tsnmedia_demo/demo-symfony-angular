<?php

namespace Backend\Query\User;

use Backend\Component\Db\GlobalWriteQuery;
use Backend\Entity\Security\User;

class UpdateUser extends GlobalWriteQuery
{
    const DIC_NAME = 'Backend.Query.User.UpdateUser';

    /**
     * @param User $user
     *
     * @return int
     */
    public function getResult(User $user)
    {
        $firstName = $user->getFirstName() ? $this->quote($user->getFirstName()) : 'first_name';
        $lastName = $user->getLastName() ? $this->quote($user->getLastName()) : 'last_name';
        $email = $user->getEmail() ? $this->quote($user->getEmail()) : 'email';
        $city = $user->getCity() ? $this->quote($user->getCity()) : 'city';
        $street = $user->getStreet() ? $this->quote($user->getStreet()) : 'street';
        $postcode = $user->getPostcode() ? $this->quote($user->getPostcode()) : 'postcode';

        $query = 'UPDATE';
        $query .= ' user';
        $query .= ' SET';
        $query .= ' first_name = '.$firstName;
        $query .= ', last_name = '.$lastName;
        $query .= ', email = '.$email;
        $query .= ', city = '.$city;
        $query .= ', street = '.$street;
        $query .= ', postcode = '.$postcode;
        $query .= ' WHERE';
        $query .= ' id = '.(int) $user->getId();

        return $this->executeUpdate($query);
    }
}
