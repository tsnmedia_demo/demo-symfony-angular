<?php

namespace Backend\Query\User;

use Backend\Component\Db\GlobalReadQuery;

class GetUsersName extends GlobalReadQuery
{
    const DIC_NAME = 'Backend.Query.User.GetUsersName';

    /**
     * @param array $userIds
     *
     * @return array
     */
    public function getResult($userIds)
    {
        $query = 'SELECT';
        $query .=  ' u.id';
        $query .=  ', u.username';
        $query .= ' FROM';
        $query .=  ' user u';
        $query .= ' WHERE ';
        $query .=  ' u.id IN ('.implode(', ', $this->quoteFromArray($userIds)).')';
        $query .= ' ORDER BY u.username ASC';

        return $this->fetchAll($query);
    }
}
