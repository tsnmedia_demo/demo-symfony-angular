<?php

namespace Backend\Query\User;

use Backend\Component\Db\GlobalWriteQuery;
use Backend\Entity\Security\User;

class StoreUser extends GlobalWriteQuery
{
    const DIC_NAME = 'Backend.Query.User.StoreUser';

    /**
     * @param User $user
     *
     * @return int
     */
    public function getResult(User $user)
    {
        $firstName = is_null($user->getFirstName()) ? 'NULL' : $this->quote($user->getFirstName());
        $lastName = is_null($user->getLastName()) ? 'NULL' : $this->quote($user->getLastName());

        $query = 'INSERT INTO';
        $query .=  ' user';
        $query .= ' (';
        $query .=   'id';
        $query .=   ', username';
        $query .=   ', first_name';
        $query .=   ', last_name';
        $query .=   ', email';
        $query .=  ')';
        $query .= ' VALUES';
        $query .= ' (';
        $query .=  ' '.$this->quote($user->getId());
        $query .=  ', '.$this->quote($user->getUsername());
        $query .=  ', '.$firstName;
        $query .=  ', '.$lastName;
        $query .=  ', '.$this->quote($user->getEmail());
        $query .=  ')';

        return $this->executeUpdate($query);
    }
}
