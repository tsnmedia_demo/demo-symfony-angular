<?php

namespace Backend\Service\Mapper\Api\Response;

use Backend\Component\Menu\MenuProvider;
use Backend\Component\Menu\MenuProviderInterface;
use Backend\Service\Mapper\MapperInterface;
use Backend\ValueObject\Api\Response\MainResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class MainResponseMapper implements MapperInterface
{
    const DIC_NAME = 'Backend.Service.Mapper.Api.Response.MainResponseMapper';

    /** @var TokenStorageInterface $securityContext */
    protected $tokenStorage;

    /** @var  MenuProviderInterface $menuProvider */
    protected $menuProvider;

    /**
     * MainResponseMapper constructor.
     *
     * @param MenuProvider $menuProvider
     */
    public function __construct(MenuProvider $menuProvider)
    {
        $this->menuProvider = $menuProvider;
    }

    /**
     * {@inheritdoc}
     */
    public function map()
    {
        /** @var MainResponse $mainResponse */
        $mainResponse = new MainResponse();
        $mainResponse->setMenu($this->menuProvider->getMenu());
        $mainResponse->setLastToolLabel($this->menuProvider->getLastToolLabel());

        return $mainResponse;
    }
}
