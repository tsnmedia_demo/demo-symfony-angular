<?php

namespace Backend\Service\Mapper\Api\Request;

use Backend\Service\Mapper\MapperInterface;

interface RequestMapperInterface extends MapperInterface
{
    /**
     * @param string $content
     */
    public function setContent($content);
}
