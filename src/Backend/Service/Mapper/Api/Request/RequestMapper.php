<?php

namespace Backend\Service\Mapper\Api\Request;

abstract class RequestMapper implements RequestMapperInterface
{
    protected $content;

    /**
     * {@inheritdoc}
     */
    public function setContent($content)
    {
        $this->content = json_decode($content);
    }
}
