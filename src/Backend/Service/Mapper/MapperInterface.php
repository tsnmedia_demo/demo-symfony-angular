<?php

namespace Backend\Service\Mapper;

interface MapperInterface
{
    /**
     * @return object
     */
    public function map();
}
