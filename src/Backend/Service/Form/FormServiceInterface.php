<?php

namespace Backend\Service\Form;

interface FormServiceInterface
{
    const DIC_NAME = 'Backend.Service.Form.FormServiceInterface';

    /**
     * @param mixed  $entity
     * @param string $type
     * @param array  $parameters
     * @param array  $options
     *
     * @return mixed $entity with data included in $parameters
     *
     * @throws InvalidFormException
     */
    public function processForm($entity, $type, array $parameters, array $options = []);

    /**
     * @param mixed  $entity
     * @param string $type
     *
     * @return array
     */
    public function createViewResponse($entity, $type);

    /**
     * @param InvalidFormException $exception
     *
     * @return array [error => 'number', value='mixed']
     */
    public function getErrorResponse(InvalidFormException $exception);
}
