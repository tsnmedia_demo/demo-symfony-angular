<?php

namespace Backend\Service\Form;

use Symfony\Component\Form\FormInterface;

class InvalidFormException extends \Exception
{
    /** @var FormInterface $form */
    protected $form;

    /**
     * @param string             $message
     * @param FormInterface|null $form
     */
    public function __construct($message, FormInterface $form = null)
    {
        parent::__construct($message);
        $this->form = $form;
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
}
