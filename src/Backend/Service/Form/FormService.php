<?php

namespace Backend\Service\Form;

use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Backend\ValueObject\Error\ErrorMessage;

class FormService implements FormServiceInterface
{
    /** @var FormFactoryInterface $formFactory */
    protected $formFactory;

    /**
     * @param FormFactoryInterface $formFactory
     */
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function processForm($entity, $type, array $parameters, array $options = [])
    {
        $form = $this->formFactory->create($type, $entity, $options);
        $form->submit($parameters);

        if (!$form->isValid()) {
            throw new InvalidFormException('Invalid submitted data', $form);
        }

        return $form->getData();
    }

    /**
     * {@inheritdoc}
     */
    public function createViewResponse($entity, $type)
    {
        $form = $this->formFactory->create($type, $entity);

        $viewResponse[$form->getName()] = $this->iterateForm($form);

        return $viewResponse;
    }

    /**
     * {@inheritdoc}
     */
    public function getErrorResponse(InvalidFormException $exception)
    {
        /** @var FormInterface $form */
        $form = $exception->getForm();
        $formErrorIterator = $form->getErrors(true, true);

        if ($formErrorIterator->count()) {
            // Show only the first error
            $formError = $formErrorIterator->offsetGet(0);

            /*
             * Maybe in assert message dont have format: message="4|{{ value }}",
             * in that case put ErrorMessage::UNDEFINED
             */
            if (strpos($formError->getMessage(), '|') !== false) {
                // "cod_error|value"
                list($errorCode, $value) = explode('|', $formError->getMessage());

                // Example: $errorCode="Backend\ValueObject\Error\ErrorMessage::IBAN"
                $errorCodeConstant = constant(ErrorMessage::class.'::'.$errorCode);

                $value = is_string($value) ? trim($value, '""') : $value;

                $errors = [
                    'error' => (int) $errorCodeConstant,
                    'value' => $value,
                ];
            } else {
                $errors = ['error' => ErrorMessage::UNDEFINED, 'value' => ''];
            }
        } else {
            $errors = ['error' => ErrorMessage::UNDEFINED, 'value' => ''];
        }

        return $errors;
    }

    private function iterateForm(FormInterface $form)
    {
        $formChildren = $form->all();
        $viewResponse = [];

        /* @var Form $form */
        foreach ($formChildren as $namePropertyFront => $formChild) {
            if ($formChild->count()) {
                $viewResponse[$namePropertyFront] = $this->iterateForm($formChild);
            } else {
                $viewResponse[$namePropertyFront] = $formChild->getData();
            }
        }

        return $viewResponse;
    }
}
