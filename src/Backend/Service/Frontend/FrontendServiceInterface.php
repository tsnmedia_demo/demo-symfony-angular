<?php

namespace Backend\Service\Frontend;

use Backend\ValueObject\Frontend\JavascriptError;

interface FrontendServiceInterface
{
    const DIC_NAME = 'Backend.Service.Frontend.FrontendServiceInterface';

    /**
     * @param JavascriptError $javascriptError
     */
    public function log(JavascriptError $javascriptError);
}
