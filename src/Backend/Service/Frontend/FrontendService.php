<?php

namespace Backend\Service\Frontend;

use Backend\Component\Log\LogTrait;
use Backend\ValueObject\Frontend\JavascriptError;
use Psr\Log\LogLevel;

class FrontendService implements FrontendServiceInterface
{
    use LogTrait;

    /**
     * {@inheritdoc}
     */
    public function log(JavascriptError $javascriptError)
    {
        $extraData = [
            'user_id' => $javascriptError->getUserId(),
            'ip' => $javascriptError->getIp(),
            'user_agent' => $javascriptError->getUserAgent(),
        ];

        $this->writeLog($javascriptError->getMessage(), $extraData, LogLevel::ERROR);
    }
}
