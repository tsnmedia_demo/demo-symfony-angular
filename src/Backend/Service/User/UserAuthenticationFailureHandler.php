<?php

namespace Backend\Service\User;

use Backend\Component\Log\LogTrait;
use Psr\Log\LogLevel;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;

class UserAuthenticationFailureHandler implements AuthenticationFailureHandlerInterface
{
    use LogTrait;

    const DIC_NAME = 'Backend.Service.User.UserAuthenticationFailureHandler';

    /** @var RouterInterface $router */
    protected $router;

    /**
     * UserAuthenticationFailureHandler constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $this->writeLog($exception->getMessage(), [], LogLevel::ERROR);

        $redirectUrl = $this->router->generate('app_login');

        return new RedirectResponse($redirectUrl);
    }
}
