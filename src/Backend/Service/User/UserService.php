<?php

namespace Backend\Service\User;

use Backend\Entity\Security\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserService implements UserServiceInterface
{
    /** @var TokenStorageInterface $tokenStorage */
    protected $tokenStorage;

    /**
     * UserService constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUserSession(User $userUpdated)
    {
        $token = $this->tokenStorage->getToken();
        $token->setUser($userUpdated);
    }
}
