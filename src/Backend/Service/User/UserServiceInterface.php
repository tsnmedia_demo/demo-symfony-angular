<?php

namespace Backend\Service\User;

use Backend\Entity\Security\User;

interface UserServiceInterface
{
    const DIC_NAME = 'Backend.Service.User.UserServiceInterface';

    /**
     * @param User $userUpdated
     */
    public function refreshUserSession(User $userUpdated);
}
