<?php

namespace Backend\Service\LinkBuilder;

use Backend\ValueObject\Api\Link;

interface LinkBuilderInterface
{
    const DIC_NAME = 'Backend.Service.LinkBuilder.LinkBuilderInterface';

    /**
     * Pass an array of Link values as parameter and return links in a proper format. i.e.:.
     *
     *  [
     *     'links' => [
     *        [
     *            'name' => 'request-rights',
     *            'href' => '/api/users/request-rights',
     *            'rel' => 'self',
     *            'method' => 'GET'
     *        ],
     *        [
     *            'name' => 'do-request',
     *            'href' => '/api/users/request-rights',
     *            'rel' => 'do-request',
     *            'method' => 'POST'
     *        ]
     *     ]
     *  ]
     *
     * @param array $links
     *
     * @return array
     */
    public function getFormattedLinks(array $links);

    /**
     * @param Link $link
     *
     * @return array
     */
    public function getFormattedLink(Link $link);
}
