<?php

namespace Backend\Service\LinkBuilder;

use Backend\Service\LinkBuilder\Exceptions\LinkBuilderExpectsLinkValueException;
use Backend\ValueObject\Api\Link;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\RouteCollection;

class LinkBuilder implements LinkBuilderInterface
{
    /** @var RouterInterface $router */
    protected $router;

    /** @var RouteCollection $routerCollection */
    protected $routerCollection;

    /**
     * LinkBuilder constructor.
     *
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
        $this->routerCollection = $this->router->getRouteCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getFormattedLinks(array $links)
    {
        $formattedLinks = [];

        foreach ($links as $link) {
            if (!($link instanceof Link)) {
                throw new LinkBuilderExpectsLinkValueException('Only link value objects are accepted');
            }

            $formattedLinks[] = $this->getFormattedLink($link);
        }

        return ['links' => $formattedLinks];
    }

    /**
     * {@inheritdoc}
     */
    public function getFormattedLink(Link $link)
    {
        $method = $link->getMethod()
            ? $link->getMethod()
            : $this->routerCollection->get($link->getRoute())->getMethods()[0];

        return [
            'name' => $link->getName(),
            'href' => $this->router->generate($link->getRoute(), $link->getRouteParameters()),
            'rel' => $link->getRel(),
            'method' => $method,
        ];
    }
}
