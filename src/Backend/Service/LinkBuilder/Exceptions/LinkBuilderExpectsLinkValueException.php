<?php

namespace Backend\Service\LinkBuilder\Exceptions;

class LinkBuilderExpectsLinkValueException extends \Exception
{
}
