<?php

namespace test\Functional;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use test\DbTestCaseTrait;

class FunctionalTestCase extends WebTestCase
{
    use DbTestCaseTrait;

    /** @var \Symfony\Component\HttpKernel\Client $client */
    protected $client = null;

    public function setUp()
    {
        $this->client = static::createClient();
    }

    protected function getContainer()
    {
        return $this->client->getContainer();
    }

    protected function loginUser($user, array $roles = [])
    {
        $session = $this->getContainer()->get('session');

        $firewall = 'secured_area';
        $token = new UsernamePasswordToken($user, null, $firewall, array_merge(['ROLE_USER'], $roles));
        $session->set('_security_'.$firewall, serialize($token));
        $session->save();

        $cookie = new Cookie($session->getName(), $session->getId());
        $this->client->getCookieJar()->set($cookie);
    }
}
