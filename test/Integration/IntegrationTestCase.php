<?php

namespace test\Integration;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IntegrationTestCase extends KernelTestCase
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface $container */
    protected $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
    }

    /**
     * @param string $service
     *
     * @return object
     */
    protected function get($service)
    {
        if (!$this->getContainer()) {
            throw new \RuntimeException('Before using containers, you should set up the kernel');
        }

        return $this->getContainer()->get($service);
    }

    protected function getContainer()
    {
        return $this->container;
    }
}
