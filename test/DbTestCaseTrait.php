<?php

namespace test;

use Doctrine\DBAL\Connection;

trait DbTestCaseTrait
{
    /** @var string $nameDatabaseLocalConnection */
    public static $nameDatabaseLocalConnection = 'doctrine.dbal.app_write_default_connection';

    /** @var string $nameDatabaseGlobalConnection */
    public static $nameDatabaseGlobalConnection = 'doctrine.dbal.app_global_write_connection';

    /** @var  Connection $connection */
    protected $connection;

    /**
     * @return mixed
     */
    abstract protected function getContainer();

    /**
     * @param bool $isGlobalDatabase
     *
     * @return \Doctrine\DBAL\Connection
     */
    public function getConnection($isGlobalDatabase = false)
    {
        $nameDatabaseConnection = ($isGlobalDatabase) ?
            static::$nameDatabaseGlobalConnection : static::$nameDatabaseLocalConnection;

        if ($this->connection === null) {
            return $this->getContainer()->get($nameDatabaseConnection);
        }

        return $this->connection;
    }

    /**
     * @param Connection $connection
     */
    public function setConnection(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * This will truncate a table and then load some fixtures into it.
     *
     * @param string $tableName
     * @param int    $fixturesFilename
     * @param bool   $globalDatabase
     *
     * @return object
     */
    public function loadTableFixtures($tableName, $fixturesFilename, $globalDatabase = false)
    {
        $this->truncateTable($tableName, $globalDatabase);

        $this->executeQueryFixtures($fixturesFilename, $globalDatabase);
    }

    /**
     * Not using TRUNCATE TABLE due to restrictions with foreign keys.
     *
     * @param string $tableName
     * @param bool   $globalDatabase
     *
     * @return object
     */
    public function truncateTable($tableName, $globalDatabase = false)
    {
        $nameDatabaseConnection = ($globalDatabase) ?
            static::$nameDatabaseGlobalConnection : static::$nameDatabaseLocalConnection;

        $this->getContainer()->get($nameDatabaseConnection)->executeQuery('DELETE FROM '.$tableName);
        $this->getContainer()
            ->get($nameDatabaseConnection)->executeQuery('ALTER TABLE '.$tableName.' AUTO_INCREMENT = 1');
    }

    /**
     * Drop table.
     *
     * @param string $tableName
     * @param bool   $globalDatabase
     *
     */
    public function dropTable($tableName, $globalDatabase = false)
    {
        $nameDatabaseConnection = ($globalDatabase) ?
            static::$nameDatabaseGlobalConnection : static::$nameDatabaseLocalConnection;

        $this->getContainer()->get($nameDatabaseConnection)->executeQuery('DROP TABLE '.$tableName);
    }
}
