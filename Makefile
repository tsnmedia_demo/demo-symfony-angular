#######################
# CONSTANTS, MESSAGES #
#######################
PHPUNIT_BIN = ./bin/phpunit
REPORTS = test/quality/reports/

#########
# TESTS #
#########
unit-tests:
	@echo "-Executing unit tests..."
	@./bin/phpunit --colors -c test/phpunit_local.xml --testsuite "app-unit"
	@echo "Done."
	@echo ""

integration-tests:
	@echo "-Executing integration tests..."
	@./bin/phpunit --colors -c test/phpunit_local.xml --testsuite "app-integration"
	@echo "Done."
	@echo ""

functional-tests:
	@echo "-Executing functional tests..."
	@./bin/phpunit --colors -c test/phpunit_local.xml --testsuite "app-functional"
	@echo "Done."
	@echo ""

all-no-coverage-tests:
	@echo "-Executing tests with no coverage report..."
	@./bin/phpunit --colors -c test/phpunit_local.xml
	@echo "Done."
	@echo ""

tests:
	@echo "-Executing tests with coverage report..."
	@rm -Rf $(REPORTS)testsCoverage
	@./bin/phpunit --colors -c test/phpunit.xml --coverage-html $(REPORTS)testsCoverage/
	@echo "Done."
	@echo "If Xdebug extension is loaded, you can see unit tests coverage in $(REPORTS)testsCoverage/index.html"
	@echo ""

#####################
# REBUILD DATABASES #
#####################
rebuild-test-databases:
	@echo ""
	@bin/rebuild-databases.sh test
	@echo ""

rebuild-dev-databases:
	@echo ""
	@bin/rebuild-databases.sh dev
	@echo ""

#################
# DEPLOY ASSETS #
#################
deploy-assets:
	@echo ""
	@./bin/prepare_assets.sh
	@echo ""
